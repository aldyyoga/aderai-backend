<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require APPPATH . '/libraries/REST_Controller.php';

class Users extends REST_Controller {

    private $table_name = 'cms_app_users';
    private $table_fans = 'cms_app_fans';
    private $table_session = 'cms_app_session';

    function __construct()
    {
        parent::__construct();
        $this->load->helper(array('security'));
    }
    
    private function generate_session($user_id)
    {
        $new_token = sha1($this->security->get_random_bytes(128));
        $waktu = date('Y-m-d H:i:s', now());
        $query = $this->db->query("SELECT * from `$this->table_session` WHERE user_id = '$user_id'");
        $row = $query->row();
        if($row){
            $query = $this->db->query("
                DELETE FROM `$this->table_session` WHERE user_id = '$user_id';
            ");
            $result = $this->db->affected_rows();
        }
        $query = $this->db->query("
            INSERT INTO `$this->table_session` 
            (`id`, `token`, `user_id`, `created`, `modified`) 
            VALUES (NULL, '$new_token', '$user_id', '$waktu', '$waktu');
        ");
        $result = $this->db->affected_rows();
        return $new_token;
    }

    public function refresh_token_post()
    {
        $token = $this->post('token');
        $response = array("error" => TRUE);

        $user_id = get_userid($token);
        if($user_id){
            $response["error"] = FALSE;
            $response["data"]["token"] = $this->generate_session($user_id);
        }else{
            $response["error_msg"] = "unauthenticated";
        }

        $this->set_response($response, REST_Controller::HTTP_OK);
    }

    public function login_post()
    {
        $email = $this->post('email');
        $password = $this->post('password');
        $response = array("error" => TRUE);

        $query = $this->db->query("
            SELECT u.id, u.name, u.email, u.password, 
            IF(t.id, 'true', 'false') AS trainer 
            FROM `$this->table_name` AS u 
            LEFT JOIN `cms_app_trainer` AS t 
            ON u.id=t.user_id WHERE email = '$email'"
        );

        $row = $query->row();
        $user = NULL;
        if($row){
            if ($row->password == sha1($password)) {
                $response["error"] = FALSE;
                $response["data"]["token"] = $this->generate_session($row->id);
                $response["data"]["id"] = $row->id;
                $response["data"]["name"] = $row->name;
                $response["data"]["email"] = $row->email;
                $response["data"]["trainer"] = $row->trainer;
                $response["data"]["phone"] = "0";
            }
        }

        if($response["error"]==TRUE){
            $response["error_msg"] = "Login credentials are wrong. Please try again!";
        }

        $this->set_response($response, REST_Controller::HTTP_OK);
    }

    public function register_post()
    {
        $email = $this->post('email');
        $name = $this->post('name');
        $password = $this->post('password');
        $phone = $this->post('phone');
        $waktu = date('Y-m-d H:i:s', now());
        $response = array("error" => TRUE);

        if($email && $name && $password && $phone){
            $query = $this->db->query("SELECT * from `$this->table_name` WHERE email = '$email'");
            $row = $query->row();
            if($row){
                $response["error"] = TRUE;
                $response["error_msg"] = "User already existed with " . $email;
            }else{
                $encrypted_password = sha1($password);
                $query = $this->db->query("
                    INSERT INTO `$this->table_name` 
                    (`id`, `name`, `email`, `password`, `remember_token`, `image`, `point`,`created`, `modified`) 
                    VALUES (NULL, '$name', '$email', '$encrypted_password', NULL, '', '100', '$waktu', '$waktu');
                ");
                $result = $this->db->affected_rows();
                if($result){
                    $query = $this->db->query("SELECT * from `$this->table_name` WHERE email = '$email'");
                    $row = $query->row();
                    if($row){
                        $response["error"] = FALSE;
                        $response["data"]["token"] = $this->generate_session($row->id);
                        $response["data"]["id"] = $row->id;
                        $response["data"]["name"] = $row->name;
                        $response["data"]["email"] = $row->email;
                        $response["data"]["trainer"] = 'false';
                        $response["data"]["phone"] = "0";

                        $query = $this->db->query("
                            INSERT INTO `$this->table_fans` (`id`, `user_id`, `point`, `created`, `modified`) VALUES (NULL, '$row->id', 5, '$waktu', '$waktu');
                        ");
                        $result = $this->db->affected_rows();
                    }
                }else{
                    $response["error_msg"] = "Register error!";
                }
            }
        } else {
            $response["error"] = TRUE;
            $response["error_msg"] = "Required parameters (name, email, password, phone) is missing!";
        }

        $this->set_response($response, REST_Controller::HTTP_OK);
    }

    public function profile_get()
    {
        $response = array("error" => TRUE, "data" => array());
        $token = $this->get('token');

        if($token){
            $user_id = get_userid($token);
            if($user_id){
                $response["error"] = FALSE;

                $query = $this->db->query("
                    SELECT u.id, u.name, u.email, u.password, 
                    IF(t.id, 'true', 'false') AS trainer 
                    FROM `$this->table_name` AS u 
                    LEFT JOIN `cms_app_trainer` AS t 
                    ON u.id=t.user_id WHERE u.id = '$user_id'
                ");
                $row = $query->row();
                if(!$row){
                    $response["error"] = TRUE;
                    $response["error_msg"] = "tidak ditemukan";
                    $this->set_response($response, REST_Controller::HTTP_OK);
                    return;
                }

                $response["data"]["id"] = $row->id;
                $response["data"]["name"] = $row->name;
                $response["data"]["email"] = $row->email;
                $response["data"]["trainer"] = $row->trainer;
                $response["data"]["phone"] = "0";

            }else{
                $response["error_msg"] = "unauthenticated";
            }
        }else{
            $response["error_msg"] = "token required!";
        }

        $this->set_response($response, REST_Controller::HTTP_OK);
    }

    public function profile_post()
    {
        $email = $this->post('email');
        $name = $this->post('name');
        $phone = $this->post('phone');
        $token = $this->post('token');
        $waktu = date('Y-m-d H:i:s', now());
        $response = array("error" => TRUE);

        if($token && $email && $name && $phone){
            $user_id = get_userid($token);
            if($user_id){
                $query = $this->db->query("
                    UPDATE `$this->table_name` SET
                    `email`='$email', `name`='$name'
                    WHERE `id` = '$user_id';
                ");
                $result = $this->db->affected_rows();
                $query = $this->db->query("
                    SELECT u.id, u.name, u.email, u.password, 
                    IF(t.id, 'true', 'false') AS trainer 
                    FROM `$this->table_name` AS u 
                    LEFT JOIN `cms_app_trainer` AS t 
                    ON u.id=t.user_id WHERE u.id = '$user_id'"
                );
                $row = $query->row();
                if($row){
                    $response["error"] = FALSE;
                    $response["data"]["id"] = $row->id;
                    $response["data"]["name"] = $row->name;
                    $response["data"]["email"] = $row->email;
                    $response["data"]["trainer"] = $row->trainer;
                    $response["data"]["phone"] = "0";
                }
            } else {
                $response["error_msg"] = "unauthenticated";
            }
        } else {
            $response["error_msg"] = "Required parameters (token, name, email, phone) is missing!";
        }

        $this->set_response($response, REST_Controller::HTTP_OK);
    }

    public function change_password_post()
    {
        $old_password = $this->post('old_password');
        $password = $this->post('password');
        $confirm_password = $this->post('confirm_password');
        $token = $this->post('token');
        $response = array("error" => TRUE);

        if($token && $old_password && $password && $confirm_password){
            $user_id = get_userid($token);
            if($user_id){
                $query = $this->db->query("SELECT * from `$this->table_name` WHERE `id` = '$user_id'");
                $row = $query->row();

                if($row->password==sha1($old_password)){
                    if($password==$confirm_password){
                        $encrypted_password = sha1($password);
                        $query = $this->db->query("
                            UPDATE `$this->table_name` SET
                            `password`='$encrypted_password'
                            WHERE `id` = '$user_id';
                        ");
                        $result = $this->db->affected_rows();
                        $response["error"] = FALSE;
                        $response["data"] = "success";
                    }else{
                        $response["error_msg"] = "Confirm password not match";
                    }
                }else{
                    $response["error_msg"] = "Old password not valid";
                }
            } else {
                $response["error_msg"] = "unauthenticated";
            }
        } else {
            $response["error_msg"] = "Required parameters (token, old_password, password, confirm_password) is missing!";
        }

        $this->set_response($response, REST_Controller::HTTP_OK);
    }

    public function reset_password_get()
    {
        $this->load->library('email');
        $response = array("error" => TRUE, "data" => array());
        $token = $this->get('token');
        $action = $this->get('action');

        if($token && $action){
            $user_id = get_userid($token);
            if($user_id){

                if($action=="generate"){
                    $query = $this->db->query("SELECT * from `$this->table_name` WHERE `id` = '$user_id'");
                    $row = $query->row();

                    $new_token = sha1($this->security->get_random_bytes(128));
                    $query = $this->db->query("
                        UPDATE `$this->table_name` SET
                        `remember_token`='$new_token'
                        WHERE `id` = '$user_id';
                    ");
                    $result = $this->db->affected_rows();
                    if($result && $row){
                        //SEND EMAIL
                        $url_reset = site_url("rest/users/reset_password?action=reset&token=$token&remember_token=$new_token");
                        $this->email->from('no-reply@aderai.id', 'Aderai App');
                        $this->email->to($row->email); 
                        $this->email->subject('Reset password Aderai App');
                        $this->email->message("
                            Follow link for reset password: <br/>
                            <a href='$url_reset'>$url_reset</>
                        "); 
                        if ( ! $this->email->send()){
                            $response["error_msg"] = "Error sending email!";
                            $response["error_debug"] = $this->email->print_debugger();
                        }else{
                            $response["error"] = FALSE;
                            $response["data"] = "success";
                        }
                    }else{
                        $response["error_msg"] = "Update error!";
                    }
                }elseif ($action=="reset") {
                    $remember_token = $this->get('remember_token');
                    if($remember_token){
                        $query = $this->db->query("SELECT * from `$this->table_name` WHERE `id` = '$user_id'");
                        $row = $query->row();
                        if($row){
                            if($row->remember_token==$remember_token){
                                $new_password = $this->security->get_random_bytes(5);
                                $encrypted_password = sha1($new_password);
                                $query = $this->db->query("
                                    UPDATE `$this->table_name` SET
                                    `password`='$encrypted_password'
                                    WHERE `id` = '$user_id';
                                ");
                                $result = $this->db->affected_rows();
                                if($result){
                                    //SEND EMAIL
                                    $this->email->from('no-reply@aderai.id', 'Aderai App');
                                    $this->email->to($row->email); 
                                    $this->email->subject('Reset password Aderai App');
                                    $this->email->message("
                                        Your new password: <br/>
                                        $new_password
                                    ");  
                                    if ( ! $this->email->send()){
                                        $response["error_msg"] = "Error sending email!";
                                        $response["error_debug"] = $this->email->print_debugger();
                                    }else{
                                        $response["error"] = FALSE;
                                        $response["data"] = "success, check email for password";
                                    }
                                }else{
                                    $response["error_msg"] = "error reset password";
                                }
                            }else{
                                $response["error_msg"] = "remember_token invalid";
                            }
                        }
                    }else{
                        $response["error_msg"] = "remember_token required";
                    }
                }else{
                    $response["error_msg"] = "action error";
                }

            }else{
                $response["error_msg"] = "unauthenticated";
            }
        }else{
            $response["error_msg"] = "parameter token,action required!";
        }

        $this->set_response($response, REST_Controller::HTTP_OK);
    }

}
