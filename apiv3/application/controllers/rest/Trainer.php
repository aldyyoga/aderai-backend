<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require APPPATH . '/libraries/REST_Controller.php';

class Trainer extends REST_Controller {

    private $table_name = 'cms_app_trainer';
    private $table_users = 'cms_app_users';

    function __construct()
    {
        parent::__construct();
    }

    public function all_get()
    {
        $response = array("error" => TRUE, "data" => array());
        $token = $this->get('token');

        $page = $this->get('page');
        if(!$page){
            $page = 1;
        }
        $per_page = $this->get('per_page');
        if(!$per_page){
            $per_page = 10;
        }
        if($page > 1){
            $page0 = ($per_page * $page) - $per_page;
            $pagez = $per_page;
        }else{
            $page0 = 0;
            $pagez = $per_page;
        }

        if($token){
            $user_id = get_userid($token);
            if($user_id){
                $response["error"] = FALSE;

                $query = $this->db->query("
                    SELECT u.id, u.name, u.image, t.status 
                    FROM `$this->table_users` AS `u`
                    INNER JOIN `$this->table_name` AS `t`
                    ON u.id=t.user_id WHERE 1 ORDER BY u.name LIMIT $page0, $pagez
                ");
                foreach ($query->result_array() as $row)
                {   
                    $data["id"] = $row['id'];
                    $data["name"] = $row['name'];
                    $data["image"] = $row['image'];

                    if($row['status']==0){
                        $str_status = 'offline';
                    }elseif ($row['status']==1) {
                        $str_status = 'online';
                    }elseif ($row['status']==2) {
                        $str_status = 'bussy';
                    }else{
                        $str_status = 'None';
                    }
                    $data["status"] = $str_status;

                    array_push($response['data'], $data);
                }

                $query = $this->db->query("
                    SELECT count(*) AS count from `$this->table_name` WHERE 1
                ");
                $row_count = $query->row();
                $total_data = $row_count->count;

                $numpages = intval($total_data/$per_page);
                $num_pages = $total_data/$per_page;
                if($num_pages > $numpages){
                    $numpages = $numpages+1;
                }
                if($page > $numpages){
                    $response["error_msg"] = "Data not found";
                }

                $response["pagination"]["numpages"] = $numpages;
                $response["pagination"]["limit"] = $per_page;
                $response["pagination"]["total_result"] = $total_data;
                $response["pagination"]["current_page"] = $page;

            }else{
                $response["error_msg"] = "unauthenticated";
            }
        }else{
            $response["error_msg"] = "token required!";
        }

        $this->set_response($response, REST_Controller::HTTP_OK);
    }

    public function status_post()
    {
        $token = $this->post('token');
        $status = $this->post('status');
        if(($status!=0) && ($status!=1) && ($status!=2)){
            $status = -5;
        }
        if($token && isset($status) && ($status!=-5)){
            $user_id = get_userid($token);
            if($user_id){
                $query = $this->db->query("
                    UPDATE `$this->table_name` 
                    SET `status`='$status' 
                    WHERE `user_id` = '$user_id';
                ");
                $result = $this->db->affected_rows();
                $response["error"] = FALSE;
                $response["data"] = "success";
            }else{
                $response["error_msg"] = "unauthenticated";
            }
        } else {
            $response["error"] = TRUE;
            $response["error_msg"] = "Required parameters is missing!";
        }
        $this->set_response($response, REST_Controller::HTTP_OK);
    }
}
