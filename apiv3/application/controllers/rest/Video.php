<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require APPPATH . '/libraries/REST_Controller.php';

class Video extends REST_Controller {

    private $table_name = 'cms_app_video';
    private $table_category = 'cms_app_video_category';
    private $table_video_feedback = 'cms_app_video_feedback_rec';
    private $table_video_online = 'cms_app_video_online';
    private $table_users = 'cms_app_users';
    private $table_trainer = 'cms_app_trainer';

    function __construct()
    {
        parent::__construct();
    }

    public function category_get()
    {
        $response = array("error" => TRUE, "data" => array());
        $token = $this->get('token');
        $asset_url = $this->config->item('asset_url');

        if($token){
            $user_id = get_userid($token);
            if($user_id){
                $response["error"] = FALSE;

                $query = $this->db->query("SELECT * from `$this->table_category` WHERE 1 LIMIT 100");
                foreach ($query->result_array() as $row)
                {   
                    $data["id"] = $row['id'];
                    $data["name"] = $row['name'];
                    $data["image"] = $asset_url."video/thumb/".$row['image'];
                    array_push($response['data'], $data);
                }

            }else{
                $response["error_msg"] = "unauthenticated";
            }
        }else{
            $response["error_msg"] = "token required!";
        }

        $this->set_response($response, REST_Controller::HTTP_OK);
    }

    public function all_get()
    {
        $response = array("error" => TRUE, "data" => array());
        $token = $this->get('token');
        $category = $this->get('category');
        $asset_url = $this->config->item('asset_url');

        $page = $this->get('page');
        if(!$page){
            $page = 1;
        }
        $per_page = $this->get('per_page');
        if(!$per_page){
            $per_page = 10;
        }
        if($page > 1){
            $page0 = ($per_page * $page) - $per_page;
            $pagez = $per_page;
        }else{
            $page0 = 0;
            $pagez = $per_page;
        }

        if($token){
            $user_id = get_userid($token);
            if($user_id){
                $response["error"] = FALSE;

                $query = $this->db->query("
                    SELECT id,title,description,image,video,total_like,total_views,created 
                    FROM `$this->table_name` 
                    WHERE `id_category`='$category' 
                    LIMIT $page0, $pagez
                ");
                foreach ($query->result_array() as $row)
                {   
                    $data["id"] = $row['id'];
                    $data["title"] = $row['title'];
                    $data["description"] = $row['description'];
                    $data["image"] = $asset_url."video/thumb/".$row['image'];
                    $data["video"] = $asset_url."video/".$row['video'];
                    $data["upload_date"] = $row['created'];
                    $data["upload_date_elapsed"] = humanTiming(strtotime($row['created']));
                    $data["liked"] = $this->is_user_feedback($user_id, $row['id'], 1);
                    $data["total_like"] = $this->get_total_feedback($row['id'], 1);
                    $data["total_views"] = $this->get_total_feedback($row['id'], 2);
                    array_push($response['data'], $data);
                }

                $query = $this->db->query("
                    SELECT count(*) AS count from `$this->table_name` WHERE `id_category`='$category' 
                ");
                $row_count = $query->row();
                $total_data = $row_count->count;

                $numpages = intval($total_data/$per_page);
                $num_pages = $total_data/$per_page;
                if($num_pages > $numpages){
                    $numpages = $numpages+1;
                }
                if($page > $numpages){
                    $response["error_msg"] = "Data not found";
                }

                $response["pagination"]["numpages"] = $numpages;
                $response["pagination"]["limit"] = $per_page;
                $response["pagination"]["total_result"] = $total_data;
                $response["pagination"]["current_page"] = $page;

            }else{
                $response["error_msg"] = "unauthenticated";
            }
        }else{
            $response["error_msg"] = "token required!";
        }

        $this->set_response($response, REST_Controller::HTTP_OK);
    }

    public function get_total_feedback($id_video, $tipe)
    {
        $query = $this->db->query("
            SELECT COUNT(*) AS count from `$this->table_video_feedback`
            WHERE id_video = '$id_video' AND feedback_type = '$tipe'
        ");
        $row = $query->row();
        return $row->count;
    }

    //apa user sudah menfeedback
    public function is_user_feedback($user_id, $id_video, $tipe)
    {
        $query = $this->db->query("
            SELECT * from `$this->table_video_feedback`
            WHERE id_user = '$user_id' AND id_video = '$id_video' AND feedback_type = '$tipe'
        ");
        $row = $query->row();
        if($row){
            return TRUE;
        }else{
            return FALSE;
        }
    }

    public function action_post()
    {
        $token = $this->post('token');
        $id = $this->post('id');
        $type = $this->post('type');
        $location = $this->post('location');
        $device = $this->post('device');
        $os = $this->post('os');
        $response = array("error" => TRUE);
        $waktu = date('Y-m-d H:i:s', now());

        if($token && $id && $type && $location && $device && $os){
            $user_id = get_userid($token);
            if($user_id){
                $response["error"] = FALSE;
                $query = $this->db->query("SELECT * from `$this->table_name` WHERE id = '$id'");
                $row = $query->row();
                if($row){

                    if($this->is_user_feedback($user_id, $id, $type)){
                        $response["error_msg"] = "user already feedback";
                        $this->set_response($response, REST_Controller::HTTP_OK);
                        return;
                    }

                    if($type=='1'){
                        $query = $this->db->query("
                            INSERT INTO `cms_app_video_feedback_rec` (`id`, `id_user`, `id_video`, `feedback_type`, `time`, `location`, `device`, `os`, `created`, `modified`) 
                            VALUES (NULL, '$user_id', '$id', '$type', '$waktu', '$location', '$device', '$os', '$waktu', '$waktu');
                        ");
                        $result = $this->db->affected_rows();
                    }elseif ($type=='2') {
                        $query = $this->db->query("
                            INSERT INTO `cms_app_video_feedback_rec` (`id`, `id_user`, `id_video`, `feedback_type`, `time`, `location`, `device`, `os`, `created`, `modified`) 
                            VALUES (NULL, '$user_id', '$id', '$type', '$waktu', '$location', '$device', '$os', '$waktu', '$waktu');
                        ");
                        $result = $this->db->affected_rows();
                    }else{
                        $this->set_response($response, REST_Controller::HTTP_OK);
                        return;
                    }

                    $data["id"] = $row->id;
                    $data["total_feedback"] = $this->get_total_feedback($id, $type);
                    $data["is_feedback"] = $this->is_user_feedback($user_id, $id, $type);
                    $response["error"] = FALSE;
                    $response["data"] = $data;
                }else{
                    $response["error_msg"] = "id_video not valid";
                }
            }else{
                $response["error_msg"] = "unauthenticated";
            }
        } else {
            $response["error"] = TRUE;
            $response["error_msg"] = "Required parameters (token, id, type, location, device, os) is missing!";
        }

        $this->set_response($response, REST_Controller::HTTP_OK);
    }

    public function online_all_get()
    {
        $response = array("error" => TRUE, "data" => array());
        $token = $this->get('token');
        $asset_url = $this->config->item('asset_url');

        $page = $this->get('page');
        if(!$page){
            $page = 1;
        }
        $per_page = $this->get('per_page');
        if(!$per_page){
            $per_page = 10;
        }
        if($page > 1){
            $page0 = ($per_page * $page) - $per_page;
            $pagez = $per_page;
        }else{
            $page0 = 0;
            $pagez = $per_page;
        }

        if($token){
            $user_id = get_userid($token);
            if($user_id){
                $response["error"] = FALSE;

                $query = $this->db->query("
                    SELECT `u`.`name`, `vo`.`id`, `vo`.`title`, `vo`.`description`, `vo`.`image`, `vo`.`video`, `vo`.`total_like`, `vo`.`modified`
                    FROM `$this->table_video_online` AS `vo`
                    INNER JOIN `$this->table_users` AS `u`
                    ON `vo`.`user_id`=`u`.`id`
                    ORDER BY modified DESC
                    LIMIT $page0, $pagez
                ");
                foreach ($query->result_array() as $row)
                {   
                    $data["id"] = $row['id'];
                    $data["name"] = $row['name'];
                    $data["title"] = $row['title'];
                    $data["description"] = $row['description'];
                    $data["image"] = $asset_url."video/thumb/".$row['image'];
                    $data["video"] = $row['video'];
                    $data["total_like"] = $row['total_like'];
                    $data["datetime"] = $row['modified'];
                    array_push($response['data'], $data);
                }

                $query = $this->db->query("
                    SELECT count(*) AS count from `$this->table_video_online`
                ");
                $row_count = $query->row();
                $total_data = $row_count->count;

                $numpages = intval($total_data/$per_page);
                $num_pages = $total_data/$per_page;
                if($num_pages > $numpages){
                    $numpages = $numpages+1;
                }
                if($page > $numpages){
                    $response["error_msg"] = "Data not found";
                }

                $response["pagination"]["numpages"] = $numpages;
                $response["pagination"]["limit"] = $per_page;
                $response["pagination"]["total_result"] = $total_data;
                $response["pagination"]["current_page"] = $page;

            }else{
                $response["error_msg"] = "unauthenticated";
            }
        }else{
            $response["error_msg"] = "token required!";
        }

        $this->set_response($response, REST_Controller::HTTP_OK);
    }

    public function online_action_post()
    {
        $token = $this->post('token');
        $image = $this->post('image');
        $video = $this->post('video');
        $title = $this->post('title');
        $description = $this->post('description');
        $response = array("error" => TRUE);
        $waktu = date('Y-m-d H:i:s', now());

        if($token && isset($_FILES['image']) && $video && $title && $description){
            $user_id = get_userid($token);
            if($user_id){

                $query = $this->db->query("
                    SELECT u.id, u.name, u.email, u.password, 
                    IF(t.id, 'true', 'false') AS trainer 
                    FROM `$this->table_users` AS u 
                    INNER JOIN `$this->table_trainer` AS t 
                    ON u.id=t.user_id WHERE u.id = '$user_id'"
                );
                $row = $query->row();
                if(!$row){
                    $response["error"] = TRUE;
                    $response["error_msg"] = "user bukan trainer";
                    $this->set_response($response, REST_Controller::HTTP_OK);
                    return;
                }

                $image_name = '';
                $uploaddir = '../assets/video/thumb/';
                $uploadfile = $uploaddir . basename($_FILES['image']['name']);
                if (move_uploaded_file($_FILES['image']['tmp_name'], $uploadfile)) {
                    $image_name = $_FILES['image']['name'];
                    $response['upload'] = 'success';       
                } else {
                    $response['upload'] =  'failed';        
                }

                $response["error"] = FALSE;
                $query = $this->db->query("SELECT * from `$this->table_video_online` WHERE user_id = '$user_id'");
                $row = $query->row();
                if($row){
                    $query = $this->db->query("
                        UPDATE `$this->table_video_online` 
                        SET `video`='$video', `image`='$image_name', `title`='$title', `description`='$description' 
                        WHERE `id` = '$row->id';
                    ");
                    $result = $this->db->affected_rows();
                }else{
                    $price = 0;
                    $query = $this->db->query("
                        INSERT INTO `$this->table_video_online` (`id`, `title`, `description`, `image`, `video`, `price`, `total_like`, `user_id`, `created`, `modified`) 
                        VALUES (NULL, '$title', '$description', '$image_name', '$video', '$price', '0', '$user_id', '$waktu', '$waktu');
                    ");
                    $result = $this->db->affected_rows();
                }

                $response["error"] = FALSE;    
                $response["data"] = "success";
            }else{
                $response["error_msg"] = "unauthenticated";
            }
        } else {
            $response["error"] = TRUE;
            $response["error_msg"] = "Required parameters (token, image, video, title, description) is missing!";
        }

        $this->set_response($response, REST_Controller::HTTP_OK);
    }
}
