<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require APPPATH . '/libraries/REST_Controller.php';

class Videocall extends REST_Controller {

    private $table_name = 'cms_app_history';
    private $table_config = 'cms_app_config';
    private $table_trainer = 'cms_app_trainer';
    private $table_users = 'cms_app_users';
    private $table_fans = 'cms_app_fans';
    private $table_wallet = 'cms_app_wallet';
    private $table_reward_users = 'cms_app_reward_users';
    private $table_reward_requir = 'cms_app_reward_requir';

    function __construct()
    {
        parent::__construct();
    }

    public function startcall_post()
    {
        $token = $this->post('token');
        $istrainer = $this->post('istrainer');
        $trainer_name = $this->post('trainer_name');
        $response = array("error" => TRUE);
        $waktu = date('Y-m-d H:i:s', now());

        if($token){

            $user_id = get_userid($token);
            if($user_id){

                if($istrainer){
                    $query = $this->db->query("
                        SELECT id,vidcallprice FROM `$this->table_trainer` WHERE `user_id`='$user_id' LIMIT 1
                    ");
                }else{
                    $query = $this->db->query("
                        SELECT `t`.`id`, `t`.`vidcallprice` 
                        FROM `$this->table_trainer` AS `t`
                        INNER JOIN `$this->table_users` AS `u` 
                        ON `t`.`user_id`=`u`.`id`
                        WHERE `u`.`name`='$trainer_name' LIMIT 1
                    ");
                }

                $row_trainer = $query->row();
                if($row_trainer){

                    $query = $this->db->query("SELECT * from `$this->table_wallet` WHERE `user_id`='$user_id' LIMIT 1");
                    $row = $query->row();
                    if(!$row){
                        $saldo = 0;
                    }else{
                        $saldo = $row->saldo;
                    }
                    $est_time = ceil($saldo / $row_trainer->vidcallprice);

                    $query = $this->db->query("
                        SELECT `ru`.`id`, `rr`.`type`, `rr`.`value` 
                        FROM `$this->table_reward_users` AS ru
                        INNER JOIN `$this->table_reward_requir` AS rr
                        ON `ru`.`id_reward`=`rr`.`id_reward`
                        WHERE `ru`.`user_id`='$user_id' AND `ru`.`status`='1' LIMIT 1
                    ");
                    $row = $query->row();
                    if(!$row){
                        $est_time_reward = 0;
                    }else{
                        if($row->type=='videocall'){
                            $est_time_reward = $row->value;
                        }else{
                            $est_time_reward = 0;
                        }
                    }

                    $data["trainer_name"] = $trainer_name;
                    $data["max_time_wallet"] = date('i:s',$est_time);
                    $data["max_time_wallet_sec"] = $est_time;
                    $data["max_time_reward"] = date('i:s',$est_time_reward);
                    $data["max_time_reward_sec"] = $est_time_reward;

                    $response["error"] = FALSE;
                    $response["data"] = $data;      

                }else{
                    $response["error_msg"] = "trainer not found";
                }

            }else{
                $response["error_msg"] = "unauthenticated";
            }

        } else {
            $response["error"] = TRUE;
            $response["error_msg"] = "Required parameters is missing!";
        }

        $this->set_response($response, REST_Controller::HTTP_OK);
    }

    public function endcall_post()
    {
        $token = $this->post('token');
        $istrainer = $this->post('istrainer');
        $title = $this->post('title');
        $calltime = $this->post('calltime');
        $use_reward = $this->post('use_reward');
        $response = array("error" => TRUE);
        $waktu = date('Y-m-d H:i:s', now());

        if($token && $title && $calltime){

            $user_id = get_userid($token);
            if($user_id){
                $str_time = $calltime;
                sscanf($str_time, "%d:%d:%d", $hours, $minutes, $seconds);
                $time_seconds = isset($seconds) ? $hours * 3600 + $minutes * 60 + $seconds : $hours * 60 + $minutes;
                $price = 0;
                $calltime = $time_seconds;

                if($istrainer){
                    $query = $this->db->query("
                        SELECT id,vidcallprice FROM `$this->table_trainer` WHERE `user_id`='$user_id' LIMIT 1
                    ");
                }else{
                    $query = $this->db->query("
                        SELECT `t`.`id`, `t`.`vidcallprice` 
                        FROM `$this->table_trainer` AS `t`
                        INNER JOIN `$this->table_users` AS `u` 
                        ON `t`.`user_id`=`u`.`id`
                        WHERE `u`.`name`='$title' LIMIT 1
                    ");
                }

                $row = $query->row();
                if($row){
                    $price = (ceil($calltime/60) * $row->vidcallprice);

                    $query = $this->db->query("
                        SELECT `ru`.`id`, `rr`.`type`, `rr`.`value` 
                        FROM `$this->table_reward_users` AS ru
                        INNER JOIN `$this->table_reward_requir` AS rr
                        ON `ru`.`id_reward`=`rr`.`id_reward`
                        WHERE `ru`.`user_id`='$user_id' AND `ru`.`status`='1' LIMIT 1
                    ");
                    $row_reward = $query->row();
                    if(!$row_reward){
                        $est_time_reward = 0;
                    }else{
                        if($row_reward->type=='videocall'){
                            $est_time_reward = $row_reward->value;
                        }else{
                            $est_time_reward = 0;
                        }
                    }

                    if(($est_time_reward > 0) && $use_reward){
                        $query = $this->db->query("
                            UPDATE `$this->table_reward_users` SET `status`='2'
                            WHERE `user_id`='$user_id' AND `status`='1' LIMIT 1
                        ");
                        $result = $this->db->affected_rows();
                        $price = 0;
                    }else{
                        $query = $this->db->query("SELECT * from `$this->table_wallet` WHERE `user_id`='$user_id' LIMIT 1");
                        $row_wallet = $query->row();
                        if(!$row_wallet){
                            $saldo = 0;
                        }else{
                            $saldo = $row_wallet->saldo;
                        }

                        $saldo_new = $saldo-$price;

                        $query = $this->db->query("
                            UPDATE `$this->table_wallet` SET `saldo`='$saldo_new'
                            WHERE `user_id`='$user_id' LIMIT 1
                        ");
                        $result = $this->db->affected_rows();
                    }

                    $query = $this->db->query("
                        INSERT INTO `$this->table_name` (`id`, `user_id`, `title`, `type`, `datetime`, `calltime`, `price`, `created`, `modified`) 
                        VALUES (NULL, '$user_id', '$title', 'VIDCALL', '$waktu', '$calltime', '$price', '$waktu', '$waktu');
                    ");
                    $result = $this->db->affected_rows();
                        
                    $data["id"] = 0;
                    $data["title"] = $title;
                    $data["type"] = 'VIDCALL';
                    $data["datetime"] = $waktu;
                    $data["calltime"] = date('i:s',$calltime);
                    $data["price"] = $price;

                    $response["error"] = FALSE;
                    $response["data"] = $data;      
                    
                }else{
                    $response["error_msg"] = "trainer not found";
                }

            }else{
                $response["error_msg"] = "unauthenticated";
            }

        } else {
            $response["error"] = TRUE;
            $response["error_msg"] = "Required parameters is missing!";
        }

        $this->set_response($response, REST_Controller::HTTP_OK);
    }

}
