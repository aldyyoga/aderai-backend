<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require APPPATH . '/libraries/REST_Controller.php';

class Merchandise extends REST_Controller {

    private $table_name = 'cms_app_merchandise';
    private $table_category = 'cms_app_merchandise_category';
    private $table_images = 'cms_app_merchandise_images';
    private $table_colors = 'cms_app_merchandise_colors';
    private $table_comment = 'cms_app_merchandise_comment';
    private $table_size = 'cms_app_merchandise_size';
    private $asset_url = 'http://gym.aderai.id/assets/';

    function __construct()
    {
        parent::__construct();
    }

    public function category_get()
    {
        $response = array("error" => TRUE, "data" => array());
        $token = $this->get('token');

        if($token){
            $user_id = get_userid($token);
            if($user_id){
                $response["error"] = FALSE;

                $query = $this->db->query("SELECT * from `$this->table_category` WHERE 1 LIMIT 20");
                foreach ($query->result_array() as $row)
                {   
                    $data["id"] = $row['id'];
                    $data["name"] = $row['name'];
                    array_push($response['data'], $data);
                }

            }else{
                $response["error_msg"] = "unauthenticated";
            }
        }else{
            $response["error_msg"] = "token required!";
        }

        $this->set_response($response, REST_Controller::HTTP_OK);
    }

    public function all_get()
    {
        $response = array("error" => TRUE, "data" => array());
        $token = $this->get('token');
        $category = $this->get('category');

        $page = $this->get('page');
        if(!$page){
            $page = 1;
        }
        $per_page = $this->get('per_page');
        if(!$per_page){
            $per_page = 10;
        }
        if($page > 1){
            $page0 = ($per_page * $page) - $per_page;
            $pagez = $per_page;
        }else{
            $page0 = 0;
            $pagez = $per_page;
        }

        if($token){
            $user_id = get_userid($token);
            if($user_id){
                $response["error"] = FALSE;

                if($category){
                    $this->db->trans_start();
                    $this->db->query("SET sql_mode = ''");
                    $query = $this->db->query("
                        SELECT `m`.*, `i`.`name` AS image
                        from `$this->table_name` AS m
                        LEFT JOIN `$this->table_images` AS i
                        ON `m`.`id`=`i`.`id_merchandise`
                        WHERE id_merchandise_category='$category'
                        GROUP BY `m`.`id` LIMIT $page0, $pagez
                    ");
                    $this->db->trans_complete();          
                }else{
                    $this->db->trans_start();
                    $this->db->query("SET sql_mode = ''");
                    $query = $this->db->query("
                        SELECT `m`.*, `i`.`name` AS image
                        from `$this->table_name` AS m
                        LEFT JOIN `$this->table_images` AS i
                        ON `m`.`id`=`i`.`id_merchandise`
                        WHERE 1 
                        GROUP BY `m`.`id` LIMIT $page0, $pagez
                    ");
                    $this->db->trans_complete(); 
                }

                foreach ($query->result_array() as $row)
                {   
                    $data["id"] = $row['id'];
                    $data["name"] = $row['name'];
                    $data["price"] = $row['price'];
                    $data["image"] = $this->asset_url."merchandise/thumb/".$row['image'];
                    array_push($response['data'], $data);
                }

                $query = $this->db->query("
                    SELECT count(*) AS count from `$this->table_name` WHERE 1
                ");
                $row_count = $query->row();
                $total_data = $row_count->count;

                $numpages = intval($total_data/$per_page);
                $num_pages = $total_data/$per_page;
                if($num_pages > $numpages){
                    $numpages = $numpages+1;
                }
                if($page > $numpages){
                    $response["error_msg"] = "Data not found";
                }

                $response["pagination"]["numpages"] = $numpages;
                $response["pagination"]["limit"] = $per_page;
                $response["pagination"]["total_result"] = $total_data;
                $response["pagination"]["current_page"] = $page;

            }else{
                $response["error_msg"] = "unauthenticated";
            }
        }else{
            $response["error_msg"] = "token required!";
        }

        $this->set_response($response, REST_Controller::HTTP_OK);
    }

    public function detail_get()
    {
        $response = array("error" => TRUE, "data" => array());
        $token = $this->get('token');
        $id = $this->get('id');

        if($token && $id){
            $user_id = get_userid($token);
            if($user_id){

                $query = $this->db->query("
                    SELECT `m`.id, `m`.`name`, `m`.`price`, `m`.`rate`, `m`.`description`
                    from `$this->table_name` AS m
                    WHERE `id`='$id'
                ");
                $row = $query->row();
                if(!$row){
                    $response["error"] = TRUE;
                    $response["error_msg"] = "data kosong";
                    $this->set_response($response, REST_Controller::HTTP_OK);
                    return;
                }

                $response["data"]["id"] = $row->id;
                $response["data"]["name"] = $row->name;
                $response["data"]["price"] = $row->price;
                $response["data"]["rate"] = $row->rate;
                $response["data"]["description"] = $row->description;
                $response["data"]["colors"] = array();
                $response["data"]["size"] = array();
                $response["data"]["images"] = array();

                $query = $this->db->query("
                    SELECT `id`, `code`
                    from `$this->table_colors`
                    WHERE `id_merchandise`='$id'
                ");
                foreach ($query->result_array() as $row)
                {   
                    $data["id"] = $row['id'];
                    $data["code"] = $row['code'];
                    array_push($response['data']['colors'], $data);
                }

                $query = $this->db->query("
                    SELECT `id`, `name`
                    from `$this->table_size`
                    WHERE `id_merchandise`='$id'
                ");
                foreach ($query->result_array() as $row)
                {   
                    $data["id"] = $row['id'];
                    $data["name"] = $row['name'];
                    array_push($response['data']['size'], $data);
                }

                $query = $this->db->query("
                    SELECT `id`, `name`
                    from `$this->table_images`
                    WHERE `id_merchandise`='$id'
                ");
                foreach ($query->result_array() as $row)
                {   
                    $data["id"] = $row['id'];
                    $data["name"] = $this->asset_url."merchandise/".$row['name'];
                    $data["thumb"] = $this->asset_url."merchandise/thumb/".$row['name'];
                    array_push($response['data']['images'], $data);
                }
                $response["error"] = FALSE;
            }else{
                $response["error_msg"] = "unauthenticated";
            }
        }else{
            $response["error_msg"] = "Required parameters is missing!";
        }

        $this->set_response($response, REST_Controller::HTTP_OK);
    }

}
