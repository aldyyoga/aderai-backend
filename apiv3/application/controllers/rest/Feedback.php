<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require APPPATH . '/libraries/REST_Controller.php';

class Feedback extends REST_Controller {

    private $table_name = 'cms_app_feedback';

    function __construct()
    {
        parent::__construct();
    }

    public function submit_post()
    {
        $token = $this->post('token');
        $title = $this->post('title');
        $content = $this->post('content');
        $response = array("error" => TRUE);
        $waktu = date('Y-m-d H:i:s', now());

        if($token && $title && $content){

            $user_id = get_userid($token);
            if($user_id){

                $query = $this->db->query("
                    INSERT INTO `$this->table_name` (`id`, `user_id`, `title`, `content`, `created`, `modified`) 
                    VALUES (NULL, '$user_id', '$title', '$content', '$waktu', '$waktu');
                ");
                $result = $this->db->affected_rows();

                $response["error"] = FALSE;    
                $response["data"] = "success";

            }else{
                $response["error_msg"] = "unauthenticated";
            }

        } else {
            $response["error"] = TRUE;
            $response["error_msg"] = "Required parameters is missing!";
        }

        $this->set_response($response, REST_Controller::HTTP_OK);
    }

}
