<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require APPPATH . '/libraries/REST_Controller.php';

class Fans extends REST_Controller {

    private $table_name = 'cms_app_fans';
    private $table_users = 'cms_app_users';

    function __construct()
    {
        parent::__construct();
    }

    public function all_get()
    {
        $response = array("error" => TRUE, "data" => array());
        $token = $this->get('token');

        $page = $this->get('page');
        if(!$page){
            $page = 1;
        }
        $per_page = $this->get('per_page');
        if(!$per_page){
            $per_page = 10;
        }
        if($page > 1){
            $page0 = ($per_page * $page) - $per_page;
            $pagez = $per_page;
        }else{
            $page0 = 0;
            $pagez = $per_page;
        }

        if($token){
            $user_id = get_userid($token);
            if($user_id){
                $response["error"] = FALSE;

                $query = $this->db->query("
                    SELECT @rownum := @rownum +1 rank, u.id, u.point, u.name, u.image
                    FROM `$this->table_users` u, (SELECT @rownum :=0)r
                    WHERE `u`.`id`='$user_id'
                ");
                $row = $query->row();
                if($row){
                    $data["id"] = $row->id;
                    $data["rank"] = $row->rank;
                    $data["point"] = $row->point;
                    $data["name"] = $row->name;
                    $data["image"] = $row->image;
                    $response['myrank'] = $data;
                }

                $query = $this->db->query("
                    SELECT @rownum := @rownum +1 rank, u.id, u.point, u.name, u.image
                    FROM `$this->table_users` u, (SELECT @rownum :=0)r
                    WHERE 1 ORDER BY `point` DESC LIMIT $page0, $pagez
                ");
                foreach ($query->result_array() as $row)
                {   
                    $data["id"] = $row['id'];
                    $data["rank"] = $row['rank'];
                    $data["point"] = $row['point'];
                    $data["name"] = $row['name'];
                    $data["image"] = $row['image'];
                    array_push($response['data'], $data);
                }

                $query = $this->db->query("
                    SELECT count(*) AS count from `$this->table_users`
                    WHERE 1
                ");
                $row_count = $query->row();
                $total_data = $row_count->count;

                $numpages = intval($total_data/$per_page);
                $num_pages = $total_data/$per_page;
                if($num_pages > $numpages){
                    $numpages = $numpages+1;
                }
                if($page > $numpages){
                    $response["error_msg"] = "Data not found";
                }

                $response["pagination"]["numpages"] = $numpages;
                $response["pagination"]["limit"] = $per_page;
                $response["pagination"]["total_result"] = $total_data;
                $response["pagination"]["current_page"] = $page;

            }else{
                $response["error_msg"] = "unauthenticated";
            }
        }else{
            $response["error_msg"] = "token required!";
        }

        $this->set_response($response, REST_Controller::HTTP_OK);
    }

}
