<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require APPPATH . '/libraries/REST_Controller.php';

class Wallet extends REST_Controller {

    private $table_name = 'cms_app_wallet';

    function __construct()
    {
        parent::__construct();
    }

    public function saldo_get()
    {
        $response = array("error" => TRUE, "data" => array());
        $token = $this->get('token');

        if($token){
            $user_id = get_userid($token);
            if($user_id){

                $query = $this->db->query("SELECT * from `$this->table_name` WHERE `user_id`='$user_id' LIMIT 1");
                $row = $query->row();
                if(!$row){
                    $saldo = 0;
                }else{
                    $saldo = $row->saldo;
                }

                $response["error"] = FALSE;
                $response["data"]["saldo"] = $saldo;

            }else{
                $response["error_msg"] = "unauthenticated";
            }
        }else{
            $response["error_msg"] = "token required!";
        }

        $this->set_response($response, REST_Controller::HTTP_OK);
    }

}
