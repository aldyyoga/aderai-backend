<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require APPPATH . '/libraries/REST_Controller.php';

class Reward extends REST_Controller {

    private $table_name = 'cms_app_reward';
    private $table_users = 'cms_app_users';
    private $table_reward_users = 'cms_app_reward_users';

    function __construct()
    {
        parent::__construct();
    }

    public function all_get()
    {
        $response = array("error" => TRUE, "data" => array());
        $token = $this->get('token');
        $asset_url = $this->config->item('asset_url');

        $page = $this->get('page');
        if(!$page){
            $page = 1;
        }
        $per_page = $this->get('per_page');
        if(!$per_page){
            $per_page = 10;
        }
        if($page > 1){
            $page0 = ($per_page * $page) - $per_page;
            $pagez = $per_page;
        }else{
            $page0 = 0;
            $pagez = $per_page;
        }

        if($token){
            $user_id = get_userid($token);
            if($user_id){
                $response["error"] = FALSE;

                $query = $this->db->query("
                    SELECT * FROM `$this->table_users`
                    WHERE `id`='$user_id'
                ");
                $row_user = $query->row();
                if(!$row_user){
                    $response["error_msg"] = "user not valid";
                    $this->set_response($response, REST_Controller::HTTP_OK);
                    return;
                }
                $response['mypoint'] = $row_user->point;

                $query = $this->db->query("
                    SELECT * from `$this->table_name`
                    ORDER BY id DESC LIMIT $page0, $pagez
                ");
                foreach ($query->result_array() as $row)
                {   
                    $data["id"] = $row['id'];
                    $data["name"] = $row['name'];
                    $data["point"] = $row['point'];
                    $data["image"] = $asset_url."reward/".$row['image'];
                    $data["description"] = $row['description'];
                    $data["howuse"] = $row['howuse'];
                    $data["terms"] = $row['terms'];
                    array_push($response['data'], $data);
                }

                $query = $this->db->query("
                    SELECT count(*) AS count 
                    from `$this->table_name`
                ");
                $row_count = $query->row();
                $total_data = $row_count->count;

                $numpages = intval($total_data/$per_page);
                $num_pages = $total_data/$per_page;
                if($num_pages > $numpages){
                    $numpages = $numpages+1;
                }
                if($page > $numpages){
                    $response["error_msg"] = "Data not found";
                }

                $response["pagination"]["numpages"] = $numpages;
                $response["pagination"]["limit"] = $per_page;
                $response["pagination"]["total_result"] = $total_data;
                $response["pagination"]["current_page"] = $page;

            }else{
                $response["error_msg"] = "unauthenticated";
            }
        }else{
            $response["error_msg"] = "token required!";
        }

        $this->set_response($response, REST_Controller::HTTP_OK);
    }

    public function tukar_post()
    {
        $token = $this->post('token');
        $id = $this->post('id');
        $response = array("error" => TRUE);
        $waktu = date('Y-m-d H:i:s', now());

        if($token && $id){

            $user_id = get_userid($token);
            if($user_id){
                $query = $this->db->query("
                    SELECT * FROM `$this->table_users`
                    WHERE `id`='$user_id'
                ");
                $row_user = $query->row();
                if(!$row_user){
                    $response["error_msg"] = "user not valid";
                    $this->set_response($response, REST_Controller::HTTP_OK);
                    return;
                }

                $query = $this->db->query("
                    SELECT * FROM `$this->table_name`
                    WHERE `id`='$id'
                ");
                $row_reward = $query->row();
                if(!$row_reward){
                    $response["error_msg"] = "user not valid";
                    $this->set_response($response, REST_Controller::HTTP_OK);
                    return;
                }

                $sisa_poin = intval($row_user->point) - intval($row_reward->point);
                if($sisa_poin < 0){
                    $response["error_msg"] = "points not enough";
                    $this->set_response($response, REST_Controller::HTTP_OK);
                    return;
                }

                $query = $this->db->query("
                    INSERT INTO `$this->table_reward_users` (`id`, `user_id`, `id_reward`, `status`, `created`, `modified`) 
                    VALUES (NULL, '$user_id', '$id', '0', '$waktu', '$waktu');
                ");
                $result = $this->db->affected_rows();

                $query = $this->db->query("
                    UPDATE `$this->table_users` 
                    SET `point`='$sisa_poin' 
                    WHERE `id`='$user_id';
                ");
                $result = $this->db->affected_rows();

                $response["error"] = FALSE;    
                $response["data"] = "success";

            }else{
                $response["error_msg"] = "unauthenticated";
            }

        } else {
            $response["error"] = TRUE;
            $response["error_msg"] = "Required parameters is missing!";
        }

        $this->set_response($response, REST_Controller::HTTP_OK);
    }

    public function user_get()
    {
        $response = array("error" => TRUE, "data" => array());
        $token = $this->get('token');
        $asset_url = $this->config->item('asset_url');

        $page = $this->get('page');
        if(!$page){
            $page = 1;
        }
        $per_page = $this->get('per_page');
        if(!$per_page){
            $per_page = 10;
        }
        if($page > 1){
            $page0 = ($per_page * $page) - $per_page;
            $pagez = $per_page;
        }else{
            $page0 = 0;
            $pagez = $per_page;
        }

        if($token){
            $user_id = get_userid($token);
            if($user_id){
                $response["error"] = FALSE;

                $query = $this->db->query("
                    SELECT * from `$this->table_name` AS r
                    INNER JOIN `$this->table_reward_users` AS ru
                    ON `r`.`id`=`ru`.`id_reward`
                    WHERE `ru`.`user_id`='$user_id' AND `ru`.`status`='0'
                    ORDER BY `ru`.`id` DESC LIMIT $page0, $pagez
                ");
                foreach ($query->result_array() as $row)
                {   
                    $data["id"] = $row['id'];
                    $data["name"] = $row['name'];
                    $data["point"] = $row['point'];
                    $data["image"] = $asset_url."reward/".$row['image'];
                    $data["description"] = $row['description'];
                    $data["howuse"] = $row['howuse'];
                    $data["terms"] = $row['terms'];
                    array_push($response['data'], $data);
                }

                $query = $this->db->query("
                    SELECT count(*) AS count 
                    FROM `$this->table_name` AS r
                    INNER JOIN `$this->table_reward_users` AS ru
                    ON `r`.`id`=`ru`.`id_reward`
                    WHERE `ru`.`user_id`='$user_id'
                ");
                $row_count = $query->row();
                $total_data = $row_count->count;

                $numpages = intval($total_data/$per_page);
                $num_pages = $total_data/$per_page;
                if($num_pages > $numpages){
                    $numpages = $numpages+1;
                }
                if($page > $numpages){
                    $response["error_msg"] = "Data not found";
                }

                $response["pagination"]["numpages"] = $numpages;
                $response["pagination"]["limit"] = $per_page;
                $response["pagination"]["total_result"] = $total_data;
                $response["pagination"]["current_page"] = $page;

            }else{
                $response["error_msg"] = "unauthenticated";
            }
        }else{
            $response["error_msg"] = "token required!";
        }

        $this->set_response($response, REST_Controller::HTTP_OK);
    }

    public function gunakan_post()
    {
        $token = $this->post('token');
        $id = $this->post('id');
        $response = array("error" => TRUE);
        $waktu = date('Y-m-d H:i:s', now());

        if($token && $id){

            $user_id = get_userid($token);
            if($user_id){

                $query = $this->db->query("
                    UPDATE `$this->table_reward_users` 
                    SET `status`='1' 
                    WHERE `user_id`='$user_id' AND `id`='$id';
                ");
                $result = $this->db->affected_rows();

                $response["error"] = FALSE;    
                $response["data"] = "success";

            }else{
                $response["error_msg"] = "unauthenticated";
            }

        } else {
            $response["error"] = TRUE;
            $response["error_msg"] = "Required parameters is missing!";
        }

        $this->set_response($response, REST_Controller::HTTP_OK);
    }

}
