<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require APPPATH . '/libraries/REST_Controller.php';

class Exercise extends REST_Controller {

    private $table_name = 'cms_app_exercise';

    function __construct()
    {
        parent::__construct();
    }

    public function all_get()
    {
        $response = array("error" => TRUE, "data" => array());
        $token = $this->get('token');

        $page = $this->get('page');
        if(!$page){
            $page = 1;
        }
        $per_page = $this->get('per_page');
        if(!$per_page){
            $per_page = 10;
        }
        if($page > 1){
            $page0 = ($per_page * $page) - $per_page;
            $pagez = $per_page;
        }else{
            $page0 = 0;
            $pagez = $per_page;
        }

        if($token){
            $user_id = get_userid($token);
            if($user_id){
                $response["error"] = FALSE;

                $query = $this->db->query("SELECT id,title,description,total_like,total_share,total_views,video from `$this->table_name` WHERE 1 LIMIT $page0, $pagez");
                foreach ($query->result_array() as $row)
                {   
                    $data["id"] = $row['id'];
                    $data["title"] = $row['title'];
                    $data["description"] = $row['description'];
                    $data["total_like"] = $row['total_like'];
                    $data["total_share"] = $row['total_share'];
                    $data["total_views"] = $row['total_views'];
                    $data["video"] = $row['video'];
                    array_push($response['data'], $data);
                }

                $query = $this->db->query("
                    SELECT count(*) AS count from `$this->table_name` WHERE 1
                ");
                $row_count = $query->row();
                $total_data = $row_count->count;

                $numpages = intval($total_data/$per_page);
                $num_pages = $total_data/$per_page;
                if($num_pages > $numpages){
                    $numpages = $numpages+1;
                }
                if($page > $numpages){
                    $response["error_msg"] = "Data not found";
                }

                $response["pagination"]["numpages"] = $numpages;
                $response["pagination"]["limit"] = $per_page;
                $response["pagination"]["total_result"] = $total_data;
                $response["pagination"]["current_page"] = $page;

            }else{
                $response["error_msg"] = "unauthenticated";
            }
        }else{
            $response["error_msg"] = "token required!";
        }

        $this->set_response($response, REST_Controller::HTTP_OK);
    }

    public function action_post()
    {
        $token = $this->post('token');
        $id = $this->post('id');
        $type = $this->post('type');
        $response = array("error" => TRUE);
        if($token && $id && $type){
            $user_id = get_userid($token);
            if($user_id){
                $response["error"] = FALSE;
                $query = $this->db->query("SELECT * from `$this->table_name` WHERE id = '$id'");
                $row = $query->row();
                if($row){
                    $data["id"] = $row->id;
                    $data["title"] = $row->title;
                    $data["description"] = $row->description;
                    $data["total_like"] = $row->total_like;
                    $data["total_share"] = $row->total_share;
                    $data["total_views"] = $row->total_views;
                    $data["video"] = $row->video;

                    if($type=='like'){
                        $data['total_like']++;
                        $update_data = $data['total_like'];
                        $query = $this->db->query("
                            UPDATE `$this->table_name` SET `total_like`='$update_data' WHERE `id` = '$id';
                        ");
                        $result = $this->db->affected_rows();
                    }elseif ($type=='share') {
                        $data['total_share']++;
                        $update_data = $data['total_share'];
                        $query = $this->db->query("
                            UPDATE `$this->table_name` SET `total_share`='$update_data' WHERE `id` = '$id';
                        ");
                        $result = $this->db->affected_rows();
                    }elseif ($type=='views') {
                        $data['total_views']++;
                        $update_data = $data['total_views'];
                        $query = $this->db->query("
                            UPDATE `$this->table_name` SET `total_views`='$update_data' WHERE `id` = '$id';
                        ");
                        $result = $this->db->affected_rows();
                    }else{
                        echo json_encode($response);
                        exit();
                    }

                    $response["error"] = FALSE;
                    $response["data"] = $data;
                }
            }else{
                $response["error_msg"] = "unauthenticated";
            }
        } else {
            $response["error"] = TRUE;
            $response["error_msg"] = "Required parameters (token, id, type) is missing!";
        }

        $this->set_response($response, REST_Controller::HTTP_OK);
    }

}
