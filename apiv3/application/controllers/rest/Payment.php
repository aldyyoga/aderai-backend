<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require APPPATH . '/libraries/REST_Controller.php';

class Payment extends REST_Controller {

    private $table_name = 'cms_app_payment';
    private $table_history = 'cms_app_history';
    private $table_paket = 'cms_app_paket_topup';
    private $table_rekening = 'cms_app_rekening';

    function __construct()
    {
        parent::__construct();
    }

    public function action_post()
    {
        $token = $this->post('token');
        $paket_id = $this->post('paket_id');
        $amount = $this->post('amount');
        $rekening_id = $this->post('rekening_id');
        $response = array("error" => TRUE);
        $waktu = date('Y-m-d H:i:s', now());


        if($token && $paket_id && $rekening_id){

            $plus = rand(10,1000);
            if($paket_id==-1){
                if(!$amount){
                    $response["error"] = TRUE;
                    $response["error_msg"] = "parameter amount required!";
                    $this->set_response($response, REST_Controller::HTTP_OK);
                    return;
                }else{
                    $amount = $amount + $plus;
                }
            }else{
                $query = $this->db->query("
                    SELECT * FROM `$this->table_paket` WHERE `id`='$paket_id' LIMIT 1
                ");
                $row = $query->row();
                if($row){
                    $amount = $row->nominal + $plus;
                }else{
                    $response["error"] = TRUE;
                    $response["error_msg"] = "parameter paket_id not valid!";
                    $this->set_response($response, REST_Controller::HTTP_OK);
                    return;
                }
            }

            $user_id = get_userid($token);
            if($user_id){

                $query = $this->db->query("
                    SELECT * FROM `$this->table_name` WHERE `user_id`='$user_id' AND `status`=0 LIMIT 1
                ");
                $row = $query->row();
                if(!$row){

                    $query = $this->db->query("
                        SELECT * FROM `$this->table_rekening` WHERE `id`='$rekening_id' LIMIT 1
                    ");
                    $tbrekening = $query->row();
                    if($tbrekening){
                        $query = $this->db->query("
                            INSERT INTO `$this->table_name` (`id`, `paket_id`, `amount`, `user_id`, `rekening_id`, `status`, `created`, `modified`) 
                            VALUES (NULL, '$paket_id', '$amount', '$user_id', '$rekening_id', '0', '$waktu', '$waktu');
                        ");
                        $result = $this->db->affected_rows();

                        $response["error"] = FALSE;
                        $response["data"]["amount"] = $amount;
                        $response["data"]["bank"]["account_number"] = $tbrekening->account_number;
                        $response["data"]["bank"]["account_number"] = $tbrekening->account_number;
                        $response["data"]["bank"]["account_name"] = $tbrekening->account_name;
                        $response["data"]["bank"]["bank_name"] = $tbrekening->bank_name;
                    }else{
                        $response["error_msg"] = "parameter rekening_id tidak valid";
                    }

                }else{
                    $response["error_msg"] = "belum dibayar";
                }

            }else{
                $response["error_msg"] = "unauthenticated";
            }
        }else{
            $response["error_msg"] = "parameter required!";
        }

        $this->set_response($response, REST_Controller::HTTP_OK);
    }

    public function bayar_get()
    {
        $response = array("error" => TRUE);
        $token = $this->get('token');

        if($token){
            $user_id = get_userid($token);
            if($user_id){
                $response["error"] = FALSE;
                $query = $this->db->query("
                    SELECT * FROM `$this->table_name` WHERE `user_id`='$user_id' AND `status`=0 LIMIT 1
                ");
                $row = $query->row();
                if($row){
                    $query = $this->db->query("
                        SELECT * FROM `$this->table_rekening` WHERE `id`='$row->rekening_id' LIMIT 1
                    ");
                    $tbrekening = $query->row();
                    if($tbrekening){
                        $response["bayar"] = TRUE;
                        $response["data"]["amount"] = $row->amount;
                        $response["data"]["bank"]["account_number"] = $tbrekening->account_number;
                        $response["data"]["bank"]["account_number"] = $tbrekening->account_number;
                        $response["data"]["bank"]["account_name"] = $tbrekening->account_name;
                        $response["data"]["bank"]["bank_name"] = $tbrekening->bank_name;
                    }else{
                        $response["error_msg"] = "rekening_id tidak valid";
                    }
                }else{
                    $response["bayar"] = FALSE;
                }

            }else{
                $response["error_msg"] = "unauthenticated";
            }
        }else{
            $response["error_msg"] = "token required!";
        }

        $this->set_response($response, REST_Controller::HTTP_OK);
    }

    public function paket_get()
    {
        $response = array("error" => TRUE, "data" => array());
        $token = $this->get('token');

        if($token){
            $user_id = get_userid($token);
            if($user_id){
                $response["error"] = FALSE;

                $query = $this->db->query("SELECT * from `$this->table_paket` WHERE 1 LIMIT 10");
                foreach ($query->result_array() as $row)
                {   
                    $data["id"] = $row['id'];
                    $data["nominal"] = $row['nominal'];
                    array_push($response['data'], $data);
                }

            }else{
                $response["error_msg"] = "unauthenticated";
            }
        }else{
            $response["error_msg"] = "token required!";
        }

        $this->set_response($response, REST_Controller::HTTP_OK);
    }

    public function rekening_get()
    {
        $response = array("error" => TRUE, "data" => array());
        $token = $this->get('token');

        if($token){
            $user_id = get_userid($token);
            if($user_id){
                $response["error"] = FALSE;

                $query = $this->db->query("SELECT * from `$this->table_rekening` WHERE 1 LIMIT 10");
                foreach ($query->result_array() as $row)
                {   
                    $data["id"] = $row['id'];
                    $data["account_number"] = $row['account_number'];
                    $data["account_name"] = $row['account_name'];
                    $data["bank_name"] = $row['bank_name'];
                    array_push($response['data'], $data);
                }

            }else{
                $response["error_msg"] = "unauthenticated";
            }
        }else{
            $response["error_msg"] = "token required!";
        }

        $this->set_response($response, REST_Controller::HTTP_OK);
    }

}
