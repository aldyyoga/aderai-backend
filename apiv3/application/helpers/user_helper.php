<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

function get_userid($token)
{
	$table_session = 'cms_app_session';
	$CI =& get_instance();
	$query = $CI->db->query("SELECT * from `$table_session` WHERE token = '$token'");
    $row = $query->row();
    if($row){
    	return $row->user_id;
    }else{
    	return NULL;
    }
}

function humanTiming ($time)
{
    $time = time() - $time; // to get the time since that moment
    $time = ($time<1)? 1 : $time;
    $tokens = array (
        31536000 => 'year',
        2592000 => 'month',
        604800 => 'week',
        86400 => 'day',
        3600 => 'hour',
        60 => 'minute',
        1 => 'second'
    );

    foreach ($tokens as $unit => $text) {
        if ($time < $unit) continue;
        $numberOfUnits = floor($time / $unit);
        return $numberOfUnits.' '.$text.(($numberOfUnits>1)?'s':'');
    }
}