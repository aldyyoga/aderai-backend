<?php
defined('BASEPATH') OR exit('No direct script access allowed');
ini_set('max_execution_time', 300);

class Error_404 extends MX_Controller {
	public function __construct() 
	{	
		parent::__construct();		
	}

	public function index() {
		$view['heading'] = 	'Page Not Found';
		$quote 			 = 	array(
							"A successful team beats with one heart<br/><em>Michael Gokturk</em>",
							"When you innovate, you’ve got to be prepared for everyone telling you you’re nuts<br/><em>Larry Ellison</em>",
							"I am who I am, and I'm focused on that, and being a great CEO of Apple<br/><em>Tim Cook</em>",
							"Without passion, you don’t have any energy, and without energy, you simply have nothing<br><em>Donald Trump</em>",
							"No man can be successful, unless he first loves his work<br><em>David Sarnoff</em>",
							"You don’t need to be a genius or a visionary, or even a college graduate for that matter, to be successful. You just need framework and a dream<br><em>Michael Dell</em>",
							"In business, what’s dangerous is not to evolve<br><em>Jeff Bezos</em>",
							"If you’re changing the world, you’re working on important things. You’re excited to get up in the morning<br><em>Larry Page</em>"
							);
		$view['message'] = $quote[rand(0,6)];		
		$this->load->view('errors/html/error_404', $view);
	}
}