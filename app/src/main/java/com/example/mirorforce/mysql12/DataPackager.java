package com.example.mirorforce.mysql12;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.Iterator;

/**
 * Created by Oclemy on 5/15/2016 for ProgrammingWizards Channel and http://www.camposha.com.
 */
public class DataPackager {

    String name,pass,email,first,last,phone;

    public DataPackager(String name,String pass,String email,String first,String last,String phone) {
        this.name = name;
        this.pass = pass;
        this.email = email;
        this.first = first;
        this.last = last;
        this.phone = phone;
    }



    public String packData()
    {
        JSONObject jo=new JSONObject();
        StringBuffer sb=new StringBuffer();

        try {
            jo.put("username",name);
            jo.put("password",pass);
            jo.put("email",email);
            jo.put("first_name",first);
            jo.put("last_name",last);
            jo.put("phone",phone);

            Boolean firstvalue=true;
            Iterator it=jo.keys();

            do {
                String key=it.next().toString();
                String value=jo.get(key).toString();

                if(firstvalue)
                {
                    firstvalue=false;
                }else
                {
                    sb.append("&");
                }

                sb.append(URLEncoder.encode(key,"UTF-8"));
                sb.append("=");
                sb.append(URLEncoder.encode(value,"UTF-8"));

            }while (it.hasNext());

            return sb.toString();

        } catch (JSONException e) {
            e.printStackTrace();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        return null;
    }
}
