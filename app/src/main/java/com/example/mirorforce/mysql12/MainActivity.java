package com.example.mirorforce.mysql12;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class MainActivity extends AppCompatActivity {

    String urlAddress="https://zolidzonehosting.com/aderai/aderai.php";
    EditText nameTxt,passwordTxt,emailTxt,firstTxt,lastTxt,phoneTxt;
    Button saveBtn;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        nameTxt= (EditText) findViewById(R.id.nameEditTxt);
        passwordTxt= (EditText) findViewById(R.id.passwordEditTxt);
        emailTxt= (EditText) findViewById(R.id.emailEditTxt);
        firstTxt= (EditText) findViewById(R.id.firstnameEditTxt);
        lastTxt= (EditText) findViewById(R.id.lastnameEditTxt);
        phoneTxt= (EditText) findViewById(R.id.phoneEditTxt);
        saveBtn= (Button) findViewById(R.id.saveBtn);

        saveBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Sender s=new Sender(MainActivity.this,urlAddress,nameTxt,passwordTxt,emailTxt,firstTxt,lastTxt,phoneTxt);
                s.execute();
            }
        });

    }
}
