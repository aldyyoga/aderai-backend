<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pembayaran extends MX_Controller {

    private $table_name     = 'app_payment';
    private $table_wallet   = 'app_wallet';
    private $url            = 'pembayaran';
    private $perpage        = 5;
    private $data           = array();
    private $update         = '';
    private $delete         = '';

    public function __construct()
    {
        parent::__construct();

        // for security check
        $this->menu->has_access();

        // for header
        $this->data          = $this->header->button_header();
        $this->data['title'] = 'Pembayaran';

        // for button link
        $this->data['button_link'] = (($this->uri->segment(2) == 'add') || ($this->uri->segment(2) == 'update')) ? site_url($this->url) : site_url($this->url).'/add' ;

        // for debug purpose only
        $this->output->enable_profiler(false);
    }

    public function index( $offset = 0 )
    {
        // pagination 
        $this->db
        ->select('a.id, b.name, a.amount, c.bank_name, a.status')
        ->from($this->table_name.' a')
        ->join('cms_app_users b','a.user_id=b.id','left')
        ->join('cms_app_rekening c','c.id=a.rekening_id','left')
        ->order_by('b.name');
        $query = $this->db->get();
        $count                      = $query->num_rows();
        $config['per_page']         = $this->perpage;
        $this->pagination->initialize($this->general_model->pagination_rules(site_url($this->url), $count, $this->perpage));
        $this->data['pagination']   = $this->pagination->create_links();

        // get all data
        $this->db
        ->select('a.id, b.name, a.amount, c.bank_name, a.status')
        ->from($this->table_name.' a')
        ->join('cms_app_users b','a.user_id=b.id','left')
        ->join('cms_app_rekening c','c.id=a.rekening_id','left')
        ->order_by('b.name')
        ->limit($this->perpage, $offset);
        $query = $this->db->get();

        // if not empty
        if($query->num_rows() > 0) {
            // return to table data
            $sort = array();
            for($i=0; $i < $this->db->count_all($this->table_name); $i++) { $sort[$i] = $i; }

            foreach($query->result() as $list) {
                // check for update access
                if($this->menu->crud_access('update')) {
                    $this->update = '<a href="'.site_url($this->url.'/'.'update'.'/'.$list->id).'" class="btn btn-xs btn-default"><i class="fa fa-pencil"></i></a>';
                }

                // return to table data
                $table_row = array(
                    $list->name,
                    $list->amount,                 
                    $list->bank_name,
                    $list->status,
                );

                // check for additional link access
                if($this->menu->crud_access('update') || $this->menu->crud_access('delete')) {
                    $table_row = array_merge($table_row, array($this->delete.' '.$this->update));
                }

                $this->table->add_row($table_row);
            }

            // generate table header
            $this->table->set_template(array('table_open' => '<table class="table">'));
            if($this->menu->crud_access('update') || $this->menu->crud_access('delete')) {
                $this->table->set_heading('Nama', 'Jumlah', 'Nama Bank', 'Status pembayaran','');
            } else {
                $this->table->set_heading('Nama', 'Jumlah', 'Nama Bank', 'Status pembayaran');
            }

            // output
            $this->data['list']         = $this->table->generate(); 
            $this->data['content']      = $this->load->view('admin/list', $this->data, TRUE);
        } else {
            // if no data
            $this->data['content']      = $this->load->view('admin/blank', $this->data, TRUE);                    
        }

        // make an output
        $this->load->view('admin/content', $this->data);
    }

    public function update( $id = NULL )
    {
        // security check
        $this->menu->check_access('update');

        // get current data
        $query = $this->db->get_where($this->table_name, array('id'=> $id));

        // if no data
        if($query->num_rows() <= 0 ) {
            return show_error('Your data may has been removed or changes by someone.', '' , 'Data Not Found');
            exit;
        }

        // return data
        $this->data['form']     = $query->row();
        $this->data['content']  = $this->load->view('pembayaran/edit', $this->data, true);       

        // validation rules
        $this->form_validation->set_rules('status', 'Status pembayaran', 'required');
    

        // if break the rules
        if ($this->form_validation->run() == FALSE) {
            $this->data['content'] = $this->load->view('pembayaran/edit', $this->data, true);
        } else {
            // if not break the rules 
            $record =   array(
                'status' => $this->input->post('status')
            );

            // start transaction
            $this->db->trans_start();
            $this->db->where('id', $this->input->post('id'));
            $this->db->update($this->table_name, $record);

            $query_wallet = $this->db->get_where($this->table_wallet, array('user_id'=> $this->data['form']->user_id));
            if(intval($this->input->post('status')) == 1){
                if($query_wallet->num_rows() <= 0 ) {
                    $record_wallet =   array(
                        'user_id'  => $this->data['form']->user_id,
                        'saldo'    => $this->data['form']->amount
                    );
                    $this->db->insert($this->table_wallet, $record_wallet);
                }else{
                    $record_wallet =   array(
                        'saldo'    => (intval($query_wallet->row()->saldo) + intval($this->data['form']->amount))
                    );
                    $this->db->where('user_id', $this->data['form']->user_id);
                    $this->db->update($this->table_wallet, $record_wallet);
                }
            }else{
                if($query_wallet->num_rows() > 0 ) {
                    $record_wallet =   array(
                        'saldo'    => (intval($query_wallet->row()->saldo) - intval($this->data['form']->amount))
                    );
                    $this->db->where('user_id', $this->data['form']->user_id);
                    $this->db->update($this->table_wallet, $record_wallet);
                }
            }

            $this->db->trans_complete();

            // check for status
            if ($this->db->trans_status() === FALSE) {
                $db_error               = $this->db->error();
                $this->data['message']  = $db_error['message'];
                $this->data['content']  = $this->load->view('pembayaran/edit', $this->data, true);
            } else {
                $direct                     = site_url($this->url);
                $this->data['button_link']  = $direct;
                $this->data['message']      = 'Your record has been updated.';
                $this->data['content']      = $this->load->view('admin/redirect', $this->data, TRUE);
            }
        }

        // make an output
        $this->load->view('admin/content', $this->data);        

    }

}    

/* End of file Packages.php */
/* Location: ./application/modules/admin/controllers/Packages.php */
