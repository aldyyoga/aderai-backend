<?php if(isset($message)) : ?>
<div class="alert alert-danger">
    <?php echo $message; ?>
</div>
<?php endif ?>
<?php if(validation_errors()) : ?>
<div class="alert alert-danger">
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
    <strong>Error</strong>
    <hr>
    <?php echo validation_errors(); ?>
</div>
<?php endif ?>
<?php echo form_open(current_url(), 'class="form-horizontal" role="form"');?>
<input type="hidden" name="id" value="<?php echo $form->id ?>">
<div class="box">
    <div class="box-header with-border">
        <h3 class="box-title">Please complete the form below.</h3>
    </div>
    <div class="box-body tm-padding">            
        <div class="form-group">
            <label for="" class="col-sm-4">User Name</label>
            <div class="col-sm-8">
            <?php echo form_input('name',set_value('name', $form->name),'class="form-control" disabled');?>
            </div>
        </div>
             
        <div class="form-group">
            <label for="" class="col-sm-4">Email</label>
            <div class="col-sm-8">
            <?php echo form_input('email',set_value('email', $form->email),'class="form-control" disabled');?>
            </div>
        </div>  

        <div class="form-group">
            <label for="" class="col-sm-4">Status</label>
            <div class="col-sm-8">
                <select class="form-control" name="status">
                    <option value="">Choose Status</option>
                    <option value="0" <?php if ($form->status==0) {echo "selected";} ?>>Offline</option>
                    <option value="1" <?php if ($form->status==1) {echo "selected";} ?>>Online</option>
                    <option value="2" <?php if ($form->status==2) {echo "selected";} ?>>Bussy</option>
                </select>
            </div>
        </div>
        <div class="form-group">
            <label for="" class="col-sm-4">Video Call Price</label>
            <div class="col-sm-8">
            <?php echo form_input('price',set_value('price', $form->vidcallprice),'class="form-control"');?>
            <span id="" class="help-block ">Add without ' , ' or ' . '</span>
            </div>
        </div>                              
</div>
<div class="panel-footer text-right">
    <a href="<?php echo site_url('trainer') ?>" class="btn tm-btn">Cancel</a>
    <button type="submit" class="btn tm-btn">Submit</button>
</div>
</div>
<?php echo form_close();?>
