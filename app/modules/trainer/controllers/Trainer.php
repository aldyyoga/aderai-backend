<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Trainer extends MX_Controller {

    private $table_name2     = 'app_trainer';
    private $table_name      = 'app_users';
    private $url            = 'trainer';
    private $perpage        = 10;
    private $data           = array();
    private $update         = '';
    private $delete         = '';

    public function __construct()
    {
        parent::__construct();

        // for security check
        $this->menu->has_access();

        // for header
        $this->data          = $this->header->button_header();
        $this->data['title'] = 'Trainer';

        // for button link
        $this->data['button_link'] = (($this->uri->segment(2) == 'add') || ($this->uri->segment(2) == 'update')) ? site_url($this->url) : site_url($this->url).'/add' ;

        // for debug purpose only
        $this->output->enable_profiler(false);
    }

    public function index( $offset = 0 )
    {
        // pagination 
        $this->db
        ->select('app_users.id, name, email')
        ->from($this->table_name)
        ->join($this->table_name2,'app_users.id = app_trainer.user_id');
        $query = $this->db->get();
        $count                      = $query->num_rows();
        $config['per_page']         = $this->perpage;
        $this->pagination->initialize($this->general_model->pagination_rules(site_url($this->url), $count, $this->perpage));
        $this->data['pagination']   = $this->pagination->create_links();

        // get all data
        $this->db
        ->select('app_users.id, name, email,status,vidcallprice')
        ->from($this->table_name)
        ->join($this->table_name2,'app_users.id = app_trainer.user_id')
        ->order_by('name')
        ->limit($this->perpage, $offset);
        $query = $this->db->get();

        // if not empty
        if($query->num_rows() > 0) {
            // return to table data
            $sort = array();
            for($i=0; $i < $this->db->count_all($this->table_name); $i++) { $sort[$i] = $i; }

            foreach($query->result() as $list) {
                // check for update access
                if($this->menu->crud_access('update')) {
                    $this->update = '<a href="'.site_url($this->url.'/'.'update'.'/'.$list->id).'" class="btn btn-xs btn-default"><i class="fa fa-pencil"></i></a>';
                }

                // check for delete access
                if($this->menu->crud_access('delete')) {
                    $this->delete = '<a href="'.site_url($this->url.'/'.'delete'.'/'.$list->id).'" class="delete btn btn-xs btn-default"><i class="fa fa-trash"></i></a>';
                }
                if ($list->status==0) {
                    $status = 'Offline';
                }else if($list->status==1){
                    $status = 'Online';
                }else{
                    $status = 'Bussy';
                }
                //make to IDR
                $price = $this->makeidr($list->vidcallprice);

                // return to table data
                $table_row = array(
                             $list->name, 
                             $list->email,
                             $status,
                             $price,
                );

                // check for additional link access
                if($this->menu->crud_access('update') || $this->menu->crud_access('delete')) {
                    $table_row = array_merge($table_row, array($this->delete.' '.$this->update));
                }

                $this->table->add_row($table_row);
            }

            // generate table header
            $this->table->set_template(array('table_open' => '<table class="table">'));
            if($this->menu->crud_access('update') || $this->menu->crud_access('delete')) {
                $this->table->set_heading('Nama', 'Email', 'Status','Video Call Price/min','Action', '');
            } else {
                $this->table->set_heading('Nama', 'Email','Status', 'Video Call Price/min','Action');
            }

            // output
            $this->data['list']         = $this->table->generate(); 
            $this->data['content']      = $this->load->view('admin/list', $this->data, TRUE);
        } else {
            // if no data
            $this->data['content']      = $this->load->view('admin/blank', $this->data, TRUE);                    
        }

        // make an output
        $this->load->view('admin/content', $this->data);
    }

    public function add( $offset = 0 )
    {
        // pagination 
        $this->db
        ->select('id, name, email')
        ->from($this->table_name);
        $query = $this->db->get();
        $count                      = $query->num_rows();
        $config['per_page']         = $this->perpage;
        $this->pagination->initialize($this->general_model->pagination_rules(site_url($this->url.'/add/'), $count, $this->perpage));
        $this->data['pagination']   = $this->pagination->create_links();

        // get all data
        $this->db
        ->select('app_users.id, name, email,status')
        ->from($this->table_name)
        ->join($this->table_name2,'app_users.id = app_trainer.user_id','left')
        ->order_by('name')
        ->limit($this->perpage, $offset);
        $query = $this->db->get();

        // if not empty
        if($query->num_rows() > 0) {
            // return to table data
            $sort = array();
            for($i=0; $i < $this->db->count_all($this->table_name); $i++) { $sort[$i] = $i; }

            foreach($query->result() as $list) {
                // check for update access
                $table_row = array(
                    ($list->status !=NULL) ? '<strong>'.$list->name.'</strong>' : $list->name ,
                    ($list->status !=NULL) ? '<strong>'.$list->email.'</strong>' : $list->name ,
                    ($list->status !=NULL) ? '<strong>Trainer</strong>' : '<a href="#" class="default" data-id="'.$list->id.'">Set as trainer</a>'
                );

                $this->table->add_row($table_row);
            }

            // generate table header
            $this->table->set_template(array('table_open' => '<table class="table">'));
            if($this->menu->crud_access('update') || $this->menu->crud_access('delete')) {
                $this->table->set_heading('Nama', 'Email', 'Action', '');
            } else {
                $this->table->set_heading('Nama', 'Email', 'Action');
            }

            // output
            $this->data['list']         = $this->table->generate(); 
            $this->data['content']      = $this->load->view('admin/list', $this->data, TRUE);
        } else {
            // if no data
            $this->data['content']      = $this->load->view('admin/blank', $this->data, TRUE);                    
        }
        // script
        $this->data['scripts'] = '
            $(".default").click(function(){
                $.post("'.site_url($this->url.'/ajax_default').'",{ "id" : $(this).data("id")})
                .done(function(){
                    location.reload();
                })
            });
        ';

        // make an output
        $this->load->view('admin/content', $this->data);
    }


 //Update one item
    public function update( $id = NULL )
    {
        // security check
        $this->menu->check_access('update');

        // get current data
        $this->db
        ->select('app_users.id, name, email,status,vidcallprice')
        ->from($this->table_name)
        ->join($this->table_name2,'app_users.id = app_trainer.user_id')
        ->where('app_users.id',$id);
        $query = $this->db->get();

        // if no data
        if($query->num_rows() <= 0 ) {
            return show_error('Your data may has been removed or changes by someone.', '' , 'Data Not Found');
            exit;
        }

        // return data
        $this->data['form']     = $query->row();
        $this->data['content']  = $this->load->view('trainer/edit', $this->data, true);       

        // validation rules
        $this->form_validation->set_rules('status', 'Status', 'required');
        

        // if break the rules
        if ($this->form_validation->run() == FALSE) {
            $this->data['content'] = $this->load->view('trainer/edit', $this->data, true);
        } else {
            // if not break the rules 
            $record =   array(
                        'status'        => $this->input->post('status'),
                        'vidcallprice'        => $this->input->post('price'),
                        'modified'      => date("Y-m-d H:i:s")
            );

            // start transaction
            $this->db->trans_start();
            $this->db->where('user_id', $id);
            $this->db->update($this->table_name2, $record);
            $this->db->trans_complete();

            // check for status
            if ($this->db->trans_status() === FALSE) {
                $db_error               = $this->db->error();
                $this->data['message']  = $db_error['message'];
                $this->data['content']  = $this->load->view('trainer/edit', $this->data, true);
            } else {
                $direct                     = site_url($this->url);
                $this->data['button_link']  = $direct;
                $this->data['message']      = 'Your record has been updated.';
                $this->data['content']      = $this->load->view('admin/redirect', $this->data, TRUE);
            }
        }

        // make an output
        $this->load->view('admin/content', $this->data);        

    }

    //Delete one item
    public function delete( $id = NULL )
    {
        // security check
        $this->menu->check_access('delete');

        $this->db->trans_start();
        $this->db->where('user_id', $id);
        $this->db->delete($this->table_name2);
        $this->db->trans_complete();
        if ($this->db->trans_status() === FALSE) {
            $message = 'Fail while removed your record.';
        } else {
            $message = 'Your record has been removed.';
        }

        $this->data['message']          = $message;
        $this->data['content']          = $this->load->view('admin/redirect', $this->data, TRUE);
        $this->load->view('admin/content', $this->data);        
    }

    public function ajax_default()
    {
        $record =   array(
                    'user_id'       => $this->input->post('id'),
                    'status'        => 0,
                    'created'       => date("Y-m-d H:i:s"),
                    'modified'      => date("Y-m-d H:i:s")
        );

        // start transaction
        $this->db->trans_start();
        $this->db->insert($this->table_name2, $record);
        $this->db->trans_complete();

        // set all is_default value to 0
        /*$this->db->update($this->table_name, array('is_default' => 0));

        // update selected is_default
        $this->db->where('id', $this->input->post('id'));
        $this->db->update($this->table_name, array('is_default' => 1));*/
        echo $this->input->post('id');
    }
    public function makeidr($price='')
    {
        $value = "Rp " . number_format($price,2,',','.');
        return $value;
    }
   
}

/* End of file Packages.php */
/* Location: ./application/modules/admin/controllers/Packages.php */
