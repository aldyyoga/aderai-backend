<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Video extends MX_Controller {

    private $table_name     = 'app_video';
    private $table_name_2   = 'app_video_category';
    private $url            = 'video';
    private $perpage        = 10;
    private $data           = array();
    private $update         = '';
    private $delete         = '';

    public function __construct()
    {
        parent::__construct();

        // for security check
        $this->menu->has_access();

        // for header
        $this->data          = $this->header->button_header();
        $this->data['title'] = 'Video';

        // for button link
        $this->data['button_link'] = (($this->uri->segment(2) == 'add') || ($this->uri->segment(2) == 'update')) ? site_url($this->url) : site_url($this->url).'/add';
        

        //for debug set as true
        $this->output->enable_profiler(false); 

    }

    public function index( $offset = 0 )
    {
        // pagination 
        $this->db
        ->select('app_video.id,app_video.title, price')
        ->from($this->table_name)
        ->join($this->table_name_2, 'id_category = app_video_category.id','left');
        $query = $this->db->get();
        $count                      = $query->num_rows();
        $config['per_page']         = $this->perpage;
        $this->pagination->initialize($this->general_model->pagination_rules(site_url($this->url), $count, $this->perpage));
        $this->data['pagination']   = $this->pagination->create_links();

        // get all data
        $this->db
        ->select('app_video.id,app_video.title,app_video_category.name, price')
        ->from($this->table_name)
        ->join($this->table_name_2, 'id_category = app_video_category.id','left')
        ->order_by('app_video_category.id')
        ->limit($this->perpage, $offset);
        $query = $this->db->get();

        // if not empty
        if($query->num_rows() > 0) {
            // return to table data
            $sort = array();

            for($i=0; $i < $this->db->count_all($this->table_name); $i++) { $sort[$i] = $i; }

            foreach($query->result() as $list) {
                // check for update access
                if($this->menu->crud_access('update')) {
                    $this->update = '<a href="'.site_url($this->url.'/'.'update'.'/'.$list->id).'" class="btn btn-xs btn-default"><i class="fa fa-pencil"></i></a>';
                }

                // check for delete access
                if($this->menu->crud_access('delete')) {
                    $this->delete = '<a href="'.site_url($this->url.'/'.'delete'.'/'.$list->id).'" class="delete btn btn-xs btn-default"><i class="fa fa-trash"></i></a>';
                }

                $price = $this->makeidr($list->price);
   
                // return to table data
                $table_row = array(
                             $list->title,
                             $list->name,
                             $price,
                        
                );

                // check for additional link access
                if($this->menu->crud_access('update') || $this->menu->crud_access('delete')) {
                    $table_row = array_merge($table_row, array($this->delete.' '.$this->update));
                }

                $this->table->add_row($table_row);
            }

            // generate table header
            $this->table->set_template(array('table_open' => '<table class="table">'));
            if($this->menu->crud_access('update') || $this->menu->crud_access('delete')) {
                $this->table->set_heading('Video\'s Name', 'Video\'s Category', 'Price', 'Action', '');
            } else {
                $this->table->set_heading('Video\'s Name', 'Video\'s Category', 'Price', 'Action');
            }

            // output
            $this->data['list']         = $this->table->generate(); 
            $this->data['content']      = $this->load->view('admin/list', $this->data, TRUE);
        } else {
            // if no data
            $this->data['content']      = $this->load->view('admin/blank', $this->data, TRUE);

        }

        // make an output
        $this->load->view('admin/content', $this->data);
    }


    // Add a new item
    public function add()   
    {
        // check for write access
        $this->menu->check_access('write');

        // validation rules
        $this->form_validation->set_rules('title', 'Video\'s title', 'required');
        $this->form_validation->set_rules('id_category', 'Category', 'required');
        $this->form_validation->set_rules('price', 'Price', 'required');
        $this->form_validation->set_rules('description', 'description', 'required');

        //add catagory list to form 
        $this->db
        ->select('id, name')
        ->from($this->table_name_2);
        $query = $this->db->get();
        $this->data['videocat'] =$query->result_array();


        // if break the rules
        if ($this->form_validation->run() == FALSE) {
            $this->data['content'] = $this->load->view('video/add', $this->data, true);
        } else {
            
            // check for status
            if ($this->db->trans_status() === FALSE) {
                $db_error               = $this->db->error();
                $this->data['message']  = $db_error['message'];
                $this->data['content']  = $this->load->view('video/add', $this->data, true);
            } else {
                if (isset($_FILES['video']['name']) && $_FILES['video']['name'] != '') {
                    unset($config);

                    $date = date("ymd");
                    $configVideo['upload_path'] = '././assets/video/';
                    $configVideo['max_size'] = '90000';
                    $configVideo['allowed_types'] = 'avi|flv|wmv|mp3|mp4';
                    $configVideo['overwrite'] = FALSE;
                    $configVideo['remove_spaces'] = TRUE;
                    $video_name = str_replace(' ', '_',$_FILES['video']['name']);
                    $video_name = $date.$video_name;
                    $configVideo['file_name'] = $video_name;

                    $this->load->library('upload', $configVideo);
                    $this->upload->initialize($configVideo);
                    if(!$this->upload->do_upload('video')) {

                        echo 'Error Upload Video = '.$this->upload->display_errors();exit();
                    }else{
                        $uploadData['video']= $configVideo['file_name'];

                        $data = array();
                        if(!empty($_FILES['userFiles']['name'])){
                        unset($config);

                        $_FILES['userFile']['name'] = $_FILES['userFiles']['name'];
                        $_FILES['userFile']['type'] = $_FILES['userFiles']['type'];
                        $_FILES['userFile']['tmp_name'] = $_FILES['userFiles']['tmp_name'];
                        $_FILES['userFile']['error'] = $_FILES['userFiles']['error'];
                        $_FILES['userFile']['size'] = $_FILES['userFiles']['size'];

                        $new_name = str_replace(' ', '_',$_FILES['userFile']['name']);
                        $new_name = time().'_'.$new_name;
                        $config['file_name'] = $new_name;
                        $config['max_size'] = '5000';

                        $uploadPath = '././assets/video/thumb/';
                        $config['upload_path'] = $uploadPath;
                        $config['allowed_types'] = 'gif|jpg|png';
                        
                        $this->load->library('upload', $config);
                        $this->upload->initialize($config);
                            if($this->upload->do_upload('userFile')){
                                unset($config);

                                $fileData = $this->upload->data();
                                $uploadData['image'] = $fileData['file_name'];
                                $uploadData['created'] = date("Y-m-d H:i:s");
                                $uploadData['modified'] = date("Y-m-d H:i:s");
                                $uploadData['title'] = $this->input->post('title');
                                $uploadData['id_category'] = $this->input->post('id_category');
                                $uploadData['description'] = $this->input->post('description');
                                $uploadData['price'] = $this->input->post('price');

                                $this->db->insert($this->table_name, $uploadData);
                                $id = $this->db->insert_id();
                            }else{
                                echo 'Error Upload Image = '.$this->upload->display_errors();exit();
                            }

                        }

                    }/* video sukses*/

                }else{
                    $this->data['message']  = 'Video and thumbnail required';
                    $this->data['content']  = $this->load->view('video/add', $this->data, TRUE);
                    return $this->load->view('admin/content', $this->data);
                }

                if ($this->db->trans_status() === FALSE) {
                    $db_error               = $this->db->error();
                    $this->data['message']  = $db_error['message'];
                    $this->data['content']  = $this->load->view('video/add', $this->data, true);
                } else {
                    $this->data['message']  = 'Your record has been saved.';
                    $this->data['content']  = $this->load->view('admin/redirect', $this->data, TRUE);
                }

            }
            
        }


        // make an output
        
        $this->load->view('admin/content', $this->data);        
    }

    



    //Update one item
    public function update( $id = NULL )
    {
        // security check
         $this->menu->check_access('update');

        // get current data
        $this->db
        ->select('id, name')
        ->from($this->table_name_2);
        $query = $this->db->get();
        $this->data['videocat'] =$query->result_array();

        $query_v = $this->db->get_where($this->table_name, array('id'=> $id));

        // if no data
        if($query_v->num_rows() <= 0 ) {
            return show_error('Your data may has been removed or changes by someone.', '' , 'Data Not Found');
            exit;
        }

        // return data
        $this->data['formv']     = $query_v->row();

        $old = $this->data['formv'];
        
        // validation rules
        $this->form_validation->set_rules('title', 'Video\'s title', 'required');
        $this->form_validation->set_rules('id_category', 'Category', 'required');
        $this->form_validation->set_rules('price', 'Price', 'required');
        $this->form_validation->set_rules('description', 'description', 'required');

        // if break the rules
        if ($this->form_validation->run() == FALSE) {
            $this->data['content'] = $this->load->view('video/edit', $this->data, true);
        } else {

           // check for status
            if ($this->db->trans_status() === FALSE) {
                $db_error               = $this->db->error();
                $this->data['message']  = $db_error['message'];
                $this->data['content']  = $this->load->view('video/edit', $this->data, true);
            } else {
                $value = array();
                $value['modified'] = date("Y-m-d H:i:s");
                $value['title'] = $this->input->post('title');
                $value['id_category'] = $this->input->post('id_category');
                $value['description'] = $this->input->post('description');
                $value['price'] = $this->input->post('price');

                if (isset($_FILES['video']['name']) && $_FILES['video']['name'] != '') {
                    unset($config);

                    $date = date("ymd");
                    $configVideo['upload_path'] = '././assets/video/';
                    $configVideo['max_size'] = '90000';
                    $configVideo['allowed_types'] = 'avi|flv|wmv|mp3|mp4';
                    $configVideo['overwrite'] = FALSE;
                    $configVideo['remove_spaces'] = TRUE;
                    $video_name = str_replace(' ', '_',$_FILES['video']['name']);
                    $video_name = $date.$video_name;
                    $configVideo['file_name'] = $video_name;

                    $this->load->library('upload', $configVideo);
                    $this->upload->initialize($configVideo);

                    if(!$this->upload->do_upload('video')) {

                        echo 'Error Upload Video = '.$this->upload->display_errors();exit();

                    }else{

                        $value['video']= $configVideo['file_name'];

                        $filename = '././assets/video/'.$old->video;
                        if (file_exists($filename)) {
                            unlink(FCPATH.'././assets/video/'.$old->video);
                        } 


                    }/* video sukses*/

                }/*end of video*/

                $data = array();
                if(!empty($_FILES['userFiles']['name'])){
                unset($config);

                $_FILES['userFile']['name'] = $_FILES['userFiles']['name'];
                $_FILES['userFile']['type'] = $_FILES['userFiles']['type'];
                $_FILES['userFile']['tmp_name'] = $_FILES['userFiles']['tmp_name'];
                $_FILES['userFile']['error'] = $_FILES['userFiles']['error'];
                $_FILES['userFile']['size'] = $_FILES['userFiles']['size'];

                $new_name = str_replace(' ', '_',$_FILES['userFile']['name']);
                $new_name = time().'_'.$new_name;
                $config['file_name'] = $new_name;
                $config['max_size'] = '5000';

                $uploadPath = '././assets/video/thumb/';
                $config['upload_path'] = $uploadPath;
                $config['allowed_types'] = 'gif|jpg|png';
                    
                $this->load->library('upload', $config);
                $this->upload->initialize($config);
                    if($this->upload->do_upload('userFile')){
                        unset($config);

                        $fileData = $this->upload->data();
                        $value['image'] = $fileData['file_name'];

                        /*hapus data*/
                        
                       $filename = '././assets/video/thumb/'.$old->image;
                        if (file_exists($filename)) {
                            unlink(FCPATH.'././assets/video/thumb/'.$old->image);
                        } 
                        
                        
                    }else{
                        echo 'Error Upload Image = '.$this->upload->display_errors();exit();
                    }

                }
                
                
                $this->db->trans_start();
                $this->db->where('id', $id);
                $this->db->update($this->table_name, $value);
                $this->db->trans_complete();



                if ($this->db->trans_status() === FALSE) {
                    $db_error               = $this->db->error();
                    $this->data['message']  = $db_error['message'];
                    $this->data['content']  = $this->load->view('video/add', $this->data, true);
                } else {
                    $this->data['message']  = 'Your record has been saved.';
                    $this->data['content']  = $this->load->view('admin/redirect', $this->data, TRUE);
                }

            }
            
        }


        // make an output
        $this->load->view('admin/content', $this->data);        

    }

    //Delete one item
    public function delete( $id = NULL )
    {
        // security check
         $this->menu->check_access('delete');

            $query_v = $this->db->get_where($this->table_name, array('id'=> $id));
            $this->data['formv'] = $query_v->row();
            $old = $this->data['formv'];

            $filename = '././assets/video/thumb/'.$old->image;
            if (file_exists($filename)) {
                unlink(FCPATH.'././assets/video/thumb/'.$old->image);
            }
            $filename = '././assets/video/'.$old->video;
            if (file_exists($filename)) {
                unlink(FCPATH.'././assets/video/'.$old->video);
            }

            $this->db->trans_start();
            $this->db->where('id', $id);
            $this->db->delete('cms_app_video');
            $this->db->trans_complete(); 


        if ($this->db->trans_status() === FALSE) {
            $message = 'Fail while removed your record.';
        } else {
            $message = 'Your record has been removed.';
        }

        $this->data['message']          = $message;
        $this->data['content']          = $this->load->view('admin/redirect', $this->data, TRUE);
        $this->load->view('admin/content', $this->data);        
    }

    public function makeidr($price='')
    {
        $value = "Rp " . number_format($price,2,',','.');
        return $value;
    }

}