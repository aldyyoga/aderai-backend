<?php if(isset($message)) : ?>
<div class="alert alert-danger">
    <?php echo $message; ?>
</div>
<?php endif ?>

<?php if(validation_errors()) : ?>
<div class="alert alert-danger">
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
    <strong>Error</strong>
    <hr>
    <?php echo validation_errors(); ?>
</div>
<?php endif ?>


<?php echo form_open_multipart(current_url(), 'class="form-horizontal" role="form"');?>
<div class="box">
    <div class="box-header with-border">
        <h3 class="box-title">Please complete the form below.</h3>
    </div>
    <div class="box-body tm-padding">            
        <div class="form-group">
            <label for="" class="col-sm-4">Video Title</label>
            <div class="col-sm-8">
            <?php echo form_input('title',set_value('title'),'class="form-control"');?>
            </div>
        </div>
        <div class="form-group">
            <label for="" class="col-sm-4">Category</label>
            <div class="col-sm-8">
                <select class="form-control" name="id_category">
                    <option value="">Choose Category</option>
                    <?php foreach ($videocat as $key => $value): ?>
                    <option value="<?php echo $value['id']; ?>"><?php echo $value['name']; ?></option>
                    <?php endforeach ?>
                </select>
            </div>
        </div>
        <div class="form-group">
            <label for="" class="col-sm-4">Price</label>
            <div class="col-sm-4">
                <input type="text" class="form-control" placeholder="1.000.000" name="price">
                <span id="" class="help-block ">Add without ' , ' or ' . '</span>
            </div>
        </div>
        <div class="form-group">
            <label for="" class="col-sm-4">Thumbnail</label>
            <div class="col-sm-4">
                <input type="file" class="form-control" name="userFiles"/>
                <span id="" class="help-block ">Add with gif|jpg|png</span>
            </div>
        </div>
        <div class="form-group">
            <label for="" class="col-sm-4">Video</label>
            <div class="col-sm-4">
                <input type="file" class="form-control" name="video"/>
                <span id="" class="help-block ">Add with avi|flv|wmv|mp3|mp4</span>
            </div>
        </div>
        

        <div class="form-group">
            <label for="" class="col-sm-4">Description</label>
            <div class="col-sm-8">
                    <textarea class="form-control" rows="7" name='description'></textarea>
            </div>
        </div>
                             
    </div>
    <div class="panel-footer text-right">
        <a href="<?php echo site_url('video') ?>" class="btn tm-btn">Cancel</a>
        <button type="submit" class="btn tm-btn">Next</button>
    </div>
</div>

<?php echo form_close();?>
