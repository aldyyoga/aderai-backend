<div class="box">
    <div class="box-header">
        <h3 class="box-title">Research</h3>
    </div>
    <div class="box-body">
        <?php if(count($research) > 0) : ?>
        <table class="table">
            <?php foreach($research as $res) : ?>
            <tr><td><?php echo $prof->realname.', '.$res->riset_partner ?> (<?php echo $res->riset_year ?>). <?php echo $res->riset_title ?>.</td></tr>
            <?php endforeach ?>
        </table>
        <?php else : ?>
        No description founded.
        <?php endif ?>
    </div>
</div>