<ul class="nav nav-pills tm-pills">
    <li role="presentation" class="active"><a href="#profile" data-toggle="tab">Profile</a></li>
    <?php $menus = array('occupation','education','employment','publication','research','dedication');                
    foreach($menus as $menu) : ?>
    <li role="presentation"><a href="#<?php echo $menu ?>" aria-controls="home" role="tab" data-toggle="tab"><?php echo ucfirst($menu) ?></a></li>
    <?php endforeach ?>    
</ul>

<div class="box" style="line-height:25px;">
	<div class="box-body tm-nopadding row-eq-height">
		<div class="col-md-3" style="border-right: solid 1px #f4f4f4;">
			<div class="text-center tm-padding tm-nopadding-bottom" style="padding-top: 50px;">						
			<img src="<?php echo base_url(ADM_IMG . 'user6-128x128.jpg') ?>" class="img-circle" alt="">
			</div>
			<div class="text-center tm-padding">						
				<strong>Name</strong>
				<p><?php echo $prof->realname?></p>
				
				<strong>Email</strong>
				<p><?php echo $prof->email?></p>

				<strong>Phone</strong>
				<p><?php echo $prof->phone?></p>					
			</div>
		</div>
		<div class="col-md-9">
			<div class="tab-content">
				<div class="tab-pane active" id="profile"><?php $this->load->view('profile') ?></div>
				<?php foreach($menus as $menu) : ?>
				<div class="tab-pane" id="<?php echo $menu ?>"><?php $this->load->view($menu) ?></div>
				<?php endforeach ?>
			</div>
		</div>
	</div>
    <div class="panel-footer text-right">
        <a href="<?php echo site_url('members') ?>" class="btn tm-btn"><i class="ion-arrow-left-a"></i> Back</a>
    </div>
</div>



<style type="text/css">
.table>tbody>tr>td, 
.table>tbody>tr>th,
.table>tfoot>tr>td,
.table>tfoot>tr>th,
.table>thead>tr>td,
.table>thead>tr>th {
    padding: 8px 0;
}    

.table>tbody>tr>th,
.table>tbody>tr>td:first-child {
    width: 30%;
    font-weight: normal;
    color: #888;
}
</style>