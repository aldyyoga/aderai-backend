<div class="box">
    <div class="box-header">
        <h3 class="box-title">Occupation</h3>
    </div>
    <div class="box-body">
        <?php if(!empty($occu->office_name)) : ?>
        <div class="row">
            <div class="col-md-6">
                <strong>Position</strong>
                <p><?php echo $prof->ahli_bid ?></p>

                <strong>Office</strong>
                <p><?php echo $occu->office_name ?></p>

                <strong>Address</strong>
                <p><?php echo $occu->office_addr.', '.$occu->office_city.'<br>'.$propinsi[$occu->office_prov] ?></p>
            </div>
        </div>
        <?php else : ?>
        No description founded.
        <?php endif ?>
    </div>
</div>
