<div class="box">
    <?php if(count($employ) > 0 ) : ?>
    <?php foreach($employ as $emp) : ?>
    <div class="box-header">
        <h3 class="box-title"><?php echo $emp->jbt_type ?></h3>
    </div>
    <div class="box-body">
        <table class="table">
            <tr>
                <th>Office</th>
                <td><?php echo $emp->jbt_office ?></td>
            </tr>
            <tr>
                <th>Periode</th>
                <td><?php echo $emp->jbt_periode ?></td>
            </tr>
        </table>
    </div>
    <?php endforeach ?>
    <?php else :?>
    <div class="box-header">
        <h3 class="box-title">Employment</h3>
    </div>
    <div class="box-body">
        No description founded.
    </div>        
    <?php endif ?>
</div>