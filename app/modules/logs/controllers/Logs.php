<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Logs extends CI_Controller {

    private $table_name     = 'logs';
    private $url            = 'logs';
    private $perpage        = 10;
    private $data           = array();
    private $update         = '';
    private $delete         = '';

    public function __construct()
    {
        parent::__construct();

        // for security check
        $this->menu->has_access();

        // for header
        $this->data          = $this->header->button_header();
        $this->data['title'] = 'Logs System';

        // set no input button 
        $this->data['button_link']  = NULL; 

        // for debug purpose only
        $this->output->enable_profiler(false);
    }

    public function index( $offset = 0 )
    {
        // pagination 
        $this->db
        ->select('a.id')
        ->from($this->table_name.' a')
        ->join('users b', 'b.id = a.users_id')
        ->join('modules c', 'c.path = a.module');
        $query = $this->db->get();
        $count                      = $query->num_rows();
        $config['per_page']         = $this->perpage;
        $this->pagination->initialize($this->general_model->pagination_rules(site_url($this->url), $count, $this->perpage));
        $this->data['pagination']   = $this->pagination->create_links();

        // get all data
        $this->db
        ->select('a.id, a.message, a.module, a.updated_at, b.first_name, b.last_name, c.name')
        ->from($this->table_name.' a')
        ->join('users b', 'b.id = a.users_id')
        ->join('modules c', 'c.path = a.module')
        ->order_by('a.updated_at', 'desc')
        ->limit($this->perpage, $offset);
        $query = $this->db->get();

        // if not empty
        if($query->num_rows() > 0) {
            foreach($query->result() as $list) {

                // return to table data
                $table_row = array(
                                    $list->first_name.' '.$list->last_name, 
                                    $list->message, 
                                    $list->name,
                                    date('d M Y H:i:s A',strtotime($list->updated_at))
                );

                $this->table->add_row($table_row);
            }

            // generate table header
            $this->table->set_template(array('table_open' => '<table class="table">'));
            $this->table->set_heading('By Users', 'Message', 'In Module', 'Updated At');

            // output
            $this->data['list']         = $this->table->generate();
            $this->data['content']      = $this->load->view('admin/list', $this->data, TRUE);
        } else {
            // if no data
            $this->data['content']      = $this->load->view('admin/blank', $this->data, TRUE);                    
        }

        // make an output
        $this->load->view('admin/content', $this->data);
    }
}

/* End of file Logs.php */
/* Location: ./application/modules/admin/controllers/Logs.php */
