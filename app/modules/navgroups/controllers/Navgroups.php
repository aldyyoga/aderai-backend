<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Navgroups extends MX_Controller {

    private $table_name     = 'nav_groups';
    private $url            = 'navgroups';
    private $perpage        = 5;
    private $data           = array();
    private $update         = '';
    private $delete         = '';

    public function __construct()
    {
        parent::__construct();

        // for security check
        $this->menu->has_access();

        // for header
        $this->data          = $this->header->button_header();
        $this->data['title'] = 'Group of Navigations';

        // for button link
        $this->data['button_link'] = (($this->uri->segment(2) == 'add') || ($this->uri->segment(2) == 'update')) ? site_url($this->url) : site_url($this->url).'/add' ;

        // for debug purpose only
        $this->output->enable_profiler(false);
    }

    public function index( $offset = 0 )
    {
        $this->data['button_link'] = site_url('navgroups/add'); 

        // get groups
        $query = $this->db->get('nav_groups');
        $this->data['groups']  = $query->result();

        // pagination 
        $this->db
        ->select('id')
        ->from($this->table_name);
        $query = $this->db->get();
        $count                      = $query->num_rows();
        $config['per_page']         = $this->perpage;
        $this->pagination->initialize($this->general_model->pagination_rules(site_url($this->url), $count, $this->perpage));
        $this->data['pagination']   = $this->pagination->create_links();

        // get all data
        $this->db
        ->select('id, name, slug')
        ->from($this->table_name)
        ->order_by('id')
        ->limit($this->perpage, $offset);
        $query = $this->db->get();

        // if not empty
        if($query->num_rows() > 0) {
            foreach($query->result() as $list) {
                // check for update access
                if($this->menu->crud_access('update')) {
                    $this->update = '<a href="'.site_url($this->url.'/'.'update'.'/'.$list->id).'" class="btn btn-xs btn-default"><i class="fa fa-pencil"></i></a>';
                }

                // check for delete access
                if($this->menu->crud_access('delete')) {
                    $this->delete = '<a href="'.site_url($this->url.'/'.'delete'.'/'.$list->id).'" class="delete btn btn-xs btn-default"><i class="fa fa-trash"></i></a>';
                }

                // return to table data
                $table_row = array(
                    $list->name,
                    $list->slug
                );

                // check for additional link access
                if($this->menu->crud_access('update') || $this->menu->crud_access('delete')) {
                    $table_row = array_merge($table_row, array($this->delete.' '.$this->update));
                }

                $this->table->add_row($table_row);
            }

            // generate table header
            $this->table->set_template(array('table_open' => '<table class="table">'));

            // generate table header
            $this->table->set_template(array('table_open' => '<table class="table">'));
            if($this->menu->crud_access('update') || $this->menu->crud_access('delete')) {
                $this->table->set_heading('Groups', 'Shortname', '');
            } else {
                $this->table->set_heading('Menu','Shortname');
            }

            // output
            $this->data['list']         = $this->table->generate(); 
            $this->data['content']      = $this->load->view('admin/list', $this->data, TRUE);
        } else {
            // if no data
            $this->data['content']      = $this->load->view('admin/blank', $this->data, TRUE);                    
        }

        // make an output
        $this->load->view('admin/content', $this->data);
    }

    // Add a new item
    public function add()
    {
        // check for write access
        $this->menu->check_access('write');

        // validation rules
        $this->form_validation->set_rules('name', 'Name', 'required');        

        // if break the rules
        if ($this->form_validation->run() == FALSE) {
            $this->data['content'] = $this->load->view('navgroups/add', $this->data, true);
        } else {

            $record = array(
                'name'      => $this->input->post('name'),
                'slug'      => $this->input->post('slugs')
            );

            // start transaction
            $this->db->trans_start();
            $this->db->insert($this->table_name, $record);
            $this->db->trans_complete();

            // check for status
            if ($this->db->trans_status() === FALSE) {
                $db_error                   = $this->db->error();
                $this->data['message']      = $db_error['message'];
                $this->data['content']      = $this->load->view('navgroups/add', $this->data, true);   

            } else {
                $this->data['button_link']  = site_url('nav');
                $this->data['message']      = 'Your record has been saved.';
                $this->data['content']      = $this->load->view('admin/redirect', $this->data, TRUE);
            }
        }

        // make an output
        $this->load->view('admin/content', $this->data);        
    }

    //Update one item
    public function update( $id = null )
    {
        // check for write access
        $this->menu->check_access('update');

        // get data
        $this->db->where('id', $id);
        $query                  = $this->db->get($this->table_name);
        $this->data['form']     = $query->row();

        // validation rules
        $this->form_validation->set_rules('name', 'Name', 'required');        
        $this->form_validation->set_rules('slugs', 'URL Alias', 'required');        

        // if break the rules
        if ($this->form_validation->run() == FALSE) {
            $this->data['content'] = $this->load->view('navgroups/edit', $this->data, true);
        } else {
            // if not break the rules 
            $record = array(
                'name'      => $this->input->post('name'),
                'slug'     => $this->input->post('slugs')
            );

            // start transaction
            $this->db->trans_start();
            $this->db->where('id', $id);
            $this->db->update($this->table_name, $record);
            $this->db->trans_complete();

            // check for status
            if ($this->db->trans_status() === FALSE) {
                $db_error                   = $this->db->error();
                $this->data['message']      = $db_error['message'];
                $this->data['content']      = $this->load->view('navgroups/edit', $this->data, true);
            } else {
                $this->data['message']      = 'Your record has been saved.';
                $this->data['button_link']  = site_url('navgroups');
                $this->data['content']      = $this->load->view('admin/redirect', $this->data, TRUE);
            }
        }

        // make an output
        $this->load->view('admin/content', $this->data);        
    }

    //Delete one item
    public function delete( $id = NULL )
    {
        // security check
        $this->menu->check_access('delete');

        $this->db->trans_start();
        $this->db->where('id', $id);
        $this->db->delete($this->table_name);
        $this->db->trans_complete();
        if ($this->db->trans_status() === FALSE) {
            $message = 'Fail while removed your record.';
        } else {
            $message = 'Your record has been removed.';
        }
        $this->data['message']          = $message;
        $this->data['content']          = $this->load->view('admin/redirect', $this->data, TRUE);
        $this->load->view('admin/content', $this->data);        
    }

    private function getChildren($id)
    {   
        $output = 0;
        if(isset($id) && ($id != 0)) {
            $this->db
            ->select ('id')
            ->from('nav')
            ->where('nav_id', $id);
            $parents = $this->db->get();
            if($parents->num_rows() > 0 ){
                // $parent = $parents->row();                
                $output = $parents->num_rows(); 
            }
        }
        return $output;
    }

    public function getParent($parent_id)
    {   
        $output = '-';
        if(isset($parent_id) && ($parent_id != 0)) {
            $this->db
            ->select('name')
            ->from($this->table_name)
            ->where('id', $parent_id);
            $parents = $this->db->get();
            if($parents->num_rows() > 0 ){
                $parent = $parents->row();                
                $output = $parent->name;            
            }
        }
        return $output;
    }
}

/* End of file Category.php */
/* Location: ./application/modules/admin/controllers/Category.php */
