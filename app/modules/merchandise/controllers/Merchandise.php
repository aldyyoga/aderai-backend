<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Merchandise extends MX_Controller {

    private $table_name     = 'app_merchandise';
    private $table_name_2   = 'app_merchandise_category';
    private $url            = 'merchandise';
    private $perpage        = 10;
    private $data           = array();
    private $update         = '';
    private $delete         = '';

    public function __construct()
    {
        parent::__construct();

        // for security check
        $this->menu->has_access();

        // for header
        $this->data          = $this->header->button_header();
        $this->data['title'] = 'Merchandise';

        // for button link
        $this->data['button_link'] = (($this->uri->segment(2) == 'add') || ($this->uri->segment(2) == 'update')) ? site_url($this->url) : site_url($this->url).'/add' ;

        // for debug purpose only
        $this->output->enable_profiler(false);
    }



    public function index( $offset = 0 )
    {
        // pagination 
        $this->db
        ->select('app_merchandise.id,app_merchandise.name, price')
        ->from($this->table_name)
        ->join($this->table_name_2, 'app_merchandise_category.id = id_merchandise_category','left');
        $query = $this->db->get();
        $count                      = $query->num_rows();
        $config['per_page']         = $this->perpage;
        $this->pagination->initialize($this->general_model->pagination_rules(site_url($this->url), $count, $this->perpage));
        $this->data['pagination']   = $this->pagination->create_links();

        // get all data
        $this->db
        ->select('app_merchandise.id,app_merchandise.name as name1,app_merchandise_category.name as name2, price')
        ->from($this->table_name)
        ->join($this->table_name_2, 'app_merchandise_category.id = id_merchandise_category','left')
        ->order_by('app_merchandise_category.name')
        ->limit($this->perpage, $offset);
        $query = $this->db->get();

        // if not empty
        if($query->num_rows() > 0) {
            // return to table data
            $sort = array();

            for($i=0; $i < $this->db->count_all($this->table_name); $i++) { $sort[$i] = $i; }

            foreach($query->result() as $list) {
                // check for update access
                if($this->menu->crud_access('update')) {
                    $this->update = '<a href="'.site_url($this->url.'/'.'update'.'/'.$list->id).'" class="btn btn-xs btn-default"><i class="fa fa-pencil"></i></a>';
                }

                // check for delete access
                if($this->menu->crud_access('delete')) {
                    $this->delete = '<a href="'.site_url($this->url.'/'.'delete'.'/'.$list->id).'" class="delete btn btn-xs btn-default"><i class="fa fa-trash"></i></a>';
                }

                $price = $this->makeidr($list->price);
   
                // return to table data
                $table_row = array(
                             $list->name1,
                             $list->name2,
                             $price,
                        
                );

                // check for additional link access
                if($this->menu->crud_access('update') || $this->menu->crud_access('delete')) {
                    $table_row = array_merge($table_row, array($this->delete.' '.$this->update));
                }

                $this->table->add_row($table_row);
            }

            // generate table header
            $this->table->set_template(array('table_open' => '<table class="table">'));
            if($this->menu->crud_access('update') || $this->menu->crud_access('delete')) {
                $this->table->set_heading('Merchandise\'s Name', 'Merchandise\'s Category', 'Price', 'Action', '');
            } else {
                $this->table->set_heading('Merchandise\'s Name', 'Merchandise\'s Category', 'Price', 'Action');
            }

            // output
            $this->data['list']         = $this->table->generate(); 
            $this->data['content']      = $this->load->view('admin/list', $this->data, TRUE);
        } else {
            // if no data
            $this->data['content']      = $this->load->view('admin/blank', $this->data, TRUE);

        }

        // make an output
        $this->load->view('admin/content', $this->data);
    }


    // Add a new item
    public function add()
    {
        // check for write access
        $this->menu->check_access('write');

        // validation rules
        $this->form_validation->set_rules('name', 'Merchandise\'s Name', 'required');
        $this->form_validation->set_rules('id_merchandise_category', 'Category', 'required');
        $this->form_validation->set_rules('price', 'Price', 'required');
        $this->form_validation->set_rules('description', 'description', 'required');

        //add catagory list to form 
        $this->db
        ->select('id, name')
        ->from($this->table_name_2);
        $query = $this->db->get();
        $this->data['category'] =$query->result_array();


        // if break the rules
        if ($this->form_validation->run() == FALSE) {
            $this->data['content'] = $this->load->view('merchandise/add', $this->data, true);
        } else {
            

            $record =   array(
                        'name'                         => $this->input->post('name'),
                        'id_merchandise_category'      => $this->input->post('id_merchandise_category'),
                        'price'                        => $this->input->post('price'),
                        'description'                  => $this->input->post('description')
            );

            // start transaction
            $this->db->trans_start();
            $this->db->insert($this->table_name, $record);
            $this->db->trans_complete();

            // check for status
            if ($this->db->trans_status() === FALSE) {
                $db_error               = $this->db->error();
                $this->data['message']  = $db_error['message'];
                $this->data['content']  = $this->load->view('merchandise/add', $this->data, true);
            } else {
                $this->data['message']  = 'Your record has been saved.';
                $name = $this->input->post('name');
                $this->session->set_flashdata('name_merchandise',$name);
                $this->data['content']  = $this->load->view('admin/redirect', $this->data, TRUE);

                $name = $this->input->post('name');
                $this->db
                ->select('id, name')
                ->from($this->table_name)
                ->where('name =',$name);
                $query = $this->db->get();
                $row=$query->row_array();
                
                redirect('merchandise/add2/'.$row['id']);

            }
        }

        // make an output
        

        $this->load->view('admin/content', $this->data);        
    }


// Add a new item
    public function add2($id)
    {
        // check for write access
        $this->menu->check_access('write');

        // validation rules
        $this->form_validation->set_rules('color[]', 'Color', 'required');
        /*$this->form_validation->set_rules('size[]', 'Size', 'required');*/
/*        $this->form_validation->set_rules('userfile[]', 'userfile', 'required');*/

        //add catagory list to form 
        $this->db
        ->select('id, name')
        ->from($this->table_name)
        ->where('id =',$id);
        $query = $this->db->get();
        $this->data['id_merchandise']=$query->row_array();
        $row=$query->row_array();

        // if break the rules
        if ($this->form_validation->run() == FALSE) {
            $this->data['content'] = $this->load->view('merchandise/add2', $this->data, true);
        } else {

            $datainput=$this->input->post();

            $data = array();
            if(!empty($_FILES['userFiles']['name'])){
                $filesCount = count($_FILES['userFiles']['name']);
                for($i = 0; $i < $filesCount; $i++){
                    $_FILES['userFile']['name'] = $_FILES['userFiles']['name'][$i];
                    $_FILES['userFile']['type'] = $_FILES['userFiles']['type'][$i];
                    $_FILES['userFile']['tmp_name'] = $_FILES['userFiles']['tmp_name'][$i];
                    $_FILES['userFile']['error'] = $_FILES['userFiles']['error'][$i];
                    $_FILES['userFile']['size'] = $_FILES['userFiles']['size'][$i];

                    $new_name = time().'_'.$_FILES['userFile']['name'];
                    $config['file_name'] = $new_name;

                    $uploadPath = '././assets/merchandise/';
                    $config['upload_path'] = $uploadPath;
                    $config['allowed_types'] = 'gif|jpg|png';
                    
                    $this->load->library('upload', $config);
                    $this->upload->initialize($config);
                    if($this->upload->do_upload('userFile')){
                        $fileData = $this->upload->data();
                        $uploadData[$i]['name'] = $fileData['file_name'];
                        $uploadData[$i]['created'] = date("Y-m-d H:i:s");
                        $uploadData[$i]['modified'] = date("Y-m-d H:i:s");
                        $uploadData[$i]['id_merchandise'] = $id;

                        $CI =& get_instance();
                        $CI->load->library('image_lib');


                        $image_thumb = $uploadPath.'thumb/'. $new_name;
                        $config['image_library']    = 'gd2';
                        $config['source_image']     = $uploadPath.$new_name;
                        $config['new_image']        = $image_thumb;
                        $config['maintain_ratio']   = TRUE;
                        $config['height']           = 120;
                        $config['width']            = 108;

                        $CI->image_lib->initialize($config);
                        $CI->image_lib->resize();
                        $CI->image_lib->clear();

                    }
                }
                
                if(!empty($uploadData)){
                    //Insert file information into the database
                    $insertI = $this->db->insert_batch('app_merchandise_images',$uploadData);

                }
            }

            $color = $this->input->post('color');
            $resultC = array();
            foreach($color AS $key => $val){
             $resultC[] = array(
              "code"  => $_POST['color'][$key],
              "created"  => date("Y-m-d H:i:s"),
              "modified"  => date("Y-m-d H:i:s"),
              "id_merchandise"  => $id
             );
            }            
            $insertC= $this->db->insert_batch('app_merchandise_colors', $resultC);


            $size = $this->input->post('size');
            $resultS = array();
            foreach($size AS $key => $val){
             $resultS[] = array(
              "name"  => $_POST['size'][$key],
              "created"  => date("Y-m-d H:i:s"),
              "modified"  => date("Y-m-d H:i:s"),
              "id_merchandise"  => $id
             );
            }            
            $insertS= $this->db->insert_batch('app_merchandise_size', $resultS);

            // check for status
            if ($insertI || $insertC) {
                $this->data['message']  = 'Your record has been saved.';
                $this->data['content']  = $this->load->view('admin/redirect', $this->data, TRUE);
            } else {
                $db_error               = $this->db->error();
                $this->data['message']  = $db_error['message'];
                $this->data['content']  = $this->load->view('merchandise/add2', $this->data, true);
            }
        }

        // make an output
        

        $this->load->view('admin/content', $this->data);        
    }

    //Update one item
    public function update( $id = NULL )
    {
        // security check

        //get del color
        if ($this->uri->segment(4)=='delc') {
            $idC=$this->uri->segment(5);
            $this->db->trans_start();
            $this->db->where('id', $idC);
            $this->db->delete('cms_app_merchandise_colors');
            $this->db->trans_complete();
            redirect(site_url().'/merchandise/update/'.$id);
        }
        //dels size
        if ($this->uri->segment(4)=='dels') {
            $idS=$this->uri->segment(5);
            $this->db->trans_start();
            $this->db->where('id', $idS);
            $this->db->delete('cms_app_merchandise_size');
            $this->db->trans_complete();
            redirect(site_url().'/merchandise/update/'.$id);
        }

        //del images
        if ($this->uri->segment(4)=='deli') {
            $idI=$this->uri->segment(5);

            $this->db
            ->select('id, name')
            ->from('cms_app_merchandise_images')
            ->where('id',$idI);
            $query = $this->db->get();
            $row=$query->row_array();
            $image = $row['name'];
            $idImg = $row['id'];

            unlink(FCPATH.'assets/merchandise/'.$image);
            unlink(FCPATH.'assets/merchandise/thumb/'.$image);
            $this->db->trans_start();
            $this->db->where('id', $idImg);
            $this->db->delete('cms_app_merchandise_images');
            $this->db->trans_complete(); 

            redirect(site_url().'/merchandise/update/'.$id);
        }


        // get current data
        $this->db
        ->select('id, name')
        ->from($this->table_name_2);
        $query = $this->db->get();
        $this->data['category'] =$query->result_array();

        $query_m = $this->db->get_where($this->table_name, array('id'=> $id));
        $query_c = $this->db->get_where('cms_app_merchandise_colors', array('id_merchandise'=> $id));
        $query_s = $this->db->get_where('cms_app_merchandise_size', array('id_merchandise'=> $id));
        $query_i = $this->db->get_where('cms_app_merchandise_images', array('id_merchandise'=> $id));

        // if no data
        if($query_m->num_rows() <= 0 ) {
            return show_error('Your data may has been removed or changes by someone.', '' , 'Data Not Found');
            exit;
        }

        // return data
        $this->data['formm']     = $query_m->row();
        $this->data['formc']     = $query_c->result();
        $this->data['forms']     = $query_s->result();
        $this->data['formi']     = $query_i->result();
        $this->data['content']  = $this->load->view('merchandise/edit', $this->data, true);       

        // validation rules
        $this->form_validation->set_rules('name', 'Merchandise\'s Name', 'required');
        $this->form_validation->set_rules('price', 'Price', 'required');
       /* $this->form_validation->set_rules('image', 'Image', 'required');*/

        // if break the rules
        if ($this->form_validation->run() == FALSE) {
            $this->data['content'] = $this->load->view('merchandise/edit', $this->data, true);
        } else {
            // if not break the rules

            //del color
            $this->db->trans_start();
            $this->db->where('id_merchandise', $id);
            $this->db->delete('cms_app_merchandise_colors');
            $this->db->trans_complete();

            //in color
            $color = $this->input->post('color');
            $resultC = array();
            foreach($color AS $key => $val){
             $resultC[] = array(
              "code"  => $_POST['color'][$key],
              "created"  => date("Y-m-d H:i:s"),
              "modified"  => date("Y-m-d H:i:s"),
              "id_merchandise"  => $id
             );
            }            
            $insertC= $this->db->insert_batch('app_merchandise_colors', $resultC);

            //del size
            $this->db->trans_start();
            $this->db->where('id_merchandise', $id);
            $this->db->delete('cms_app_merchandise_size');
            $this->db->trans_complete();

            //in size
            $size = $this->input->post('size');
            $resultS = array();
            foreach($size AS $key => $val){
             $resultS[] = array(
              "name"  => $_POST['size'][$key],
              "created"  => date("Y-m-d H:i:s"),
              "modified"  => date("Y-m-d H:i:s"),
              "id_merchandise"  => $id
             );
            }            
            $insertS= $this->db->insert_batch('app_merchandise_size', $resultS);

            $record =   array(
                        'name'                         => $this->input->post('name'),
                        'id_merchandise_category'      => $this->input->post('id_merchandise_category'),
                        'price'                        => $this->input->post('price'),
                        'description'                  => $this->input->post('description'),
                        'modified'                     => date("Y-m-d H:i:s")
            );
            // start transaction
            $this->db->trans_start();
            $this->db->where('id', $id);
            $this->db->update($this->table_name, $record);
            $this->db->trans_complete();

            $data = array();
            if(!empty($_FILES['userFiles']['name'])){
                $filesCount = count($_FILES['userFiles']['name']);
                for($i = 0; $i < $filesCount; $i++){
                    $_FILES['userFile']['name'] = $_FILES['userFiles']['name'][$i];
                    $_FILES['userFile']['type'] = $_FILES['userFiles']['type'][$i];
                    $_FILES['userFile']['tmp_name'] = $_FILES['userFiles']['tmp_name'][$i];
                    $_FILES['userFile']['error'] = $_FILES['userFiles']['error'][$i];
                    $_FILES['userFile']['size'] = $_FILES['userFiles']['size'][$i];

                    $new_name = time().'_'.$_FILES['userFile']['name'];
                    $config['file_name'] = $new_name;

                    $uploadPath = '././assets/merchandise/';
                    $config['upload_path'] = $uploadPath;
                    $config['allowed_types'] = 'gif|jpg|png';
                    
                    $this->load->library('upload', $config);
                    $this->upload->initialize($config);
                    if($this->upload->do_upload('userFile')){
                        $fileData = $this->upload->data();
                        $uploadData[$i]['name'] = $fileData['file_name'];
                        $uploadData[$i]['created'] = date("Y-m-d H:i:s");
                        $uploadData[$i]['modified'] = date("Y-m-d H:i:s");
                        $uploadData[$i]['id_merchandise'] = $id;

                        $CI =& get_instance();
                        $CI->load->library('image_lib');


                        $image_thumb = $uploadPath.'thumb/'. $new_name;
                        $config['image_library']    = 'gd2';
                        $config['source_image']     = $uploadPath.$new_name;
                        $config['new_image']        = $image_thumb;
                        $config['maintain_ratio']   = TRUE;
                        $config['height']           = 120;
                        $config['width']            = 108;

                        $CI->image_lib->initialize($config);
                        $CI->image_lib->resize();
                        $CI->image_lib->clear();

                    }
                }
                
                if(!empty($uploadData)){
                    //Insert file information into the database
                    $insertI = $this->db->insert_batch('app_merchandise_images',$uploadData);

                }
            }



            // check for status
            if ($this->db->trans_status() === FALSE) {
                $db_error               = $this->db->error();
                $this->data['message']  = $db_error['message'];
                $this->data['content']  = $this->load->view('merchandise/edit', $this->data, true);
            } else {
                $direct                     = site_url($this->url);
                $this->data['button_link']  = $direct;
                $this->data['message']      = 'Your record has been updated.';
                $this->data['content']      = $this->load->view('admin/redirect', $this->data, TRUE);
            }
        }

        // make an output
        $this->load->view('admin/content', $this->data);        

    }

    //Delete one item
    public function delete( $id = NULL )
    {
        // security check
         $this->menu->check_access('delete');


        $img = $this->db->query("SELECT COUNT(*) as ttl FROM cms_app_merchandise_images where id_merchandise =".$id)->row();
        for($i = 0; $i <$img->ttl; $i++){
            $this->db
            ->select('id, name')
            ->from('cms_app_merchandise_images')
            ->where('id_merchandise =',$id);
            $query = $this->db->get();
            $row=$query->row_array();
            $image = $row['name'];
            $idImg = $row['id'];

            unlink(FCPATH.'assets/merchandise/'.$image);
            unlink(FCPATH.'assets/merchandise/thumb/'.$image);
            $this->db->trans_start();
            $this->db->where('id', $idImg);
            $this->db->delete('cms_app_merchandise_images');
            $this->db->trans_complete(); 
        }


        $this->db->trans_start();
        $this->db->where('id', $id);
        $this->db->delete($this->table_name);
        $this->db->trans_complete();


        $this->db->trans_start();
        $this->db->where('id_merchandise', $id);
        $this->db->delete('cms_app_merchandise_size');
        $this->db->trans_complete();

        $this->db->trans_start();
        $this->db->where('id_merchandise', $id);
        $this->db->delete('cms_app_merchandise_colors');
        $this->db->trans_complete();



        if ($this->db->trans_status() === FALSE) {
            $message = 'Fail while removed your record.';
        } else {
            $message = 'Your record has been removed.';
        }

        $this->data['message']          = $message;
        $this->data['content']          = $this->load->view('admin/redirect', $this->data, TRUE);
        $this->load->view('admin/content', $this->data);        
    }
    public function makeidr($price='')
    {
        $value = "Rp " . number_format($price,2,',','.');
        return $value;
    }
}

/* End of file Packages.php */
/* Location: ./application/modules/admin/controllers/Packages.php */
