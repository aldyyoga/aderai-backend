<script>
var color = 1;
function add_fields_col() {
    color++;
    var objTo = document.getElementById('room_fileds_col')
    var divCol = document.createElement("div");
    divCol.innerHTML = '<div class="form-group"><label class="col-sm-4">Color ' + color +'</label><div class="col-sm-2"><input class="form-control" type="color" name="color[]" value="#ff0000"></div></div>';
    
    objTo.appendChild(divCol)
}
</script>

<script type="text/javascript">
var size = 1;
function add_fields_si() {
    size++;
    var objTo = document.getElementById('room_fileds_si')
    var divSi = document.createElement("div");
    divSi.innerHTML = '<div class="form-group"><label for="" class="col-sm-4"s>Size ' + size +'</label><div class="col-sm-3"><input type="text" name="size[]" value=""  class="form-control" /></div></div>';
    
    objTo.appendChild(divSi)
}
</script>

<?php if(isset($message)) : ?>
<div class="alert alert-danger">
    <?php echo $message; ?>
</div>
<?php endif ?>

<?php if(validation_errors()) : ?>
<div class="alert alert-danger">
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
    <strong>Error</strong>
    <hr>
    <?php echo validation_errors(); ?>
</div>
<?php endif ?>


<?php echo form_open_multipart(current_url(), 'class="form-horizontal" role="form"');?>
<div class="box">
    <div class="box-header with-border">
        <h3 class="box-title">Please complete the form below.</h3>
    </div>
    <div class="box-body tm-padding">            
        <div class="form-group">
            <label for="" class="col-sm-4">Name</label>
            <div class="col-sm-8">
                <input class="form-control" type="text" name="name" value="<?php echo $id_merchandise['name']; ?>" disabled>
                <input type="hidden" name="id" value="<?php echo $id_merchandise['id']; ?>">
            </div>
        </div>
        <hr>
        <div class="box-header with-border" >
            <h3 class="box-title"> Colors</h3>
        </div>
        <div class="form-group">
            <label for="" class="col-sm-4">Color 1</label>
            <div class="col-sm-2">
                <input class="form-control" type="color" name="color[]" value="#ff0000">
            </div>
        </div>
        <div id="room_fileds_col">
            
        </div>
        <input type="button" id="more_fields" onclick="add_fields_col();" value="Add More" />
        <hr>



        <div class="box-header with-border">
            <h3 class="box-title"> Size</h3>
        </div>
        <div class="form-group">
            <label for="" class="col-sm-4">Size 1</label>
            <div class="col-sm-3">
            <?php echo form_input('size[]',set_value('size'),'class="form-control" required');?>
            </div>
        </div>
        <div id="room_fileds_si">
            
        </div>
        <input type="button" id="more_fields" onclick="add_fields_si();" value="Add More" />
        <hr>
        <div class="box-header with-border">
            <h3 class="box-title"> Images</h3>
        </div>
        <div class="form-group">
            <label for="" class="col-sm-4">Image</label>
            <div class="col-sm-4">
                <input type="file" class="form-control" name="userFiles[]" multiple/>
            </div>
        </div>
        <hr>
                             
    </div>
    <div class="panel-footer text-right">
        <a href="<?php echo site_url('merchandise') ?>" class="btn tm-btn">Cancel</a>
        <button type="submit" class="btn tm-btn">Submit</button>
    </div>
</div>

<?php echo form_close();?>
