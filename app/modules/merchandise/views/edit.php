<script>
var color = 1;
function add_fields_col() {
    color++;
    var objTo = document.getElementById('room_fileds_col')
    var divCol = document.createElement("div");
    divCol.innerHTML = '<div class="form-group"><label class="col-sm-4">Color ' + color +'</label><div class="col-sm-2"><input class="form-control" type="color" name="color[]" value="#ff0000"></div></div>';
    
    objTo.appendChild(divCol)
}


</script>

<script type="text/javascript">
var size = 1;
function add_fields_si() {
    size++;
    var objTo = document.getElementById('room_fileds_si')
    var divSi = document.createElement("div");
    divSi.innerHTML = '<div class="form-group"><label for="" class="col-sm-4"s>Size ' + size +'</label><div class="col-sm-3"><input type="text" name="size[]" value=""  class="form-control" /></div></div>';
    
    objTo.appendChild(divSi)
}
</script>

<?php if(isset($message)) : ?>
<div class="alert alert-danger">
    <?php echo $message; ?>
</div>
<?php endif ?>
<?php if(validation_errors()) : ?>
<div class="alert alert-danger">
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
    <strong>Error</strong>
    <hr>
    <?php echo validation_errors(); ?>
</div>
<?php endif ?>
<?php echo form_open_multipart(current_url(), 'class="form-horizontal" role="form"');?>
<div class="box">
    <div class="box-header with-border">
        <h3 class="box-title">Please complete the form below.</h3>
    </div>
    <div class="box-body tm-padding">            
        <div class="form-group">
            <label for="" class="col-sm-4">Name</label>
            <div class="col-sm-8">
            <?php echo form_input('name',set_value('name', $formm->name),'class="form-control"');?>
            </div>
        </div>
             
        <div class="form-group">
            <label for="" class="col-sm-4">Price</label>
            <div class="col-sm-8">
            <?php echo form_input('price',set_value('price', $formm->price),'class="form-control"');?>
            <span id="" class="help-block ">Add without ' , ' or ' . '</span>
            </div>
        </div>
        <div class="form-group">
            <label for="" class="col-sm-4">Category</label>
            <div class="col-sm-8">
                <select class="form-control" name="id_merchandise_category">
                    <option value="">Choose Category</option>
                    <?php foreach ($category as $key => $value): ?>
                    <option value="<?php echo $value['id']; ?>"  <?php if($formm->id_merchandise_category == $value['id']) echo "selected"; ?>><?php echo $value['name']; ?></option>
                    <?php endforeach ?>
                </select>
            </div>
        </div>
        <div class="form-group">
            <label for="" class="col-sm-4">Description</label>
            <div class="col-sm-8">
                    <textarea class="form-control" rows="7" name='description'><?php echo $formm->description; ?></textarea>
            </div>
        </div>




        <hr>
        <div class="box-header with-border" >
            <h3 class="box-title"> Colors</h3>
        </div>
        <?php $i=1;foreach ($formc as $c): ?>
        <div class="form-group">
            <label for="" class="col-sm-4">Color <?php echo $i; ?></label>
            <div class="col-sm-2">
                <input class="form-control" type="color" name="color[]" value="<?php echo $c->code; ?>">
            </div>
            <a href="<?php echo current_url().'/delc/'.$c->id;?>"><button type="button" class="btn tm-btn" name="del_img" >Delete</button></a>
        </div>
        <?php $i++;endforeach ?>

        <div id="room_fileds_col">
        </div>
        <button type="button" class="btn tm-btn" name="del_img  id="more_fields" onclick="add_fields_col();" >Add More</button>
        <hr>



        <div class="box-header with-border">
            <h3 class="box-title"> Size</h3>
        </div>
        <?php $i=1;foreach ($forms as $s): ?>
        <div class="form-group">
            <label for="" class="col-sm-4">Size <?php echo $i; ?></label>
            <div class="col-sm-3">
                <input type="text" class="form-control" name="size[]" value="<?php echo $s->name; ?>">
            </div>
            <a href="<?php echo current_url().'/dels/'.$s->id;?>"><button type="button" class="btn tm-btn" name="del_img" >Delete</button></a>
        </div>
        <?php $i++;endforeach ?>
        <div id="room_fileds_si">
            
        </div>
        <button type="button" class="btn tm-btn" name="del_img  id="more_fields" onclick="add_fields_si();">Add More</button>
        <hr>
<!-- --------------------------------------------------------------------------------- -->
        <div class="box-header with-border">
            <h3 class="box-title"> Images</h3>
        </div>
        
        <div class="row">
        <?php $i=1;foreach ($formi as $i): ?>
            <div class="col-md-3">
                <img src="<?php echo base_url().'assets/merchandise/'.$i->name;; ?>" class="img-thumbnail" alt="Cinque Terre">
                <div for="" class="col-sm-2 col-offset-sm-6 ">
                    <a href="<?php echo current_url().'/deli/'.$i->id;?>"><button type="button" class="btn tm-btn" name="del_img" >Delete</button></a>
                </div>
            </div>
        <?php $i++;endforeach ?>
        </div>
        <hr>

        <div class="form-group">
            <label for="" class="col-sm-4">New Image</label>
            <div class="col-sm-4">
                <input type="file" class="form-control" name="userFiles[]" multiple/>
            </div>
        </div>
        <hr>
        
<!-- --------------------------------------------------------------------------------- -->



       
        </div>                                
</div>
<div class="panel-footer text-right">
    <a href="<?php echo site_url('merchandise') ?>" class="btn tm-btn">Cancel</a>
    <button type="submit" class="btn tm-btn">Submit</button>
</div>
</div>
<?php echo form_close();?>
