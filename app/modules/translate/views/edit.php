<?php if(validation_errors()) : ?>
<div class="alert alert-danger">
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
    <strong>Error</strong>
    <hr>
    <?php echo validation_errors(); ?>
</div>
<?php endif ?>
<form action="<?php echo current_url() ?>" method="POST" class="form-horizontal" role="form">
<div class="box">
    <div class="box-header with-border">
        <h3 class="box-title">Please complete the form below.</h3>
    </div>
    <ul class="nav nav-tabs" role="tablist" style="padding-left: 10px;">
        <?php foreach($language as $lang) : ?>
        <li role="presentation" <?php echo ($lang->is_default == 1) ? 'class="active"' : '' ?>><a href="#<?php echo $lang->shortname ?>" aria-controls="<?php echo $lang->shortname ?>" role="tab" data-toggle="tab" style="padding: 25px 15px;"><?php echo $lang->name ?></a></li>
        <?php endforeach ?>
    </ul>
    <div class="box-body tab-content tm-padding">   
        <div class="form-group" style="margin-bottom:-20px">
            <label for="" class="col-sm-4">Shortname</label>
            <div class="col-sm-8">
            <input type="text" name="slug" value="<?php echo set_value("slug", $form[$this->session->userdata('lang')]->slug) ?>" class="form-control" id="" placeholder="">
            </div>
        </div>                              
        <?php foreach($language as $lang) : ?>
            <div class="tab-pane <?php echo ($lang->is_default == 1) ? 'active' : '' ?>" id="<?php echo $lang->shortname ?>">
                <div class="form-group">
                    <label for="" class="col-sm-4">Translate to <?php echo $lang->name ?></label>
                    <div class="col-sm-8">
                    <input type="text" name="trans[<?php echo $lang->shortname ?>]" value="<?php echo set_value("trans[".$lang->shortname."]", $form[$lang->shortname]->translate) ?>" class="form-control" id="" placeholder="">
                    </div>
                </div>              
            </div>
        <?php endforeach ?>

    </div>
    <div class="panel-footer text-right">
    <input type="hidden" name="id" value="<?php echo $form[$this->session->userdata('lang')]->id ?>">
    <a href="<?php echo site_url('translate') ?>" class="btn tm-btn">Cancel</a>
    <button type="submit" class="btn tm-btn">Submit</button>
    </div>
</div>
</form>