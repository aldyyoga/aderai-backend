<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Payment extends MX_Controller {

    private $table_name     = 'payment';
    private $url            = 'payment';
    private $perpage        = 10;
    private $data           = array();
    private $update         = '';
    private $delete         = '';

    public function __construct()
    {
        parent::__construct();

        // for security check
        $this->menu->has_access();

        // for header
        $this->data          = $this->header->button_header();
        $this->data['title'] = 'Pages';

        // for button link
        $this->data['button_link'] = (($this->uri->segment(2) == 'add') || ($this->uri->segment(2) == 'update')) ? site_url($this->url) : site_url($this->url).'/add';

        //for debug set as true
        $this->output->enable_profiler(false); 

    }

    public function index()
    {
        $this->data['content'] = 'In Progress';

        // make an output
        $this->load->view('admin/content', $this->data);
    }
}