<?php echo form_open(current_url(), 'class="form-horizontal" role="form"');?>
    <div class="box">
        <div class="box-header with-border">
            <h3 class="box-title"><?php echo lang('deactivate_heading');?></h3>
        </div>
        <div class="box-body tm-padding">
            <p><?php echo sprintf(lang('deactivate_subheading'), $user->username) ?></p>
            <input type="radio" name="confirm" value="yes" checked="checked" /> Yes
            <input type="radio" name="confirm" value="no" /> No
        </div>
        <div class="panel-footer text-right">
            <?php echo form_hidden($csrf); ?>
            <?php echo form_hidden(array('id' => $user->id )); ?>
            <a href="<?php echo site_url('auth') ?>" class="btn tm-btn">Cancel</a>
            <button type="submit" class="btn tm-btn"><?php echo lang('deactivate_submit_btn') ?></button>
        </div>
    </div>
<?php echo form_close();?>

