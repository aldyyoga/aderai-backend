<?php if(count($files) > 0) : ?>
<ul class="mailbox-attachments">
    <?php foreach($files as $file) : ?>
        <li style="position: relative !important; height: 150px; overflow: hidden;">
            <div class="tm-action animate fade in">
                <a href="#"><i class="ion-android-create"></i></a>
                <a href="<?php echo site_url('files/delete/') .'/'. $file->id ?>" class="delete"><i class="ion-android-close"></i></a>
            </div>
            <a href="#" data-id="<?php echo $file->id ?>" class="mailbox-attachment-name">
                <span class="mailbox-attachment-icon has-img" style="height: 100px; overflow: hidden;">
                <img src="<?php echo site_url('files/media') . '/' . $file->alias ?>" alt="Attachment"/>
                </span>
                <div class="mailbox-attachment-info">
                    <?php echo (strlen($file->filename) >= 20) ? substr($file->filename, 0, 10) .'...'.substr($file->filename, -8) : $file->filename ?>
                    <span class="mailbox-attachment-size"><?php echo $file->size ?> KB</span>
                </div>
            </a>
        </li>
    <?php endforeach ?>
</ul>
<?php else :?>
<div class="text-center">
    No Media Available. <a href="<?php echo base_url('files/add') ?>">Upload New Media</a>
</div>
<?php endif ?>
<script type="text/javascript">
    $('#media .mailbox-attachments > li a').each(function(index){
        $(this).click(function(){
            id  = $(this).attr('data-id');
            src = $(this).children('span').children('img').attr('src');
            $('#file_id').val(id);
            $('#featured_attach').attr('src', src).attr('style','margin-bottom:20px;');
            $('#remove-img').removeClass('hidden');
        });
    });

    $('.delete').click(function(){
        if(confirm('Are you sure delete this record ? ')) {
            return true;
        } else {
            return false;
        }
    });
</script>