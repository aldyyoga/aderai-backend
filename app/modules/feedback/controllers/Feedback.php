<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Feedback extends MX_Controller {

    private $table_name     = 'app_feedback';
    private $url            = 'feedback';
    private $perpage        = 5;
    private $data           = array();
    private $update         = '';
    private $delete         = '';

    public function __construct()
    {
        parent::__construct();

        // for security check
        $this->menu->has_access();

        // for header
        $this->data          = $this->header->button_header();
        $this->data['title'] = 'Feedback';

        // for debug purpose only
        $this->output->enable_profiler(false);
    }

    public function index( $offset = 0 )
    {
        // pagination 
        $this->db
        ->select('f.id, u.name, f.title, f.content')
        ->from($this->table_name.' f')
        ->join('cms_app_users u','u.id=f.user_id','left')
        ->order_by('u.name');
        $query = $this->db->get();
        $count                      = $query->num_rows();
        $config['per_page']         = $this->perpage;
        $this->pagination->initialize($this->general_model->pagination_rules(site_url($this->url), $count, $this->perpage));
        $this->data['pagination']   = $this->pagination->create_links();

        // get all data
        $this->db
        ->select('f.id, u.name, f.title, f.content')
        ->from($this->table_name.' f')
        ->join('cms_app_users u','u.id=f.user_id','left')
        ->order_by('u.name')
        ->limit($this->perpage, $offset);
        $query = $this->db->get();

        // if not empty
        if($query->num_rows() > 0) {
            // return to table data
            $sort = array();
            for($i=0; $i < $this->db->count_all($this->table_name); $i++) { $sort[$i] = $i; }

            foreach($query->result() as $list) {
            
                // return to table data
                $table_row = array(
                    $list->name,
                    $list->title,                 
                    $list->content,
                );

                // check for additional link access
                if($this->menu->crud_access('update') || $this->menu->crud_access('delete')) {
                    $table_row = array_merge($table_row, array($this->delete.' '.$this->update));
                }

                $this->table->add_row($table_row);
            }

            // generate table header
            $this->table->set_template(array('table_open' => '<table class="table">'));
            if($this->menu->crud_access('update') || $this->menu->crud_access('delete')) {
                $this->table->set_heading('Name', 'Title', 'Content', '');
            } else {
                $this->table->set_heading('Name', 'Title', 'Content');
            }

            // output
            $this->data['list']         = $this->table->generate(); 
            $this->data['content']      = $this->load->view('admin/list', $this->data, TRUE);
        } else {
            // if no data
            $this->data['content']      = $this->load->view('admin/blank', $this->data, TRUE);                    
        }
        $this->load->view('admin/content', $this->data);
    }

}

/* End of file Packages.php */
/* Location: ./application/modules/admin/controllers/Packages.php */
