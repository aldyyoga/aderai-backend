<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Category extends MX_Controller {

    private $table_name     = 'app_merchandise_category';
    private $url            = 'category';
    private $perpage        = 5;
    private $data           = array();
    private $update         = '';
    private $delete         = '';

    public function __construct()
    {
        parent::__construct();

        // for security check
        $this->menu->has_access();

        // for header
        $this->data          = $this->header->button_header();
        $this->data['title'] = 'Merchandise Category';

        // for button link
        $this->data['button_link'] = (($this->uri->segment(2) == 'add') || ($this->uri->segment(2) == 'update')) ? site_url($this->url) : site_url($this->url).'/add' ;

        // for debug purpose only
        $this->output->enable_profiler(false);
    }

    public function index( $offset = 0 )
    {
        // pagination 
        $this->db
        ->select('id,name')
        ->from($this->table_name );
        $query = $this->db->get();
        $count                      = $query->num_rows();
        $config['per_page']         = $this->perpage;
        $this->pagination->initialize($this->general_model->pagination_rules(site_url($this->url), $count, $this->perpage));
        $this->data['pagination']   = $this->pagination->create_links();

        // get all data
        $this->db
        ->select('id,name')
        ->from($this->table_name )
        ->order_by('id')
        ->limit($this->perpage, $offset);
        $query = $this->db->get();
        // if not empty
        if($query->num_rows() > 0) {
            foreach($query->result() as $list) {
                // check for update access
                if($this->menu->crud_access('update')) {
                    $this->update = '<a href="'.site_url($this->url.'/'.'update'.'/'.$list->id).'" class="btn btn-xs btn-default"><i class="fa fa-pencil"></i></a>';                    
                }

                // check for delete access
                if($this->menu->crud_access('delete')) {
                    $this->delete = '<a href="'.site_url($this->url.'/'.'delete'.'/'.$list->id).'" class="delete btn btn-xs btn-default"><i class="fa fa-trash"></i></a>';
                }

                // return to table data
                $table_row = array(
                             $list->id,
                             $list->name,
                        
                );

                // check for additional link access
                if($this->menu->crud_access('update') || $this->menu->crud_access('delete')) {
                    $table_row = array_merge($table_row, array($this->delete.' '.$this->update));
                }

                $this->table->add_row($table_row);
            }

            // generate table header
            $this->table->set_template(array('table_open' => '<table class="table">'));
            if($this->menu->crud_access('update') || $this->menu->crud_access('delete')) {
                $this->table->set_heading('Id', 'Category', 'Action');
            } else {
                $this->table->set_heading('Id','Category');
            }

            // output
            $this->data['list']         = $this->table->generate(); 
            $this->data['content']      = $this->load->view('admin/list', $this->data, TRUE);
        } else {
            // if no data
            $this->data['content']      = $this->load->view('admin/blank', $this->data, TRUE);                    
        }

        // make an output
        $this->load->view('admin/content', $this->data);
    }


    // Add a new item
    public function add()
    {
        // check for write access
        $this->menu->check_access('write');

        // validation rules
        $this->form_validation->set_rules('name', 'Category Name', 'required');

        // if break the rules
        if ($this->form_validation->run() == FALSE) {
            $this->data['content'] = $this->load->view('category/add', $this->data, true);
        } else {
            // save to category first
            $record = array(
                'name'          => $this->input->post('name'),
                'created'       => date('Y-m-d h:i:s'),
                'modified'       => date('Y-m-d h:i:s')
            );

            // start transaction
            $this->db->insert($this->table_name, $record);
            $id = $this->db->insert_id();


            // check for status
            if ($this->db->trans_status() === FALSE) {
                $db_error               = $this->db->error();
                $this->data['message']  = $db_error['message'];
                $this->data['content']  = $this->load->view('category/add', $this->data, true);
            } else {
                $this->data['message']  = 'Your record has been saved.';
                $this->data['content']  = $this->load->view('admin/redirect', $this->data, TRUE);
            }
        }

        // make an output
        $this->load->view('admin/content', $this->data);        
    }

    // Add a new item
    public function update($id = NULL)
    {
        // check for write access
        $this->menu->check_access('update');

        // get current data
        $this->db
        ->select('id, name')
        ->from($this->table_name)
        ->where('id',$id);
        $query = $this->db->get();
        $this->data['category'] =$query->row();

        // validation rules
        $this->form_validation->set_rules('id', 'Category ID', 'required');
        $this->form_validation->set_rules('name', 'Category Name', 'required');


        // if break the rules
        if ($this->form_validation->run() == FALSE) {
            $this->data['content'] = $this->load->view('category/edit', $this->data, true);
        } else {
            // save to category first
            $data = array(
                'id'          => $this->input->post('id'),
                'name'        => $this->input->post('name'),
                'modified'    => date('Y-m-d h:i:s')
            );

            // start transaction
            $this->db->trans_start();
            $this->db->where('id', $id);
            $this->db->update($this->table_name, $data);
            $this->db->trans_complete();

            if ($this->db->trans_status() === FALSE) {
                $db_error               = $this->db->error();
                $this->data['message']  = $db_error['message'];
                $this->data['content']  = $this->load->view('category/edit', $this->data, true);
            } else {
                $this->data['message']  = 'Your record has been saved.';
                $this->data['content']  = $this->load->view('admin/redirect', $this->data, TRUE);
            }
        }

        // make an output
        $this->load->view('admin/content', $this->data);        
    }


    //Delete one item
    public function delete( $id = NULL )
    {
        // security check
        $this->menu->check_access('delete');

        $this->db
        ->select('id')
        ->from('app_merchandise')
        ->where('id_merchandise_category',$id);
        $query = $this->db->get();

        if($query->num_rows() > 0) {
            $this->data['message']  = "<p class='text-danger'>Warning! Can not remove this data, This record is still used by merchandise</p>";
        }else{
            $this->db->trans_start();
            $this->db->where('id', $id);
            $this->db->delete($this->table_name);
            $this->db->trans_complete();
            if ($this->db->trans_status() === FALSE) {
                $message = 'Fail while removed your record.';
            } else {
                $message = 'Your record has been removed.';
            }
            $this->data['message']          = $message;
        }
        $this->data['content']          = $this->load->view('admin/redirect', $this->data, TRUE);
        $this->load->view('admin/content', $this->data);    
    }


    public function ajax_default()
    {
        // set all is_default value to 0
        $this->db->update($this->table_name, array('is_default' => 0));

        // update selected is_default
        $this->db->where('id', $this->input->post('id'));
        $this->db->update($this->table_name, array('is_default' => 1));
        echo $this->input->post('id');
    }


}

/* End of file Category.php */
/* Location: ./application/modules/admin/controllers/Category.php */
