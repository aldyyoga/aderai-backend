<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Nav extends MX_Controller {

    private $table_name     = 'nav';
    private $url            = 'nav';
    private $perpage        = 5;
    private $data           = array();
    private $update         = '';
    private $delete         = '';
    private $lang           = '';

    public function __construct()
    {
        parent::__construct();

        // for security check
        $this->menu->has_access();

        // for header
        $this->data          = $this->header->button_header();
        $this->data['title'] = 'Navigations';

        // for button link
        $this->data['button_link'] = (($this->uri->segment(2) == 'add') || ($this->uri->segment(2) == 'update')) ? site_url($this->url) : site_url($this->url).'/add' ;

        // current lang
        $this->db->where('is_default', '1');
        $query  = $this->db->get('cms_lang');
        $lang   = $query->row();
        $this->lang = $lang->shortname;

        // for debug purpose only
        $this->output->enable_profiler(false);
    }

    public function index( $offset = 0 )
    {
        // pagination 
        $this->db
        ->select('a.id')
        ->from($this->table_name.' a')
        ->join('cms_nav_lang b','b.id = a.id', 'left')
        ->where('b.lang', $this->session->userdata('lang'));
        $query = $this->db->get();
        $count                      = $query->num_rows();
        $config['per_page']         = $this->perpage;
        $this->pagination->initialize($this->general_model->pagination_rules(site_url($this->url), $count, $this->perpage));
        $this->data['pagination']   = $this->pagination->create_links();

        // get all data
        $this->db
        ->select('a.id, a.parent_id, a.slug, b.name, DATE_FORMAT(a.updated_at,\'%d %M %Y %T\') updated_at')
        ->from($this->table_name.' a')
        ->join('cms_nav_lang b','b.id = a.id', 'left')
        ->where('b.lang', $this->lang)
        ->order_by('a.id')
        ->limit($this->perpage, $offset);
        $query = $this->db->get();

        // $sql = 'SELECT  a.id, a.parent_id, b.name, DATE_FORMAT(a.updated_at,\'%d %M %Y %T\') created_at
        //         FROM '.$this->table_name.' a
        //         LEFT JOIN cms_nav_lang b ON b.id = a.id
        //         ORDER BY
        //                 CASE 
        //                     WHEN a.parent_id = 0 THEN name
        //                     ELSE    (
        //                             SELECT  b.name
        //                             FROM    '.$this->table_name.' a 
        //                             LEFT JOIN cms_nav_lang b ON b.id = a.id
        //                             WHERE   a.id = a.parent_id
        //                             ) 
        //                     END,       
        //                 CASE WHEN a.parent_id = 0 THEN 1 END DESC,       
        //                 name
        //         LIMIT '.$offset.', '.$this->perpage;
        // $query = $this->db->query($sql);
        // debug($query->result());
        // if not empty
        if($query->num_rows() > 0) {
            // get groups
            $sql                    = $this->db->get('nav_groups');
            $this->data['groups']   = $sql->result();

            foreach($query->result() as $list) {
                // check for update access
                if($this->menu->crud_access('update')) {
                    $this->update = '<a href="'.site_url($this->url.'/'.'update'.'/'.$list->id).'" class="btn btn-xs btn-default"><i class="fa fa-pencil"></i></a>';
                }

                // check for delete access
                if($this->menu->crud_access('delete')) {
                    $this->delete = '<a href="'.site_url($this->url.'/'.'delete'.'/'.$list->id).'" class="delete btn btn-xs btn-default"><i class="fa fa-trash"></i></a>';
                }

                // return to table data
                $table_row = array(
                                    ($list->parent_id == 0) ? '<strong>'.$list->name.'</strong>' : '<span class="text-muted">'.$list->name.'</span>', 
                                    $this->getParent($list->parent_id),
                                    $list->slug,
                                    $list->updated_at
                );

                // check for additional link access
                if($this->menu->crud_access('update') || $this->menu->crud_access('delete')) {
                    $table_row = array_merge($table_row, array($this->delete.' '.$this->update));
                }

                $this->table->add_row($table_row);
            }

            // generate table header
            $this->table->set_template(array('table_open' => '<table class="table">'));
            if($this->menu->crud_access('update') || $this->menu->crud_access('delete')) {
                $this->table->set_heading('Menu', 'Parent', 'Path', 'Created At', '');
            } else {
                $this->table->set_heading('Menu', 'Parent', 'Path', 'Created At');
            }

            // output
            $this->data['list']         = $this->table->generate(); 
            $this->data['content']      = $this->load->view('nav/list', $this->data, TRUE);
        } else {
            // if no data
            $this->data['content']      = $this->load->view('admin/blank', $this->data, TRUE);                    
        }

        // make an output
        $this->load->view('admin/content', $this->data);
    }

    // Add a new item
    public function add()
    {
        // check for write access
        $this->menu->check_access('write');

        // get language list
        $this->db
        ->order_by('is_default','desc');
        $query = $this->db->get('lang');
        $this->data['language'] = $query->result();

        // validation rules
        foreach($this->data['language'] as $lang) {
            $this->form_validation->set_rules("name[".$lang->shortname."]", "Menu's Name in ".$lang->name, 'required');
        }

        $this->db
        ->select('a.id, a.parent_id, b.name')
        ->from($this->table_name.' a')
        ->join('nav_lang b', 'b.id = a.id')
        ->where('a.parent_id', '0')
        ->order_by('a.id');
        $parents = $this->db->get();
        $set_parent[] = 'As Parents';
        if($parents->row()) {        
            foreach($parents->result() as $parent) {
                $set_parent[$parent->id] = $parent->name;
            } 
        }

        $this->data['dropdown_opt']     = $set_parent;

        // get nav groups
        $set_group = array();
        $query = $this->db->get('nav_groups');
        if($query->num_rows() > 0) {        
            foreach($query->result() as $group) {
                $set_group[$group->id] = $group->name;
            } 
        }

        $this->data['dropdown_groups']  = $set_group;

        // get page
        $this->db
        ->select('a.id, b.title')
        ->from('page a')
        ->join('page_lang b', 'b.id = a.id', 'left')        
        ->where('a.type', 'page')
        ->where('b.lang', $this->session->userdata('lang'));
        $query = $this->db->get();
        if($query->num_rows() > 0) {
            foreach ($query->result() as $page) {
                $this->data['pages'][$page->id] = $page->title;
            }
        }

        // get category
        $this->db
        ->select('a.id, b.name')
        ->from('category a')
        ->join('category_lang b', 'b.id = a.id', 'left')        
        ->where('a.type', 'post')
        ->where('b.lang', $this->session->userdata('lang'));
        $query = $this->db->get();
        if($query->num_rows() > 0) {
            foreach ($query->result() as $category) {
                $this->data['category'][$category->id] = $category->name;
            }
        }

        // if break the rules
        if ($this->form_validation->run() == FALSE) {
            $this->data['content'] = $this->load->view('nav/add', $this->data, true);
        } else {
            // if not break the rules 
            $type = $this->input->post('type');
            switch($type) {
                case '#page' :
                $slug = $this->input->post('page');
                break;

                case '#category' :
                $slug = $this->input->post('category');
                break;

                case '#web' :
                $slug = $this->input->post('web');
                break;

                case '#module' :
                $slug = $this->input->post('module');
                break;

            }

            $record = array(
                'nav_id'    => $this->input->post('groups') ,
                'parent_id' => $this->input->post('parent') ,
                'slug'     => $slug,
                'type'      => substr($this->input->post('type'), 1, strlen($this->input->post('type')))
            );

            // start transaction
            $this->db->insert($this->table_name, $record);
            $id = $this->db->insert_id();

            // then repeat by language
            foreach($this->input->post('name') as $key => $val) {
                $record = array(
                    'id'        => $id,
                    'lang'      => $key,
                    'name'      => $val
                );

                $this->db->trans_start();
                $this->db->insert('nav_lang', $record);
                $this->db->trans_complete();
            }

            // check for status
            if ($this->db->trans_status() === FALSE) {
                $db_error                   = $this->db->error();
                $this->data['message']      = $db_error['message'];
                $this->data['content']      = $this->load->view('nav/add', $this->data, true);
            } else {
                $this->data['message']      = 'Your record has been saved.';
                $this->data['button_link']  = site_url('nav');
                $this->data['content']      = $this->load->view('admin/redirect', $this->data, TRUE);
            }
        }

        // make an output
        $this->load->view('admin/content', $this->data);        
    }

    //Update one item
    public function update( $id = null )
    {
        // check for write access
        $this->menu->check_access('update');

        // get language list
        $this->db->order_by('is_default','desc');
        $query = $this->db->get('lang');
        $this->data['language'] = $query->result();

        // create output by available language
        foreach($this->data['language'] as $lang) {
            // get current data
            $this->db
            ->from($this->table_name.' a')
            ->join('nav_lang b', 'b.id = a.id', 'left')
            ->where('a.id', $id)
            ->where('b.lang', $lang->shortname);
            $query = $this->db->get();

            // return data
            $this->data['form'][$lang->shortname]     = $query->row();
        }

        // validation rules
        foreach($this->data['language'] as $lang) {
            $this->form_validation->set_rules("name[".$lang->shortname."]", "Menu's Name in ".$lang->name, 'required');
        }

        $this->db
        ->select('a.id, a.parent_id, b.name')
        ->from($this->table_name.' a')
        ->join('cms_nav_lang b', 'b.id = a.id', 'left')
        ->where('a.parent_id', '0')
        ->order_by('a.id');
        $parents = $this->db->get();
        $set_parent[] = 'As Parents';
        if($parents->row()) {        
            foreach($parents->result() as $parent) {
                $set_parent[$parent->id] = $parent->name;
            } 
        }

        $this->data['dropdown_opt']     = $set_parent;

        // get nav groups
        $set_group = array();
        $query = $this->db->get('nav_groups');
        if($query->num_rows() > 0) {        
            foreach($query->result() as $group) {
                $set_group[$group->id] = $group->name;
            } 
        }

        $this->data['dropdown_groups']  = $set_group;

        // get page
        $this->db
        ->select('a.id, b.title')
        ->from('page a')
        ->join('page_lang b', 'b.id = a.id', 'left')        
        ->where('a.type', 'page')
        ->where('b.lang', $this->session->userdata('lang'));
        $query = $this->db->get();
        if($query->num_rows() > 0) {
            foreach ($query->result() as $page) {
                $this->data['pages'][$page->id] = $page->title;
            }
        }

        // get category
        $this->db
        ->select('a.id, b.name')
        ->from('category a')
        ->join('category_lang b', 'b.id = a.id', 'left')        
        ->where('a.type', 'post')
        ->where('b.lang', $this->session->userdata('lang'));
        $query = $this->db->get();
        if($query->num_rows() > 0) {
            foreach ($query->result() as $category) {
                $this->data['category'][$category->id] = $category->name;
            }
        }

        // if break the rules
        if ($this->form_validation->run() == FALSE) {
            $this->data['content'] = $this->load->view('nav/edit', $this->data, true);
        } else {
            // if not break the rules 
            $type = $this->input->post('type');
            switch($type) {
                case '#page' :
                $slug = $this->input->post('page');
                break;

                case '#category' :
                $slug = $this->input->post('category');
                break;

                case '#web' :
                $slug = $this->input->post('web');
                break;

                case '#module' :
                $slug = $this->input->post('module');
                break;

            }

            $record = array(
                'parent_id' => $this->input->post('parent') ,
                'nav_id'    => $this->input->post('groups') ,
                'slug'      => $slug,
                'type'      => substr($this->input->post('type'), 1, strlen($this->input->post('type')))
            );

            // start transaction
            $this->db->trans_start();
            $this->db->where('id', $id);
            $this->db->update($this->table_name, $record);
            $this->db->trans_complete();

            // check for status
            if ($this->db->trans_status() === FALSE) {
                $db_error                   = $this->db->error();
                $this->data['message']      = $db_error['message'];
                $this->data['content']      = $this->load->view('nav/edit', $this->data, true);
            } else {
                foreach($this->input->post('name') as $key => $val) {
                    $record = array(
                        'name'      => $val
                    );

                    $this->db
                    ->where('lang', $key)
                    ->where('id', $this->input->post('id'));
                    $this->db->update('nav_lang', $record);
                }

                $this->data['message']      = 'Your record has been saved.';
                $this->data['button_link']  = site_url('nav');
                $this->data['content']      = $this->load->view('admin/redirect', $this->data, TRUE);
            }
        }

        // make an output
        $this->load->view('admin/content', $this->data);        
    }

    //Delete one item
    public function delete( $id = NULL )
    {
        // security check
        $this->menu->check_access('delete');

        $this->db->trans_start();
        $this->db->where('id', $id);
        $this->db->delete($this->table_name);
        $this->db->trans_complete();
        if ($this->db->trans_status() === FALSE) {
            $message = 'Fail while removed your record.';
        } else {
            $message = 'Your record has been removed.';
        }
        $this->data['message']          = $message;
        $this->data['content']          = $this->load->view('admin/redirect', $this->data, TRUE);
        $this->load->view('admin/content', $this->data);        
    }

    public function getParent($parent_id)
    {   
        $output = '-';
        if(isset($parent_id) && ($parent_id != 0)) {
            $this->db
            ->select('b.name')
            ->from($this->table_name. ' a')
            ->join('cms_nav_lang b', 'b.id = a.id', 'left')
            ->where('a.id', $parent_id);
            $parents = $this->db->get();
            if($parents->num_rows() > 0 ){
                $parent = $parents->row();                
                $output = $parent->name;            
            }
        }
        return $output;
    }

    public function filter( $id = '', $offset = null )
    {
        // get groups
        $query = $this->db->get('cms_nav_groups');
        $this->data['groups']  = $query->result();

        // pagination 
        if(isset($id) && ($id != null)) {
            $this->db->where('nav_id', $id);
        }

        // pagination 
        $this->db
        ->select('a.id')
        ->from($this->table_name.' a')
        ->join('cms_nav_lang b','b.id = a.id', 'left')
        ->where('b.lang', $this->session->userdata('lang'));
        $query = $this->db->get();
        $count                      = $query->num_rows();
        $config['per_page']         = $this->perpage;
        $this->pagination->initialize($this->general_model->pagination_rules(site_url($this->url), $count, $this->perpage));
        $this->data['pagination']   = $this->pagination->create_links();

        // get all data
        $this->db
        ->select('a.id, a.parent_id, a.slug, b.name, DATE_FORMAT(a.updated_at,\'%d %M %Y %T\') updated_at')
        ->from($this->table_name.' a')
        ->join('cms_nav_lang b','b.id = a.id', 'left')
        ->where('b.lang', $this->session->userdata('lang'))
        ->order_by('a.id')
        ->limit($this->perpage, $offset);
        $query = $this->db->get();

        // if not empty
        if($query->num_rows() > 0) {
            foreach($query->result() as $list) {
                // check for update access
                if($this->menu->crud_access('update')) {
                    $this->update = '<a href="'.site_url($this->url.'/'.'update'.'/'.$list->id).'" class="btn btn-xs btn-default"><i class="fa fa-pencil"></i></a>';
                }

                // check for delete access
                if($this->menu->crud_access('delete')) {
                    $this->delete = '<a href="'.site_url($this->url.'/'.'delete'.'/'.$list->id).'" class="delete btn btn-xs btn-default"><i class="fa fa-trash"></i></a>';
                }

                // return to table data
                $table_row = array(
                                    ($list->parent_id == 0) ? '<strong>'.$list->name.'</strong>' : '<span class="text-muted">'.$list->name.'</span>', 
                                    $this->getParent($list->parent_id),
                                    $list->slug,
                                    $list->updated_at
                );

                // check for additional link access
                if($this->menu->crud_access('update') || $this->menu->crud_access('delete')) {
                    $table_row = array_merge($table_row, array($this->delete.' '.$this->update));
                }

                $this->table->add_row($table_row);
            }

            // generate table header
            $this->table->set_template(array('table_open' => '<table class="table">'));
            if($this->menu->crud_access('update') || $this->menu->crud_access('delete')) {
                $this->table->set_heading('Menu', 'Parent', 'Path', 'Created At', '');
            } else {
                $this->table->set_heading('Menu', 'Parent', 'Path', 'Created At');
            }

            // output
            $this->data['list']         = $this->table->generate(); 
            $this->data['content']      = $this->load->view('nav/list', $this->data, TRUE);
        } else {
            // if no data
            $this->data['content']      = $this->load->view('admin/blank', $this->data, TRUE);                    
        }

        // make an output
        $this->load->view('admin/content', $this->data);
    }

}

/* End of file Category.php */
/* Location: ./application/modules/admin/controllers/Category.php */
