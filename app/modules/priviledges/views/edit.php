<?php if(isset($message)) : ?>
<div class="alert alert-danger">
    <?php echo $message; ?>
</div>
<?php endif ?>
<?php if(validation_errors()) : ?>
<div class="alert alert-danger">
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
    <strong>Error</strong>
    <hr>
    <?php echo validation_errors(); ?>
</div>
<?php endif ?>
<div class="box tm-padding">
    <div class="box-body no-padding">
		<?php if($groups != '' ) { ?>
			<form action="<?php echo current_url()?>" method="post" role="form" class="form-horizontal" data-validate="parsley">
				<input type="hidden" name="groups" value="<?php echo $id ?>">
				<div class="col-md-12" style="padding:10px 0 0 0;">
					<div class="well" style="box-shadow: none;">
					<h2 style="color:red">Note:</h2>
					<p>You will change the priviledges for <strong><?php echo ucwords($groups) ?></strong> groups.</p>
					<p>Miss of configuration may cause you lose some priviledges.</p>
					</div>
				</div>
				<div class="clearfix"></div>
				<table class="table table-bordered">
					<thead>
						<tr>
							<th>Modul</th>
							<th class="text-center" width="1">Access</th>
							<th class="text-center" width="1">Write</th>
							<th class="text-center" width="1">Update</th>
							<th class="text-center" width="1">Delete</th>
						</tr>
					</thead>
					<tbody>
						<?php $i=0; foreach ($menu as $key => $modul) : ?>
						<tr>
							<td><label for="check<?php echo $i?>"><?php echo $modul['menu'] ?></label> <?php echo ($modul['show']) ? '' : '<em style="color:red;">(Hidden)</em>'?></td>
							<td class="text-center" style="padding-top:14px !important;"><input type="checkbox" id="check<?php echo $i?>" name="modules[<?php echo $i ?>][id]" value="<?php echo $modul['id'] ?>" <?php echo ($modul['access'] == 1) ? 'checked' : '' ?> ></td>
	
							<?php if($modul['has_write']) : ?>
							<td class="text-center" style="padding-top:14px !important;"><input type="checkbox" id="check<?php echo $i?>" name="modules[<?php echo $i ?>][write]" value="1" <?php echo ($modul['can_write'] == 1) ? 'checked' : '' ?>></td>
							<?php else : ?>
							<td></td>
							<?php endif ?>

							<?php if($modul['has_update']) : ?>
							<td class="text-center" style="padding-top:14px !important;"><input type="checkbox" id="check<?php echo $i?>" name="modules[<?php echo $i ?>][update]" value="1" <?php echo ($modul['can_update'] == 1) ? 'checked' : '' ?>></td>
							<?php else : ?>
							<td></td>
							<?php endif ?>

							<?php if($modul['has_delete']) : ?>
							<td class="text-center" style="padding-top:14px !important;"><input type="checkbox" id="check<?php echo $i?>" name="modules[<?php echo $i ?>][delete]" value="1" <?php echo ($modul['can_delete'] == 1) ? 'checked' : '' ?>></td>
							<?php else : ?>
							<td></td>
							<?php endif ?>
						</tr>
						<?php
							$x = 0;
							if(count($modul['sub']) > 0 ) :
							foreach ($modul['sub'] as $key => $sub) : ?>
							<tr>
								<td><label for="check<?php echo $i.$x?>">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<i class="fa fa-angle-right"></i>&nbsp;&nbsp;<?php echo $sub['menu'] ?></label> <?php echo ($sub['show']) ? '' : '<em style="color:red;">(Hidden)</en>' ?></td>
								<td class="text-center" style="padding-top:14px !important;"><input type="checkbox" id="check<?php echo $i.$x?>" name="modules[<?php echo $i.$x?>][id]" value="<?php echo $sub['id'] ?>" <?php echo ($sub['access'] == 1) ? 'checked' : '' ?>></td>

								<?php if($sub['has_write']) : ?>
								<td class="text-center" style="padding-top:14px !important;"><input type="checkbox" id="check<?php echo $i.$x?>" name="modules[<?php echo $i.$x?>][write]" value="1" <?php echo ($sub['can_write'] == 1) ? 'checked' : '' ?>></td>
								<?php else : ?>
								<td></td>
								<?php endif ?>

								<?php if($sub['has_update']) : ?>
								<td class="text-center" style="padding-top:14px !important;"><input type="checkbox" id="check<?php echo $i.$x?>" name="modules[<?php echo $i.$x?>][update]" value="1" <?php echo ($sub['can_update'] == 1) ? 'checked' : '' ?>></td>
								<?php else : ?>
								<td></td>
								<?php endif ?>

								<?php if($sub['has_delete']) : ?>
								<td class="text-center" style="padding-top:14px !important;"><input type="checkbox" id="check<?php echo $i.$x?>" name="modules[<?php echo $i.$x?>][delete]" value="1" <?php echo ($sub['can_delete']  == 1) ? 'checked' : '' ?>></td>
								<?php else : ?>
								<td></td>
								<?php endif ?>
							</tr>                
						<?php 
							$x++; 
							endforeach; 
						endif;
						$i++;
					endforeach; ?>
					</tbody>
				</table>
				<div class="text-right">
					<button type="reset" class="btn tm-btn">Cancel</button>
					<button type="submit" class="btn tm-btn">Save</button>
				</div>
			</form>
		<?php } else { ?>
			<div class="alert alert-info">
				<h3>Info</h3>
				<p>
					Hak akses group per modul sudah dibuat seluruhnya. 
					<br>
					Hanya dapat melakukan perubahan pada Hak akses yang telah ada atau membuat group baru terlebih dahulu.  
				</p>
			</div>
		<?php } ?>
	</div>
</div>
<style>
	.table td {
		padding: 0 15px !important;
	}

	.table td label {
		font-weight: normal;
	}

</style>
