<script>
var color = 1;
function add_fields_col() {
    color++;
    var objTo = document.getElementById('room_fileds_col')
    var divCol = document.createElement("div");
    divCol.innerHTML = '<div class="form-group"><label class="col-sm-4">Color ' + color +'</label><div class="col-sm-2"><input class="form-control" type="color" name="color[]" value="#ff0000"></div></div>';
    
    objTo.appendChild(divCol)
}


</script>

<script type="text/javascript">
var size = 1;
function add_fields_si() {
    size++;
    var objTo = document.getElementById('room_fileds_si')
    var divSi = document.createElement("div");
    divSi.innerHTML = '<div class="form-group"><label for="" class="col-sm-4"s>Size ' + size +'</label><div class="col-sm-3"><input type="text" name="size[]" value=""  class="form-control" /></div></div>';
    
    objTo.appendChild(divSi)
}
</script>

<?php if(isset($message)) : ?>
<div class="alert alert-danger">
    <?php echo $message; ?>
</div>
<?php endif ?>
<?php if(validation_errors()) : ?>
<div class="alert alert-danger">
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
    <strong>Error</strong>
    <hr>
    <?php echo validation_errors(); ?>
</div>
<?php endif ?>
<?php echo form_open_multipart(current_url(), 'class="form-horizontal" role="form"');?>
<div class="box">
    <div class="box-header with-border">
        <h3 class="box-title">Please complete the form below.</h3>
    </div>
    <div class="box-body tm-padding">            
        <div class="form-group has-error">
            <label for="" class="col-sm-4">ID</label>
            <div class="col-sm-5">
            <?php echo form_input('id',set_value('id', $videocat->id),'class="form-control"');?>
            <span id="" class="help-block ">* Please be careful to edit Category ID</span>
            </div>
        </div>
             
        <div class="form-group">
            <label for="" class="col-sm-4">Name</label>
            <div class="col-sm-8">
            <?php echo form_input('name',set_value('name', $videocat->name),'class="form-control"');?>
            </div>
        </div>

        <div class="box-header with-border">
            <h3 class="box-title"> Icon</h3>
        </div>
        
        <div class="row">
            <div class="col-md-offset-4 col-md-3 ">
                <img src="<?php echo base_url().'assets/video/category/'.$videocat->image; ?>" class="img-thumbnail" alt="Cinque Terre">
            </div>
        </div>
        <br>
        <div class="form-group">
            <label for="" class="col-sm-4">New Icon</label>
            <div class="col-sm-4">
                <input type="file" class="form-control" name="userFiles"/>
            </div>
        </div>
        <hr>
       
    </div>                                
</div>
<div class="panel-footer text-right">
    <a href="<?php echo site_url('videocat') ?>" class="btn tm-btn">Cancel</a>
    <button type="submit" class="btn tm-btn">Submit</button>
</div>
</div>
<?php echo form_close();?>
