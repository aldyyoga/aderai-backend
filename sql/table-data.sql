-- --------------------------------------------------------
-- Host:                         localhost
-- Versi server:                 10.1.9-MariaDB-log - mariadb.org binary distribution
-- OS Server:                    Win32
-- HeidiSQL Versi:               9.3.0.4984
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
-- Dumping data for table aderai.cms_category: 9 rows
/*!40000 ALTER TABLE `cms_category` DISABLE KEYS */;
INSERT INTO `cms_category` (`id`, `type`, `is_default`, `created_at`, `updated_at`) VALUES
	(1, 'folder', 0, '2016-05-18 05:59:28', '2016-05-18 05:59:28'),
	(2, 'folder', 0, '2016-05-18 06:56:50', '2016-05-18 06:56:50'),
	(3, 'folder', 0, '2016-05-18 06:57:07', '2016-05-18 06:57:07'),
	(4, 'folder', 0, '2016-05-18 06:57:27', '2016-05-18 06:57:27'),
	(5, 'folder', 0, '2016-05-18 06:57:49', '2016-05-18 06:57:49'),
	(6, 'post', 1, '2016-05-18 08:03:55', '2016-05-18 08:03:55'),
	(7, 'post', NULL, '2016-05-18 08:04:52', '2016-05-18 08:04:52'),
	(8, 'post', NULL, '2016-05-18 08:05:31', '2016-05-18 08:05:31'),
	(9, 'post', NULL, '2016-05-18 04:31:51', '2016-05-18 04:31:51');
/*!40000 ALTER TABLE `cms_category` ENABLE KEYS */;

-- Dumping data for table aderai.cms_category_lang: 18 rows
/*!40000 ALTER TABLE `cms_category_lang` DISABLE KEYS */;
INSERT INTO `cms_category_lang` (`lang`, `id`, `name`, `slug`) VALUES
	('en', 1, 'Blogs', 'blogs'),
	('id', 1, 'Berita', 'berita'),
	('en', 2, 'Slider', 'slider'),
	('id', 2, 'Slider', 'slider'),
	('en', 3, 'Gallery', 'gallery'),
	('id', 3, 'Galeri', 'galeri'),
	('en', 4, 'Theme', 'theme'),
	('id', 4, 'Tema', 'tema'),
	('en', 5, 'Download', 'download'),
	('id', 5, 'Unduh', 'unduh'),
	('en', 6, 'Event', 'event'),
	('id', 6, 'Kegiatan', 'kegiatan'),
	('en', 7, 'Seminar', 'seminar'),
	('id', 7, 'Seminar', 'seminar'),
	('en', 8, 'Study', 'study'),
	('id', 8, 'Kajian', 'kajian'),
	('en', 9, 'Announcements', 'announcements'),
	('id', 9, 'Pengumuman', 'pengumuman');
/*!40000 ALTER TABLE `cms_category_lang` ENABLE KEYS */;

-- Dumping data for table aderai.cms_config: 14 rows
/*!40000 ALTER TABLE `cms_config` DISABLE KEYS */;
INSERT INTO `cms_config` (`id`, `name`, `value`, `label`, `groups`) VALUES
	(1, 'site_name', 'Ade Rai Apps', 'Site Name', 'general'),
	(2, 'site_url', 'ade.rai', 'Site URL', 'general'),
	(3, 'site_tagline', 'For Your Healthy', 'Tagline', 'general'),
	(4, 'email', 'info@ade.rai', 'Email', 'general'),
	(5, 'phone', '+62 21 123456', 'Phone', 'general'),
	(6, 'fax', '+62 21 123456', 'Fax.', 'general'),
	(7, 'address_1', 'Jalan', 'Address (Line 1)', 'general'),
	(8, 'address_2', 'Raya Lintas Jakarta', 'Address (Line 2)', 'general'),
	(9, 'facebook', 'ade.rai', 'Facebook', 'general'),
	(10, 'date_format', 'd/m/Y h:i:s A', 'Default Date Format', 'configuration'),
	(11, 'rss', 'rss', 'RSS Address', 'configuration'),
	(12, 'meta_description', 'Ade Rai Apps - For Your Healthy', 'Meta Description', 'configuration'),
	(13, 'registration', 'Pendaftaran Anggota', 'Registration Title', 'information'),
	(14, 'registration_info', 'Saya ingin mendaftar sebagai anggota.', 'Registration Description', 'information');
/*!40000 ALTER TABLE `cms_config` ENABLE KEYS */;

-- Dumping data for table aderai.cms_event: 0 rows
/*!40000 ALTER TABLE `cms_event` DISABLE KEYS */;
/*!40000 ALTER TABLE `cms_event` ENABLE KEYS */;

-- Dumping data for table aderai.cms_event_lang: 0 rows
/*!40000 ALTER TABLE `cms_event_lang` DISABLE KEYS */;
/*!40000 ALTER TABLE `cms_event_lang` ENABLE KEYS */;

-- Dumping data for table aderai.cms_groups: ~2 rows (approximately)
/*!40000 ALTER TABLE `cms_groups` DISABLE KEYS */;
INSERT INTO `cms_groups` (`id`, `name`, `description`) VALUES
	(0, 'Administrator', 'Highest level of users privillegges. Do not remove.'),
	(1, 'Members', 'Set as default groups for new user. ');
/*!40000 ALTER TABLE `cms_groups` ENABLE KEYS */;

-- Dumping data for table aderai.cms_lang: 2 rows
/*!40000 ALTER TABLE `cms_lang` DISABLE KEYS */;
INSERT INTO `cms_lang` (`id`, `shortname`, `name`, `is_default`, `updated_at`) VALUES
	(1, 'id', 'Bahasa', 1, '2016-05-24 13:06:49'),
	(2, 'eng', 'English', 0, '2016-05-24 13:06:49');
/*!40000 ALTER TABLE `cms_lang` ENABLE KEYS */;

-- Dumping data for table aderai.cms_logs: 0 rows
/*!40000 ALTER TABLE `cms_logs` DISABLE KEYS */;
/*!40000 ALTER TABLE `cms_logs` ENABLE KEYS */;

-- Dumping data for table aderai.cms_media: 0 rows
/*!40000 ALTER TABLE `cms_media` DISABLE KEYS */;
/*!40000 ALTER TABLE `cms_media` ENABLE KEYS */;

-- Dumping data for table aderai.cms_media_lang: 0 rows
/*!40000 ALTER TABLE `cms_media_lang` DISABLE KEYS */;
/*!40000 ALTER TABLE `cms_media_lang` ENABLE KEYS */;

-- Dumping data for table aderai.cms_member: 0 rows
/*!40000 ALTER TABLE `cms_member` DISABLE KEYS */;
/*!40000 ALTER TABLE `cms_member` ENABLE KEYS */;

-- Dumping data for table aderai.cms_member_tokens: 0 rows
/*!40000 ALTER TABLE `cms_member_tokens` DISABLE KEYS */;
/*!40000 ALTER TABLE `cms_member_tokens` ENABLE KEYS */;

-- Dumping data for table aderai.cms_message: 0 rows
/*!40000 ALTER TABLE `cms_message` DISABLE KEYS */;
/*!40000 ALTER TABLE `cms_message` ENABLE KEYS */;

-- Dumping data for table aderai.cms_modules: 28 rows
/*!40000 ALTER TABLE `cms_modules` DISABLE KEYS */;
INSERT INTO `cms_modules` (`id`, `name`, `parent`, `show`, `path`, `icon`, `has_write`, `has_update`, `has_delete`, `orders`) VALUES
	(1, 'Dashboard', 0, 1, 'dashboard', 'ion-android-calendar', NULL, NULL, NULL, 0),
	(2, 'Systems', 0, 1, '#', 'ion-android-settings', NULL, NULL, NULL, 23),
	(3, 'Modules', 2, 1, 'packages', 'fa-folder-open', 1, 1, 1, 26),
	(4, 'Priviledges', 8, 1, 'priviledges', 'fa-lock', 1, 1, 1, 22),
	(5, 'General', 2, 1, 'general', 'fa-desktop', 1, 1, 1, 24),
	(6, 'Groups', 8, 1, 'groups', 'fa-sitemap', 1, 1, 1, 21),
	(7, 'Logs System', 2, 1, 'logs', 'fa-rocket', 0, 0, 0, 28),
	(8, 'Access Manager', 0, 1, '#', 'ion-android-person', NULL, NULL, NULL, 19),
	(9, 'Users', 8, 1, 'auth', '', 1, 1, 1, 20),
	(10, 'Contents', 0, 1, '#', 'ion-android-list', NULL, NULL, NULL, 7),
	(11, 'Blogs', 10, 1, 'blogs', '', 1, 1, 1, 8),
	(12, 'Pages', 10, 1, 'pages', '', 1, 1, 1, 9),
	(13, 'Category', 10, 1, 'category', '', 1, 1, 1, 10),
	(14, 'Media', 0, 1, '#', 'ion-android-image', NULL, NULL, NULL, 13),
	(15, 'Folders', 14, 1, 'folders', '', 1, 1, 1, 14),
	(16, 'Files', 14, 1, 'files', '', 1, 1, 1, 15),
	(18, 'Events', 10, 1, 'events', '', 1, 1, 1, 11),
	(19, 'Transactions', 0, 1, '#', 'ion-card', NULL, NULL, NULL, 3),
	(29, 'Configuration', 19, 1, 'payment', '', 1, 1, 1, 6),
	(20, 'Navigations', 0, 1, '#', 'ion-share', NULL, NULL, NULL, 16),
	(21, 'Accounts', 0, 1, '#', 'ion-android-contact', 1, 1, 1, 1),
	(25, 'Language', 2, 1, 'langs', 'ion ion-ios-barcode-outline', 1, 1, 1, 25),
	(26, 'Groups', 20, 1, 'navgroups', '', 1, 1, 1, 17),
	(27, 'Links', 20, 1, 'nav', '', 1, 1, 1, 18),
	(28, 'Translate', 2, 1, 'translate', '', 1, 1, 1, 27),
	(30, 'Balances', 19, 1, 'balance', '', 1, 1, 1, 4),
	(31, 'Report', 19, 1, 'report', '', 1, 1, 1, 5),
	(32, 'Video', 10, 1, 'video', '', 1, 1, 1, 12),
	(33, 'Membership', 21, 1, 'members', '', 1, 1, 1, 2);
/*!40000 ALTER TABLE `cms_modules` ENABLE KEYS */;

-- Dumping data for table aderai.cms_nav: 1 rows
/*!40000 ALTER TABLE `cms_nav` DISABLE KEYS */;
INSERT INTO `cms_nav` (`id`, `nav_id`, `parent_id`, `slug`, `type`, `updated_at`) VALUES
	(1, 1, 0, '/', 'module', '2016-05-24 13:08:42');
/*!40000 ALTER TABLE `cms_nav` ENABLE KEYS */;

-- Dumping data for table aderai.cms_nav_groups: 1 rows
/*!40000 ALTER TABLE `cms_nav_groups` DISABLE KEYS */;
INSERT INTO `cms_nav_groups` (`id`, `name`, `slug`, `updated_at`) VALUES
	(1, 'Main Navigation', 'main', '2016-05-24 08:37:56');
/*!40000 ALTER TABLE `cms_nav_groups` ENABLE KEYS */;

-- Dumping data for table aderai.cms_nav_lang: 2 rows
/*!40000 ALTER TABLE `cms_nav_lang` DISABLE KEYS */;
INSERT INTO `cms_nav_lang` (`lang`, `id`, `name`) VALUES
	('eng', 1, 'Home'),
	('id', 1, 'Beranda');
/*!40000 ALTER TABLE `cms_nav_lang` ENABLE KEYS */;

-- Dumping data for table aderai.cms_page: 0 rows
/*!40000 ALTER TABLE `cms_page` DISABLE KEYS */;
/*!40000 ALTER TABLE `cms_page` ENABLE KEYS */;

-- Dumping data for table aderai.cms_page_lang: 0 rows
/*!40000 ALTER TABLE `cms_page_lang` DISABLE KEYS */;
/*!40000 ALTER TABLE `cms_page_lang` ENABLE KEYS */;

-- Dumping data for table aderai.cms_priviledges: 28 rows
/*!40000 ALTER TABLE `cms_priviledges` DISABLE KEYS */;
INSERT INTO `cms_priviledges` (`modules_id`, `groups_id`, `can_write`, `can_update`, `can_delete`, `created`, `created_by`) VALUES
	(21, 0, 1, 1, 0, '2016-06-09 08:06:56', 1),
	(27, 0, 1, 1, 1, '2016-06-09 08:06:56', 1),
	(26, 0, 1, 1, 1, '2016-06-09 08:06:56', 1),
	(20, 0, 0, 0, 0, '2016-06-09 08:06:56', 1),
	(31, 0, 1, 1, 1, '2016-06-09 08:06:56', 1),
	(30, 0, 1, 1, 1, '2016-06-09 08:06:56', 1),
	(29, 0, 1, 1, 1, '2016-06-09 08:06:56', 1),
	(19, 0, 0, 0, 0, '2016-06-09 08:06:56', 1),
	(16, 0, 1, 1, 1, '2016-06-09 08:06:56', 1),
	(15, 0, 1, 1, 1, '2016-06-09 08:06:56', 1),
	(14, 0, 0, 0, 0, '2016-06-09 08:06:56', 1),
	(32, 0, 1, 1, 1, '2016-06-09 08:06:56', 1),
	(18, 0, 1, 1, 1, '2016-06-09 08:06:56', 1),
	(13, 0, 1, 1, 1, '2016-06-09 08:06:56', 1),
	(12, 0, 1, 1, 1, '2016-06-09 08:06:56', 1),
	(11, 0, 1, 1, 1, '2016-06-09 08:06:56', 1),
	(10, 0, 0, 0, 0, '2016-06-09 08:06:56', 1),
	(9, 0, 1, 1, 1, '2016-06-09 08:06:56', 1),
	(6, 0, 1, 1, 1, '2016-06-09 08:06:56', 1),
	(4, 0, 1, 1, 1, '2016-06-09 08:06:56', 1),
	(33, 0, 1, 1, 1, '2016-06-09 08:06:56', 1),
	(8, 0, 0, 0, 0, '2016-06-09 08:06:56', 1),
	(28, 0, 1, 1, 1, '2016-06-09 08:06:56', 1),
	(25, 0, 1, 1, 1, '2016-06-09 08:06:56', 1),
	(7, 0, 0, 0, 0, '2016-06-09 08:06:56', 1),
	(5, 0, 1, 1, 1, '2016-06-09 08:06:56', 1),
	(3, 0, 1, 1, 1, '2016-06-09 08:06:56', 1),
	(2, 0, 0, 0, 0, '2016-06-09 08:06:56', 1),
	(1, 0, 0, 0, 0, '2016-06-09 08:06:56', 1);
/*!40000 ALTER TABLE `cms_priviledges` ENABLE KEYS */;

-- Dumping data for table aderai.cms_sessions: ~0 rows (approximately)
/*!40000 ALTER TABLE `cms_sessions` DISABLE KEYS */;
INSERT INTO `cms_sessions` (`id`, `ip_address`, `timestamp`, `data`) VALUES
	('bf6a63c008b92b50edbd0968df092d9890c7ee2f', '::1', 1465474620, _binary 0x5F5F63695F6C6173745F726567656E65726174657C693A313436353437343335393B6964656E746974797C733A31353A2261646D696E4061646D696E2E636F6D223B656D61696C7C733A31353A2261646D696E4061646D696E2E636F6D223B757365725F69647C733A313A2231223B67726F75705F69647C733A313A2230223B6F6C645F6C6173745F6C6F67696E7C733A31303A2231343635343635363330223B6C616E677C733A323A226964223B),
	('f261ccd021118e1ff2683297f35851899753fa9a', '::1', 1465474357, _binary 0x5F5F63695F6C6173745F726567656E65726174657C693A313436353437343035373B6964656E746974797C733A31353A2261646D696E4061646D696E2E636F6D223B656D61696C7C733A31353A2261646D696E4061646D696E2E636F6D223B757365725F69647C733A313A2231223B67726F75705F69647C733A313A2230223B6F6C645F6C6173745F6C6F67696E7C733A31303A2231343635343635363330223B6C616E677C733A323A226964223B),
	('f6787d72708de8167fc71fe6f1554880d5215dbd', '::1', 1465472721, _binary 0x5F5F63695F6C6173745F726567656E65726174657C693A313436353437323730303B6964656E746974797C733A31353A2261646D696E4061646D696E2E636F6D223B656D61696C7C733A31353A2261646D696E4061646D696E2E636F6D223B757365725F69647C733A313A2231223B67726F75705F69647C733A313A2230223B6F6C645F6C6173745F6C6F67696E7C733A31303A2231343635343635363330223B6C616E677C733A323A226964223B),
	('f7e094a4cb88565ae10ccefa06dce4f8614d2f12', '::1', 1465474053, _binary 0x5F5F63695F6C6173745F726567656E65726174657C693A313436353437333735343B6964656E746974797C733A31353A2261646D696E4061646D696E2E636F6D223B656D61696C7C733A31353A2261646D696E4061646D696E2E636F6D223B757365725F69647C733A313A2231223B67726F75705F69647C733A313A2230223B6F6C645F6C6173745F6C6F67696E7C733A31303A2231343635343635363330223B6C616E677C733A323A226964223B);
/*!40000 ALTER TABLE `cms_sessions` ENABLE KEYS */;

-- Dumping data for table aderai.cms_stats: 0 rows
/*!40000 ALTER TABLE `cms_stats` DISABLE KEYS */;
/*!40000 ALTER TABLE `cms_stats` ENABLE KEYS */;

-- Dumping data for table aderai.cms_translate: 0 rows
/*!40000 ALTER TABLE `cms_translate` DISABLE KEYS */;
/*!40000 ALTER TABLE `cms_translate` ENABLE KEYS */;

-- Dumping data for table aderai.cms_translate_lang: 0 rows
/*!40000 ALTER TABLE `cms_translate_lang` DISABLE KEYS */;
/*!40000 ALTER TABLE `cms_translate_lang` ENABLE KEYS */;

-- Dumping data for table aderai.cms_users: ~2 rows (approximately)
/*!40000 ALTER TABLE `cms_users` DISABLE KEYS */;
INSERT INTO `cms_users` (`id`, `ip_address`, `username`, `password`, `salt`, `email`, `activation_code`, `forgotten_password_code`, `forgotten_password_time`, `remember_code`, `created_on`, `last_login`, `active`, `first_name`, `last_name`, `company`, `phone`) VALUES
	(1, '127.0.0.1', 'administrator', '$2a$07$SeBknntpZror9uyftVopmu61qg0ms8Qv1yV6FG.kQOSM.9QhmTo36', '', 'admin@admin.com', 'ad9953b0f8bfe7fef21a24c223932a1f2ade9879', NULL, NULL, NULL, 1268889823, 1465472707, 1, 'Eddy', 'Subratha', 'Admin', '321'),
	(2, '::1', NULL, '$2y$08$hN2OOeNjhQoLanv0QM9OUuxOAxTrIDzj3YEkLm5BiMJpKAyy2mumK', NULL, 'eddy.subratha@gmail.com', NULL, NULL, NULL, NULL, 1456843873, 1463144183, 1, 'Eddy', 'Nugoroh', 'Accounting Dept.', '085228720780');
/*!40000 ALTER TABLE `cms_users` ENABLE KEYS */;

-- Dumping data for table aderai.cms_users_groups: ~2 rows (approximately)
/*!40000 ALTER TABLE `cms_users_groups` DISABLE KEYS */;
INSERT INTO `cms_users_groups` (`id`, `user_id`, `group_id`) VALUES
	(31, 1, 0),
	(26, 2, 1);
/*!40000 ALTER TABLE `cms_users_groups` ENABLE KEYS */;
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
