-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: May 28, 2017 at 01:02 PM
-- Server version: 10.1.19-MariaDB
-- PHP Version: 5.6.28

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `aderai`
--

-- --------------------------------------------------------

--
-- Table structure for table `cms_app_config`
--

CREATE TABLE `cms_app_config` (
  `id` int(11) NOT NULL,
  `key` varchar(255) NOT NULL,
  `value` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `cms_app_config`
--

INSERT INTO `cms_app_config` (`id`, `key`, `value`) VALUES
(1, 'VIDCALL.PRICE', '1000');

-- --------------------------------------------------------

--
-- Table structure for table `cms_app_event`
--

CREATE TABLE `cms_app_event` (
  `id` int(11) NOT NULL,
  `date` datetime NOT NULL,
  `title` varchar(45) NOT NULL,
  `description` varchar(255) NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `cms_app_event`
--

INSERT INTO `cms_app_event` (`id`, `date`, `title`, `description`, `created`, `modified`) VALUES
(1, '2017-03-24 00:00:00', 'Event aderai', 'Event video call aderai', '2017-03-22 00:00:00', '2017-03-22 00:00:00'),
(2, '2017-03-25 00:00:00', 'Event aderai 2', 'Event video call aderai 2', '2017-03-22 00:00:00', '2017-03-22 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `cms_app_exercise`
--

CREATE TABLE `cms_app_exercise` (
  `id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `description` varchar(255) NOT NULL,
  `total_like` int(11) NOT NULL,
  `total_share` int(11) NOT NULL,
  `total_views` int(11) NOT NULL,
  `video` varchar(255) NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `cms_app_exercise`
--

INSERT INTO `cms_app_exercise` (`id`, `title`, `description`, `total_like`, `total_share`, `total_views`, `video`, `created`, `modified`) VALUES
(1, 'Exercise aderai1', 'Exercise aderai1', 100, 10, 1002, 'http://104.236.40.173/aderai1.mp4', '2017-03-22 00:00:00', '2017-03-22 00:00:00'),
(2, 'Excersise 2', 'Exercise aderai 2', 300, 20, 2000, 'http://104.236.40.173/aderai2.mp4', '2017-03-22 00:00:00', '2017-03-22 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `cms_app_fans`
--

CREATE TABLE `cms_app_fans` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `point` int(11) NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `cms_app_fans`
--

INSERT INTO `cms_app_fans` (`id`, `user_id`, `point`, `created`, `modified`) VALUES
(1, 4, 123, '2017-03-22 00:00:00', '2017-03-22 00:00:00'),
(7, 14, 5, '2017-03-21 00:00:00', '2017-03-21 00:00:00'),
(8, 15, 5, '2017-03-21 00:00:00', '2017-03-21 00:00:00'),
(9, 16, 5, '2017-03-21 00:00:00', '2017-03-21 00:00:00'),
(10, 17, 5, '2017-03-21 00:00:00', '2017-03-21 00:00:00'),
(11, 18, 5, '2017-03-21 00:00:00', '2017-03-21 00:00:00'),
(12, 19, 5, '2017-03-21 00:00:00', '2017-03-21 00:00:00'),
(13, 20, 5, '2017-03-21 00:00:00', '2017-03-21 00:00:00'),
(14, 21, 5, '2017-03-21 00:00:00', '2017-03-21 00:00:00'),
(15, 22, 5, '2017-03-21 00:00:00', '2017-03-21 00:00:00'),
(16, 23, 5, '2017-03-21 00:00:00', '2017-03-21 00:00:00'),
(17, 24, 5, '2017-03-21 00:00:00', '2017-03-21 00:00:00'),
(18, 25, 5, '2017-03-21 00:00:00', '2017-03-21 00:00:00'),
(19, 26, 5, '2017-04-30 14:40:05', '2017-04-30 14:40:05');

-- --------------------------------------------------------

--
-- Table structure for table `cms_app_history`
--

CREATE TABLE `cms_app_history` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `type` varchar(64) NOT NULL COMMENT 'VIDCALL, MERCHAN',
  `datetime` datetime NOT NULL,
  `calltime` int(11) NOT NULL,
  `price` int(11) NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `cms_app_history`
--

INSERT INTO `cms_app_history` (`id`, `user_id`, `title`, `type`, `datetime`, `calltime`, `price`, `created`, `modified`) VALUES
(1, 22, 'ADERAI', 'VIDCALL', '2017-04-04 08:05:52', 10, 0, '2017-04-04 08:05:52', '2017-04-04 08:05:52'),
(2, 22, 'ADERAI', 'VIDCALL', '2017-04-04 08:06:09', 70, 0, '2017-04-04 08:06:09', '2017-04-04 08:06:09'),
(4, 22, 'ADERAI', 'VIDCALL', '2017-04-12 20:38:46', 70, 2000, '2017-04-12 20:38:46', '2017-04-12 20:38:46'),
(5, 22, 'ADERAI', 'VIDCALL', '2017-04-13 21:28:54', 70, 2000, '2017-04-13 21:28:54', '2017-04-13 21:28:54'),
(6, 22, 'ADERAI', 'VIDCALL', '2017-04-13 21:47:31', 70, 2000, '2017-04-13 21:47:31', '2017-04-13 21:47:31');

-- --------------------------------------------------------

--
-- Table structure for table `cms_app_merchandise`
--

CREATE TABLE `cms_app_merchandise` (
  `id` int(11) NOT NULL,
  `id_merchandise_category` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `price` int(11) NOT NULL,
  `rate` int(11) NOT NULL,
  `description` text NOT NULL,
  `created` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `modified` datetime NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `cms_app_merchandise`
--

INSERT INTO `cms_app_merchandise` (`id`, `id_merchandise_category`, `name`, `price`, `rate`, `description`, `created`, `modified`) VALUES
(40, 1, 'kaos tanggan', 1000, 0, 'asdasda', '0000-00-00 00:00:00', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `cms_app_merchandise_category`
--

CREATE TABLE `cms_app_merchandise_category` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `created` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `modified` datetime NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `cms_app_merchandise_category`
--

INSERT INTO `cms_app_merchandise_category` (`id`, `name`, `created`, `modified`) VALUES
(1, 'SHIRT', '2017-03-21 00:00:00', '2017-03-21 00:00:00'),
(2, 'PANTS', '2017-03-21 00:00:00', '2017-03-21 00:00:00'),
(3, 'SHOES', '2017-03-21 00:00:00', '2017-03-21 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `cms_app_merchandise_colors`
--

CREATE TABLE `cms_app_merchandise_colors` (
  `id` int(11) NOT NULL,
  `id_merchandise` int(11) NOT NULL,
  `code` varchar(10) NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `cms_app_merchandise_colors`
--

INSERT INTO `cms_app_merchandise_colors` (`id`, `id_merchandise`, `code`, `created`, `modified`) VALUES
(48, 40, '#ff0000', '2017-05-27 23:37:49', '2017-05-27 23:37:49'),
(49, 40, '#ff0011', '2017-05-21 00:00:00', '2017-05-28 00:00:00'),
(50, 40, '#ffffff', '2017-05-21 00:00:00', '2017-05-28 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `cms_app_merchandise_comment`
--

CREATE TABLE `cms_app_merchandise_comment` (
  `id` int(11) NOT NULL,
  `id_users` int(11) NOT NULL,
  `id_merchandise` int(11) NOT NULL,
  `comment` text NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `cms_app_merchandise_images`
--

CREATE TABLE `cms_app_merchandise_images` (
  `id` int(11) NOT NULL,
  `id_merchandise` int(11) NOT NULL,
  `name` varchar(50) NOT NULL,
  `detail` text NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `cms_app_merchandise_images`
--

INSERT INTO `cms_app_merchandise_images` (`id`, `id_merchandise`, `name`, `detail`, `created`, `modified`) VALUES
(33, 40, '1495942668_logan.jpg', '', '2017-05-27 23:37:49', '2017-05-27 23:37:49'),
(34, 40, '1495942669_street.jpg', '', '2017-05-27 23:37:49', '2017-05-27 23:37:49');

-- --------------------------------------------------------

--
-- Table structure for table `cms_app_merchandise_size`
--

CREATE TABLE `cms_app_merchandise_size` (
  `id` int(11) NOT NULL,
  `id_merchandise` int(11) NOT NULL,
  `name` varchar(50) NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `cms_app_merchandise_size`
--

INSERT INTO `cms_app_merchandise_size` (`id`, `id_merchandise`, `name`, `created`, `modified`) VALUES
(46, 40, 'All', '2017-05-27 23:37:49', '2017-05-27 23:37:49');

-- --------------------------------------------------------

--
-- Table structure for table `cms_app_paket_topup`
--

CREATE TABLE `cms_app_paket_topup` (
  `id` int(11) NOT NULL,
  `nominal` int(11) NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `cms_app_paket_topup`
--

INSERT INTO `cms_app_paket_topup` (`id`, `nominal`, `created`, `modified`) VALUES
(1, 50000, '2017-04-30 00:00:00', '2017-04-30 00:00:00'),
(2, 100000, '2017-04-30 00:00:00', '2017-04-30 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `cms_app_payment`
--

CREATE TABLE `cms_app_payment` (
  `id` int(11) NOT NULL,
  `paket_id` int(11) NOT NULL,
  `amount` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `rekening_id` int(11) NOT NULL,
  `status` int(11) NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `cms_app_payment`
--

INSERT INTO `cms_app_payment` (`id`, `paket_id`, `amount`, `user_id`, `rekening_id`, `status`, `created`, `modified`) VALUES
(2, -1, 100525, 2, 1, 1, '2017-05-03 12:55:31', '2017-05-03 12:55:31');

-- --------------------------------------------------------

--
-- Table structure for table `cms_app_rekening`
--

CREATE TABLE `cms_app_rekening` (
  `id` int(11) NOT NULL,
  `account_number` varchar(255) NOT NULL,
  `account_name` varchar(255) NOT NULL,
  `bank_name` varchar(255) NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `cms_app_rekening`
--

INSERT INTO `cms_app_rekening` (`id`, `account_number`, `account_name`, `bank_name`, `created`, `modified`) VALUES
(1, '1234567', 'PT. Sukses', 'Bank Rakyat Indonesia (BRI)', '2017-04-30 00:00:00', '2017-04-30 00:00:00'),
(2, '54321', 'PT. Sukses', 'Bank Mandiri', '2017-04-30 00:00:00', '2017-04-30 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `cms_app_session`
--

CREATE TABLE `cms_app_session` (
  `id` int(11) NOT NULL,
  `token` varchar(255) NOT NULL,
  `user_id` int(11) NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `cms_app_session`
--

INSERT INTO `cms_app_session` (`id`, `token`, `user_id`, `created`, `modified`) VALUES
(2, '891f791a6973bef4b37845ad08d1c5891ac135d3', 1, '2017-04-30 14:30:18', '2017-04-30 14:30:18'),
(8, '0e96009d8df229a17ee11e3828d38e6a4ca38287', 26, '2017-04-30 14:40:05', '2017-04-30 14:40:05'),
(10, '9302dde30dbd99c8a85cb1d272ff248d252cbabb', 2, '2017-05-01 08:51:46', '2017-05-01 08:51:46'),
(11, 'b12d7d724b2b0e32a92ec5dc331435d60382514b', 22, '2017-05-01 09:23:54', '2017-05-01 09:23:54');

-- --------------------------------------------------------

--
-- Table structure for table `cms_app_trainer`
--

CREATE TABLE `cms_app_trainer` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `status` int(11) NOT NULL COMMENT '0=offiline, 1=online, 2=bussy',
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `cms_app_trainer`
--

INSERT INTO `cms_app_trainer` (`id`, `user_id`, `status`, `created`, `modified`) VALUES
(1, 2, 1, '2017-04-07 00:00:00', '2017-04-07 00:00:00'),
(2, 4, 0, '2017-04-07 00:00:00', '2017-04-07 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `cms_app_users`
--

CREATE TABLE `cms_app_users` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `password` varchar(60) NOT NULL,
  `remember_token` varchar(100) DEFAULT NULL,
  `image` varchar(255) NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `cms_app_users`
--

INSERT INTO `cms_app_users` (`id`, `name`, `email`, `password`, `remember_token`, `image`, `created`, `modified`) VALUES
(2, 'ADERAI', 'aderai@p', '40bd001563085fc35165329ea1ff5c5ecbdbbeef', NULL, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(4, 'Oji Setyawan', 'ojixzzz@gmail.com', '7c222fb2927d828af22f592134e8932480637c0d', NULL, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(14, 'Testing', 'tes@gmail.com', '40bd001563085fc35165329ea1ff5c5ecbdbbeef', NULL, '', '2017-03-21 00:00:00', '2017-03-21 00:00:00'),
(15, 'Tes user', 'ojixxxz@gmail.com', '7c222fb2927d828af22f592134e8932480637c0d', NULL, '', '2017-03-21 00:00:00', '2017-03-21 00:00:00'),
(16, 'Aldy Yoga Alfianta', 'alfianta25@gmail.com', '077454edf4bf8bd07430de53cc2dea185dde1e09', NULL, '', '2017-03-21 00:00:00', '2017-03-21 00:00:00'),
(17, 'ras', 'ras@mail.com', '40bd001563085fc35165329ea1ff5c5ecbdbbeef', NULL, '', '2017-03-21 00:00:00', '2017-03-21 00:00:00'),
(19, 'r', 'r@m.com', '40bd001563085fc35165329ea1ff5c5ecbdbbeef', NULL, '', '2017-03-21 00:00:00', '2017-03-21 00:00:00'),
(20, 'anggoro', 'anggorokasih10@gmail.com', '8cb2237d0679ca88db6464eac60da96345513964', NULL, '', '2017-03-21 00:00:00', '2017-03-21 00:00:00'),
(21, 'Yudha', 'yudhaputrama.dev@gmail.com', 'f865b53623b121fd34ee5426c792e5c33af8c227', NULL, '', '2017-03-21 00:00:00', '2017-03-21 00:00:00'),
(22, 'panji', 'panji@p', '40bd001563085fc35165329ea1ff5c5ecbdbbeef', NULL, '', '2017-03-21 00:00:00', '2017-03-21 00:00:00'),
(23, 'eddy', 'eddy@mail.com', '40bd001563085fc35165329ea1ff5c5ecbdbbeef', NULL, '', '2017-03-21 00:00:00', '2017-03-21 00:00:00'),
(24, 'ADERAI', 'aderai@ps', '40bd001563085fc35165329ea1ff5c5ecbdbbeef', NULL, '', '2017-03-21 00:00:00', '2017-03-21 00:00:00'),
(25, 'slackie', 'azmiee@gmail.com', '7c4a8d09ca3762af61e59520943dc26494f8941b', NULL, '', '2017-03-21 00:00:00', '2017-03-21 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `cms_app_wallet`
--

CREATE TABLE `cms_app_wallet` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `saldo` int(11) NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `cms_app_wallet`
--

INSERT INTO `cms_app_wallet` (`id`, `user_id`, `saldo`, `created`, `modified`) VALUES
(1, 2, 100000, '2017-05-03 00:00:00', '2017-05-03 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `cms_category`
--

CREATE TABLE `cms_category` (
  `id` int(10) NOT NULL,
  `type` varchar(100) DEFAULT 'post',
  `is_default` int(1) DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `cms_category`
--

INSERT INTO `cms_category` (`id`, `type`, `is_default`, `created_at`, `updated_at`) VALUES
(1, 'folder', 0, '2016-05-17 22:59:28', '2016-05-17 22:59:28'),
(2, 'folder', 0, '2016-05-17 23:56:50', '2016-05-17 23:56:50'),
(3, 'folder', 0, '2016-05-17 23:57:07', '2016-05-17 23:57:07'),
(4, 'folder', 0, '2016-05-17 23:57:27', '2016-05-17 23:57:27'),
(5, 'folder', 0, '2016-05-17 23:57:49', '2016-05-17 23:57:49'),
(6, 'post', 1, '2016-05-18 01:03:55', '2016-05-18 01:03:55'),
(7, 'post', NULL, '2016-05-18 01:04:52', '2016-05-18 01:04:52'),
(8, 'post', NULL, '2016-05-18 01:05:31', '2016-05-18 01:05:31'),
(9, 'post', NULL, '2016-05-17 21:31:51', '2016-05-17 21:31:51');

-- --------------------------------------------------------

--
-- Table structure for table `cms_category_lang`
--

CREATE TABLE `cms_category_lang` (
  `lang` varchar(45) NOT NULL,
  `id` int(10) NOT NULL,
  `name` varchar(100) DEFAULT NULL,
  `slug` varchar(45) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `cms_category_lang`
--

INSERT INTO `cms_category_lang` (`lang`, `id`, `name`, `slug`) VALUES
('en', 1, 'Blogs', 'blogs'),
('id', 1, 'Berita', 'berita'),
('en', 2, 'Slider', 'slider'),
('id', 2, 'Slider', 'slider'),
('en', 3, 'Gallery', 'gallery'),
('id', 3, 'Galeri', 'galeri'),
('en', 4, 'Theme', 'theme'),
('id', 4, 'Tema', 'tema'),
('en', 5, 'Download', 'download'),
('id', 5, 'Unduh', 'unduh'),
('en', 6, 'Event', 'event'),
('id', 6, 'Kegiatan', 'kegiatan'),
('en', 7, 'Seminar', 'seminar'),
('id', 7, 'Seminar', 'seminar'),
('en', 8, 'Study', 'study'),
('id', 8, 'Kajian', 'kajian'),
('en', 9, 'Announcements', 'announcements'),
('id', 9, 'Pengumuman', 'pengumuman');

-- --------------------------------------------------------

--
-- Table structure for table `cms_config`
--

CREATE TABLE `cms_config` (
  `id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `value` text COLLATE utf8mb4_unicode_ci,
  `label` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `groups` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `cms_config`
--

INSERT INTO `cms_config` (`id`, `name`, `value`, `label`, `groups`) VALUES
(1, 'site_name', 'Ade Rai Apps', 'Site Name', 'general'),
(2, 'site_url', 'ade.rai', 'Site URL', 'general'),
(3, 'site_tagline', 'For Your Healthy', 'Tagline', 'general'),
(4, 'email', 'info@ade.rai', 'Email', 'general'),
(5, 'phone', '+62 21 123456', 'Phone', 'general'),
(6, 'fax', '+62 21 123456', 'Fax.', 'general'),
(7, 'address_1', 'Jalan', 'Address (Line 1)', 'general'),
(8, 'address_2', 'Raya Lintas Jakarta', 'Address (Line 2)', 'general'),
(9, 'facebook', 'ade.rai', 'Facebook', 'general'),
(10, 'date_format', 'd/m/Y h:i:s A', 'Default Date Format', 'configuration'),
(11, 'rss', 'rss', 'RSS Address', 'configuration'),
(12, 'meta_description', 'Ade Rai Apps - For Your Healthy', 'Meta Description', 'configuration'),
(13, 'registration', 'Pendaftaran Anggota', 'Registration Title', 'information'),
(14, 'registration_info', 'Saya ingin mendaftar sebagai anggota.', 'Registration Description', 'information');

-- --------------------------------------------------------

--
-- Table structure for table `cms_event`
--

CREATE TABLE `cms_event` (
  `id` int(10) NOT NULL,
  `users_id` int(10) DEFAULT NULL,
  `status` enum('draft','publish') DEFAULT NULL,
  `start_date` date DEFAULT NULL,
  `due_date` date DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `cms_event_lang`
--

CREATE TABLE `cms_event_lang` (
  `lang` varchar(5) NOT NULL,
  `id` int(10) NOT NULL,
  `title` varchar(200) DEFAULT NULL,
  `content` text,
  `place` varchar(200) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `cms_groups`
--

CREATE TABLE `cms_groups` (
  `id` mediumint(8) UNSIGNED NOT NULL,
  `name` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `cms_groups`
--

INSERT INTO `cms_groups` (`id`, `name`, `description`) VALUES
(0, 'Administrator', 'Highest level of users privillegges. Do not remove.'),
(1, 'Members', 'Set as default groups for new user. ');

-- --------------------------------------------------------

--
-- Table structure for table `cms_lang`
--

CREATE TABLE `cms_lang` (
  `id` int(10) NOT NULL,
  `shortname` varchar(5) DEFAULT NULL,
  `name` varchar(100) DEFAULT NULL,
  `is_default` int(1) DEFAULT '0',
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `cms_lang`
--

INSERT INTO `cms_lang` (`id`, `shortname`, `name`, `is_default`, `updated_at`) VALUES
(1, 'id', 'Bahasa', 1, '2016-05-24 06:06:49'),
(2, 'eng', 'English', 0, '2016-05-24 06:06:49');

-- --------------------------------------------------------

--
-- Table structure for table `cms_logs`
--

CREATE TABLE `cms_logs` (
  `id` int(11) NOT NULL,
  `flag` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `module` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `users_id` int(11) NOT NULL DEFAULT '0',
  `message` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `cms_media`
--

CREATE TABLE `cms_media` (
  `id` int(10) NOT NULL,
  `category_id` int(10) NOT NULL,
  `type` varchar(20) DEFAULT 'jpg',
  `size` decimal(10,0) DEFAULT '0',
  `alias` varchar(50) DEFAULT NULL,
  `filename` varchar(255) DEFAULT NULL,
  `download` int(10) DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `cms_media_lang`
--

CREATE TABLE `cms_media_lang` (
  `lang` varchar(5) NOT NULL,
  `id` int(10) NOT NULL,
  `alt` varchar(255) DEFAULT NULL,
  `desc` text
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `cms_member`
--

CREATE TABLE `cms_member` (
  `member_id` int(11) NOT NULL,
  `realname` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `password` text COLLATE utf8_unicode_ci NOT NULL,
  `status` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `birth_city` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `birth_date` date DEFAULT NULL,
  `gender` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `home_addr` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `home_city` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `home_prov` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `phone` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `avatar` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ahli_bid` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `website` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `socmed_fb` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `socmed_tw` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `last_update` datetime DEFAULT NULL,
  `reg_date` datetime DEFAULT NULL,
  `last_login` datetime DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `cms_member_tokens`
--

CREATE TABLE `cms_member_tokens` (
  `token_id` int(11) NOT NULL,
  `token` varchar(255) CHARACTER SET utf8 NOT NULL,
  `member_id` int(11) NOT NULL,
  `created` date DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `cms_message`
--

CREATE TABLE `cms_message` (
  `id` bigint(20) NOT NULL,
  `from` varchar(100) DEFAULT NULL,
  `to` varchar(100) DEFAULT NULL,
  `subject` text,
  `message` text,
  `status` int(1) DEFAULT '0' COMMENT '0 - New 1 - Read 9 Trash',
  `reply` bigint(20) DEFAULT '0',
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `cms_modules`
--

CREATE TABLE `cms_modules` (
  `id` int(11) NOT NULL,
  `name` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parent` int(2) NOT NULL DEFAULT '0',
  `show` int(1) NOT NULL DEFAULT '1',
  `path` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '#',
  `icon` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `has_write` int(1) DEFAULT NULL COMMENT 'Has add method ? 1=Yes 0=No',
  `has_update` int(1) DEFAULT NULL COMMENT 'Has update method ? 1=Yes 0=No',
  `has_delete` int(1) DEFAULT NULL COMMENT 'Has delete method ? 1=Yes 0=No',
  `orders` int(1) NOT NULL DEFAULT '1'
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=DYNAMIC;

--
-- Dumping data for table `cms_modules`
--

INSERT INTO `cms_modules` (`id`, `name`, `parent`, `show`, `path`, `icon`, `has_write`, `has_update`, `has_delete`, `orders`) VALUES
(1, 'Dashboard', 0, 1, 'dashboard', 'ion-android-calendar', NULL, NULL, NULL, 0),
(2, 'Systems', 0, 1, '#', 'ion-android-settings', NULL, NULL, NULL, 23),
(3, 'Modules', 2, 1, 'packages', 'fa-folder-open', 1, 1, 1, 26),
(4, 'Priviledges', 8, 1, 'priviledges', 'fa-lock', 1, 1, 1, 22),
(5, 'General', 2, 1, 'general', 'fa-desktop', 1, 1, 1, 24),
(6, 'Groups', 8, 1, 'groups', 'fa-sitemap', 1, 1, 1, 21),
(7, 'Logs System', 2, 1, 'logs', 'fa-rocket', 0, 0, 0, 28),
(8, 'Access Manager', 0, 1, '#', 'ion-android-person', NULL, NULL, NULL, 19),
(9, 'Users', 8, 1, 'auth', '', 1, 1, 1, 20),
(10, 'Contents', 0, 1, '#', 'ion-android-list', NULL, NULL, NULL, 7),
(11, 'Blogs', 10, 0, 'blogs', '', 1, 1, 1, 8),
(36, 'Merchandise', 10, 1, 'merchandise', '', 1, 1, 1, 27),
(14, 'Media', 0, 0, '#', 'ion-android-image', NULL, NULL, NULL, 13),
(15, 'Folders', 14, 1, 'folders', '', 1, 1, 1, 14),
(16, 'Files', 14, 1, 'files', '', 1, 1, 1, 15),
(18, 'Events', 10, 1, 'events', '', 1, 1, 1, 11),
(19, 'Transactions', 0, 0, '#', 'ion-card', NULL, NULL, NULL, 3),
(20, 'Navigations', 0, 0, '#', 'ion-share', NULL, NULL, NULL, 16),
(21, 'Accounts', 0, 1, '#', 'ion-android-contact', 1, 1, 1, 1),
(25, 'Language', 2, 1, 'langs', 'ion ion-ios-barcode-outline', 1, 1, 1, 25),
(26, 'Groups', 20, 1, 'navgroups', '', 1, 1, 1, 17),
(27, 'Links', 20, 1, 'nav', '', 1, 1, 1, 18),
(28, 'Translate', 2, 1, 'translate', '', 1, 1, 1, 27),
(30, 'Topup payment', 19, 1, 'balance', '', 1, 1, 1, 4),
(31, 'Report', 19, 1, 'report', '', 1, 1, 1, 5),
(32, 'Video', 10, 0, 'video', '', 1, 1, 1, 12),
(33, 'Membership', 21, 0, 'members', '', 1, 1, 1, 3),
(34, 'Users', 21, 1, 'user', '', 1, 1, 1, 20);

-- --------------------------------------------------------

--
-- Table structure for table `cms_nav`
--

CREATE TABLE `cms_nav` (
  `id` int(11) NOT NULL,
  `nav_id` int(11) NOT NULL,
  `parent_id` int(11) NOT NULL,
  `slug` varchar(50) DEFAULT NULL,
  `type` varchar(45) DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `cms_nav`
--

INSERT INTO `cms_nav` (`id`, `nav_id`, `parent_id`, `slug`, `type`, `updated_at`) VALUES
(1, 1, 0, '/', 'module', '2016-05-24 06:08:42');

-- --------------------------------------------------------

--
-- Table structure for table `cms_nav_groups`
--

CREATE TABLE `cms_nav_groups` (
  `id` int(5) NOT NULL,
  `name` varchar(50) NOT NULL DEFAULT '0',
  `slug` varchar(50) NOT NULL DEFAULT '0',
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `cms_nav_groups`
--

INSERT INTO `cms_nav_groups` (`id`, `name`, `slug`, `updated_at`) VALUES
(1, 'Main Navigation', 'main', '2016-05-24 01:37:56');

-- --------------------------------------------------------

--
-- Table structure for table `cms_nav_lang`
--

CREATE TABLE `cms_nav_lang` (
  `lang` varchar(5) NOT NULL,
  `id` int(10) NOT NULL,
  `name` varchar(100) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `cms_nav_lang`
--

INSERT INTO `cms_nav_lang` (`lang`, `id`, `name`) VALUES
('eng', 1, 'Home'),
('id', 1, 'Beranda');

-- --------------------------------------------------------

--
-- Table structure for table `cms_page`
--

CREATE TABLE `cms_page` (
  `id` int(10) NOT NULL,
  `category_id` int(10) NOT NULL,
  `users_id` int(10) NOT NULL,
  `type` varchar(50) DEFAULT 'post',
  `status` enum('draft','publish') DEFAULT 'draft',
  `media_id` int(10) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `cms_page_lang`
--

CREATE TABLE `cms_page_lang` (
  `lang` varchar(5) NOT NULL,
  `id` int(10) NOT NULL,
  `title` varchar(200) DEFAULT NULL,
  `intro` text,
  `body` text,
  `slug` varchar(200) DEFAULT NULL,
  `view` int(11) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `cms_priviledges`
--

CREATE TABLE `cms_priviledges` (
  `modules_id` int(2) NOT NULL,
  `groups_id` int(2) NOT NULL,
  `can_write` int(1) NOT NULL DEFAULT '0' COMMENT '0 = No - 1 = True',
  `can_update` int(1) NOT NULL DEFAULT '0' COMMENT '0 = No - 1 = True',
  `can_delete` int(1) NOT NULL DEFAULT '0' COMMENT '0 = No - 1 = True',
  `created` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `created_by` int(11) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `cms_priviledges`
--

INSERT INTO `cms_priviledges` (`modules_id`, `groups_id`, `can_write`, `can_update`, `can_delete`, `created`, `created_by`) VALUES
(34, 0, 1, 1, 1, '2017-04-12 17:06:12', 1),
(33, 0, 1, 1, 1, '2017-04-12 17:06:12', 1),
(21, 0, 1, 1, 0, '2017-04-12 17:06:12', 1),
(27, 0, 1, 1, 1, '2017-04-12 17:06:12', 1),
(26, 0, 1, 1, 1, '2017-04-12 17:06:12', 1),
(20, 0, 0, 0, 0, '2017-04-12 17:06:12', 1),
(31, 0, 1, 1, 1, '2017-04-12 17:06:12', 1),
(30, 0, 1, 1, 1, '2017-04-12 17:06:12', 1),
(19, 0, 0, 0, 0, '2017-04-12 17:06:12', 1),
(16, 0, 1, 1, 1, '2017-04-12 17:06:12', 1),
(15, 0, 1, 1, 1, '2017-04-12 17:06:12', 1),
(14, 0, 0, 0, 0, '2017-04-12 17:06:12', 1),
(32, 0, 1, 1, 1, '2017-04-12 17:06:12', 1),
(18, 0, 1, 1, 1, '2017-04-12 17:06:12', 1),
(36, 0, 1, 1, 1, '2017-04-12 17:06:12', 1),
(11, 0, 1, 1, 1, '2017-04-12 17:06:12', 1),
(10, 0, 0, 0, 0, '2017-04-12 17:06:12', 1),
(9, 0, 1, 1, 1, '2017-04-12 17:06:12', 1),
(6, 0, 1, 1, 1, '2017-04-12 17:06:12', 1),
(4, 0, 1, 1, 1, '2017-04-12 17:06:12', 1),
(8, 0, 0, 0, 0, '2017-04-12 17:06:12', 1),
(28, 0, 1, 1, 1, '2017-04-12 17:06:12', 1),
(25, 0, 1, 1, 1, '2017-04-12 17:06:12', 1),
(7, 0, 0, 0, 0, '2017-04-12 17:06:12', 1),
(5, 0, 1, 1, 1, '2017-04-12 17:06:12', 1),
(3, 0, 1, 1, 1, '2017-04-12 17:06:12', 1),
(2, 0, 0, 0, 0, '2017-04-12 17:06:12', 1),
(1, 0, 0, 0, 0, '2017-04-12 17:06:12', 1);

-- --------------------------------------------------------

--
-- Table structure for table `cms_sessions`
--

CREATE TABLE `cms_sessions` (
  `id` varchar(40) NOT NULL,
  `ip_address` varchar(45) NOT NULL,
  `timestamp` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `data` blob NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `cms_sessions`
--

INSERT INTO `cms_sessions` (`id`, `ip_address`, `timestamp`, `data`) VALUES
('0b9e618e4e41a8fc1c5afd8e368eee55602b55f1', '::1', 1495945955, 0x5f5f63695f6c6173745f726567656e65726174657c693a313439353934353636323b6964656e746974797c733a31353a2261646d696e4061646d696e2e636f6d223b656d61696c7c733a31353a2261646d696e4061646d696e2e636f6d223b757365725f69647c733a313a2231223b67726f75705f69647c733a313a2230223b6f6c645f6c6173745f6c6f67696e7c733a31303a2231343935383835383630223b6c616e677c733a323a226964223b),
('0e62058dd02b979984f3b8bd112421e70c5097f8', '::1', 1495432405, 0x5f5f63695f6c6173745f726567656e65726174657c693a313439353433323331303b6964656e746974797c733a31353a2261646d696e4061646d696e2e636f6d223b656d61696c7c733a31353a2261646d696e4061646d696e2e636f6d223b757365725f69647c733a313a2231223b67726f75705f69647c733a313a2230223b6f6c645f6c6173745f6c6f67696e7c733a31303a2231343935343036313139223b6c616e677c733a323a226964223b),
('159d6dfcdbce876b25d0fa9e06db5d4f104e27b1', '::1', 1495151670, 0x5f5f63695f6c6173745f726567656e65726174657c693a313439353135313633333b6964656e746974797c733a31353a2261646d696e4061646d696e2e636f6d223b656d61696c7c733a31353a2261646d696e4061646d696e2e636f6d223b757365725f69647c733a313a2231223b67726f75705f69647c733a313a2230223b6f6c645f6c6173745f6c6f67696e7c733a31303a2231343935303934303837223b6c616e677c733a323a226964223b),
('1ac4e622019bba6337ea0f4fe57ee66317e92bd4', '::1', 1495890971, 0x5f5f63695f6c6173745f726567656e65726174657c693a313439353839303730353b6964656e746974797c733a31353a2261646d696e4061646d696e2e636f6d223b656d61696c7c733a31353a2261646d696e4061646d696e2e636f6d223b757365725f69647c733a313a2231223b67726f75705f69647c733a313a2230223b6f6c645f6c6173745f6c6f67696e7c733a31303a2231343935373634393833223b6c616e677c733a323a226964223b),
('1acdad0c517bfe9a55cd09dfa3c024165e1b9d7c', '::1', 1495677771, 0x5f5f63695f6c6173745f726567656e65726174657c693a313439353637373437363b6964656e746974797c733a31353a2261646d696e4061646d696e2e636f6d223b656d61696c7c733a31353a2261646d696e4061646d696e2e636f6d223b757365725f69647c733a313a2231223b67726f75705f69647c733a313a2230223b6f6c645f6c6173745f6c6f67696e7c733a31303a2231343935363435383139223b6c616e677c733a323a226964223b),
('1e2b4917bb66e8c9d1b2b8b80952011778fe21b8', '::1', 1495691225, 0x5f5f63695f6c6173745f726567656e65726174657c693a313439353639303936363b6964656e746974797c733a31353a2261646d696e4061646d696e2e636f6d223b656d61696c7c733a31353a2261646d696e4061646d696e2e636f6d223b757365725f69647c733a313a2231223b67726f75705f69647c733a313a2230223b6f6c645f6c6173745f6c6f67696e7c733a31303a2231343935363638313731223b6c616e677c733a323a226964223b),
('22aaf2a32cf1c5aad16b868ba52a494c2e69ca9c', '::1', 1495410240, 0x5f5f63695f6c6173745f726567656e65726174657c693a313439353430393936313b6964656e746974797c733a31353a2261646d696e4061646d696e2e636f6d223b656d61696c7c733a31353a2261646d696e4061646d696e2e636f6d223b757365725f69647c733a313a2231223b67726f75705f69647c733a313a2230223b6f6c645f6c6173745f6c6f67696e7c733a31303a2231343935323831303539223b6c616e677c733a323a226964223b),
('2454a32e066fdb3b28e870ea7ea1c2c1209839a6', '::1', 1495408478, 0x5f5f63695f6c6173745f726567656e65726174657c693a313439353430383334333b6964656e746974797c733a31353a2261646d696e4061646d696e2e636f6d223b656d61696c7c733a31353a2261646d696e4061646d696e2e636f6d223b757365725f69647c733a313a2231223b67726f75705f69647c733a313a2230223b6f6c645f6c6173745f6c6f67696e7c733a31303a2231343935323831303539223b6c616e677c733a323a226964223b),
('259ef67d0ed57009dc577c64f4e60816b20f0e19', '::1', 1495895290, 0x5f5f63695f6c6173745f726567656e65726174657c693a313439353839353030323b6964656e746974797c733a31353a2261646d696e4061646d696e2e636f6d223b656d61696c7c733a31353a2261646d696e4061646d696e2e636f6d223b757365725f69647c733a313a2231223b67726f75705f69647c733a313a2230223b6f6c645f6c6173745f6c6f67696e7c733a31303a2231343935373634393833223b6c616e677c733a323a226964223b),
('25abe1993e7a1225c1b29c3e8982057690ef2f9a', '::1', 1495939111, 0x5f5f63695f6c6173745f726567656e65726174657c693a313439353933383832383b6964656e746974797c733a31353a2261646d696e4061646d696e2e636f6d223b656d61696c7c733a31353a2261646d696e4061646d696e2e636f6d223b757365725f69647c733a313a2231223b67726f75705f69647c733a313a2230223b6f6c645f6c6173745f6c6f67696e7c733a31303a2231343935383835383630223b6c616e677c733a323a226964223b),
('27c6d0e1c8cfb25d129e60b3bcc0e462cf725bde', '::1', 1495690596, 0x5f5f63695f6c6173745f726567656e65726174657c693a313439353639303332323b6964656e746974797c733a31353a2261646d696e4061646d696e2e636f6d223b656d61696c7c733a31353a2261646d696e4061646d696e2e636f6d223b757365725f69647c733a313a2231223b67726f75705f69647c733a313a2230223b6f6c645f6c6173745f6c6f67696e7c733a31303a2231343935363638313731223b6c616e677c733a323a226964223b),
('28ac60eef333926a459df9e1000c52f9c7e41d57', '::1', 1495676553, 0x5f5f63695f6c6173745f726567656e65726174657c693a313439353637363535333b6964656e746974797c733a31353a2261646d696e4061646d696e2e636f6d223b656d61696c7c733a31353a2261646d696e4061646d696e2e636f6d223b757365725f69647c733a313a2231223b67726f75705f69647c733a313a2230223b6f6c645f6c6173745f6c6f67696e7c733a31303a2231343935363435383139223b6c616e677c733a323a226964223b),
('29ae2da6d617898b77e65398926f4d44da965c1d', '::1', 1495094068, 0x5f5f63695f6c6173745f726567656e65726174657c693a313439353039343036363b),
('2ea851a760ff1d14eb46ef8927cf4f92bb09bea4', '::1', 1495430763, 0x5f5f63695f6c6173745f726567656e65726174657c693a313439353433303637333b6964656e746974797c733a31353a2261646d696e4061646d696e2e636f6d223b656d61696c7c733a31353a2261646d696e4061646d696e2e636f6d223b757365725f69647c733a313a2231223b67726f75705f69647c733a313a2230223b6f6c645f6c6173745f6c6f67696e7c733a31303a2231343935343036313139223b6c616e677c733a323a226964223b),
('30c119a64ba52b410d35713a42b8313dd8465968', '::1', 1495708697, 0x5f5f63695f6c6173745f726567656e65726174657c693a313439353730383431353b6964656e746974797c733a31353a2261646d696e4061646d696e2e636f6d223b656d61696c7c733a31353a2261646d696e4061646d696e2e636f6d223b757365725f69647c733a313a2231223b67726f75705f69647c733a313a2230223b6f6c645f6c6173745f6c6f67696e7c733a31303a2231343935363930333636223b6c616e677c733a323a226964223b7c4e3b),
('31205ec830ef91b6006239ddc028e4a671f293f2', '::1', 1495706860, 0x5f5f63695f6c6173745f726567656e65726174657c693a313439353730363830383b6964656e746974797c733a31353a2261646d696e4061646d696e2e636f6d223b656d61696c7c733a31353a2261646d696e4061646d696e2e636f6d223b757365725f69647c733a313a2231223b67726f75705f69647c733a313a2230223b6f6c645f6c6173745f6c6f67696e7c733a31303a2231343935363930333636223b6c616e677c733a323a226964223b7c4e3b),
('326c83efcadad846adad493ddc0d85693f2b0adc', '::1', 1495937992, 0x5f5f63695f6c6173745f726567656e65726174657c693a313439353933373734353b6964656e746974797c733a31353a2261646d696e4061646d696e2e636f6d223b656d61696c7c733a31353a2261646d696e4061646d696e2e636f6d223b757365725f69647c733a313a2231223b67726f75705f69647c733a313a2230223b6f6c645f6c6173745f6c6f67696e7c733a31303a2231343935383835383630223b6c616e677c733a323a226964223b),
('32afba4852d4c120c9d08736ad5046af4562dfb8', '::1', 1495893161, 0x5f5f63695f6c6173745f726567656e65726174657c693a313439353839333136313b6964656e746974797c733a31353a2261646d696e4061646d696e2e636f6d223b656d61696c7c733a31353a2261646d696e4061646d696e2e636f6d223b757365725f69647c733a313a2231223b67726f75705f69647c733a313a2230223b6f6c645f6c6173745f6c6f67696e7c733a31303a2231343935373634393833223b6c616e677c733a323a226964223b7374617475734d73677c733a32383a2246696c65732075706c6f61646564207375636365737366756c6c792e223b5f5f63695f766172737c613a313a7b733a393a227374617475734d7367223b733a333a226f6c64223b7d),
('33cc8c8005aea0499186234c7bdc59edfa6f11b3', '::1', 1495895515, 0x5f5f63695f6c6173745f726567656e65726174657c693a313439353839353332383b6964656e746974797c733a31353a2261646d696e4061646d696e2e636f6d223b656d61696c7c733a31353a2261646d696e4061646d696e2e636f6d223b757365725f69647c733a313a2231223b67726f75705f69647c733a313a2230223b6f6c645f6c6173745f6c6f67696e7c733a31303a2231343935373634393833223b6c616e677c733a323a226964223b),
('34217dd6ef5b1bc250043a4c40d7176dc46d0728', '::1', 1494829690, 0x5f5f63695f6c6173745f726567656e65726174657c693a313439343832393632393b6964656e746974797c733a31353a2261646d696e4061646d696e2e636f6d223b656d61696c7c733a31353a2261646d696e4061646d696e2e636f6d223b757365725f69647c733a313a2231223b67726f75705f69647c733a313a2230223b6f6c645f6c6173745f6c6f67696e7c733a31303a2231343934363432343237223b6c616e677c733a323a226964223b),
('3bf1ccb7d42acb0b20d7833eb2051114e59693f7', '::1', 1495709104, 0x5f5f63695f6c6173745f726567656e65726174657c693a313439353730393130343b6964656e746974797c733a31353a2261646d696e4061646d696e2e636f6d223b656d61696c7c733a31353a2261646d696e4061646d696e2e636f6d223b757365725f69647c733a313a2231223b67726f75705f69647c733a313a2230223b6f6c645f6c6173745f6c6f67696e7c733a31303a2231343935363930333636223b6c616e677c733a323a226964223b7c4e3b),
('3cebe79c5bbe711367b13d5445aafe9db17ebe3f', '::1', 1495668185, 0x5f5f63695f6c6173745f726567656e65726174657c693a313439353636383135383b6964656e746974797c733a31353a2261646d696e4061646d696e2e636f6d223b656d61696c7c733a31353a2261646d696e4061646d696e2e636f6d223b757365725f69647c733a313a2231223b67726f75705f69647c733a313a2230223b6f6c645f6c6173745f6c6f67696e7c733a31303a2231343935363435383139223b6c616e677c733a323a226964223b),
('3f2f28a87100ce0bde0808d144d22a8feef44a2f', '::1', 1495431808, 0x5f5f63695f6c6173745f726567656e65726174657c693a313439353433313632343b6964656e746974797c733a31353a2261646d696e4061646d696e2e636f6d223b656d61696c7c733a31353a2261646d696e4061646d696e2e636f6d223b757365725f69647c733a313a2231223b67726f75705f69647c733a313a2230223b6f6c645f6c6173745f6c6f67696e7c733a31303a2231343935343036313139223b6c616e677c733a323a226964223b),
('40318f62acb93adcf5f62a92b6bc1251a58c0336', '::1', 1495454261, 0x5f5f63695f6c6173745f726567656e65726174657c693a313439353435343139363b6964656e746974797c733a31353a2261646d696e4061646d696e2e636f6d223b656d61696c7c733a31353a2261646d696e4061646d696e2e636f6d223b757365725f69647c733a313a2231223b67726f75705f69647c733a313a2230223b6f6c645f6c6173745f6c6f67696e7c733a31303a2231343935343236323234223b6c616e677c733a323a226964223b),
('438c136cfd0861244b5454bafda71f871103c6ad', '::1', 1495891188, 0x5f5f63695f6c6173745f726567656e65726174657c693a313439353839313031323b6964656e746974797c733a31353a2261646d696e4061646d696e2e636f6d223b656d61696c7c733a31353a2261646d696e4061646d696e2e636f6d223b757365725f69647c733a313a2231223b67726f75705f69647c733a313a2230223b6f6c645f6c6173745f6c6f67696e7c733a31303a2231343935373634393833223b6c616e677c733a323a226964223b),
('44c5c859ee98113d899b860afafbae3cb8b4c5b3', '::1', 1495939655, 0x5f5f63695f6c6173745f726567656e65726174657c693a313439353933393634333b6964656e746974797c733a31353a2261646d696e4061646d696e2e636f6d223b656d61696c7c733a31353a2261646d696e4061646d696e2e636f6d223b757365725f69647c733a313a2231223b67726f75705f69647c733a313a2230223b6f6c645f6c6173745f6c6f67696e7c733a31303a2231343935383835383630223b6c616e677c733a323a226964223b),
('481324baace8dbeb5047a448d8635a2aa3e813b1', '::1', 1495941145, 0x5f5f63695f6c6173745f726567656e65726174657c693a313439353934313033383b6964656e746974797c733a31353a2261646d696e4061646d696e2e636f6d223b656d61696c7c733a31353a2261646d696e4061646d696e2e636f6d223b757365725f69647c733a313a2231223b67726f75705f69647c733a313a2230223b6f6c645f6c6173745f6c6f67696e7c733a31303a2231343935383835383630223b6c616e677c733a323a226964223b),
('482bc19f8278d9da577de47730c4d1d30e09fe3a', '::1', 1495409827, 0x5f5f63695f6c6173745f726567656e65726174657c693a313439353430393630303b6964656e746974797c733a31353a2261646d696e4061646d696e2e636f6d223b656d61696c7c733a31353a2261646d696e4061646d696e2e636f6d223b757365725f69647c733a313a2231223b67726f75705f69647c733a313a2230223b6f6c645f6c6173745f6c6f67696e7c733a31303a2231343935323831303539223b6c616e677c733a323a226964223b),
('48b7b9476096ef2f999b9e372973cf5ecc56265b', '::1', 1495407381, 0x5f5f63695f6c6173745f726567656e65726174657c693a313439353430373132333b6964656e746974797c733a31353a2261646d696e4061646d696e2e636f6d223b656d61696c7c733a31353a2261646d696e4061646d696e2e636f6d223b757365725f69647c733a313a2231223b67726f75705f69647c733a313a2230223b6f6c645f6c6173745f6c6f67696e7c733a31303a2231343935323831303539223b6c616e677c733a323a226964223b),
('4b3cfdd802494aaf563e2ae621cb3a13b0d37fd4', '::1', 1495702525, 0x5f5f63695f6c6173745f726567656e65726174657c693a313439353730323331313b6964656e746974797c733a31353a2261646d696e4061646d696e2e636f6d223b656d61696c7c733a31353a2261646d696e4061646d696e2e636f6d223b757365725f69647c733a313a2231223b67726f75705f69647c733a313a2230223b6f6c645f6c6173745f6c6f67696e7c733a31303a2231343935363930333636223b6c616e677c733a323a226964223b),
('4fef051484f756715e6ac0f6cf99f303f0b1596b', '::1', 1495704602, 0x5f5f63695f6c6173745f726567656e65726174657c693a313439353730343434333b6964656e746974797c733a31353a2261646d696e4061646d696e2e636f6d223b656d61696c7c733a31353a2261646d696e4061646d696e2e636f6d223b757365725f69647c733a313a2231223b67726f75705f69647c733a313a2230223b6f6c645f6c6173745f6c6f67696e7c733a31303a2231343935363930333636223b6c616e677c733a323a226964223b7c4e3b6e616d655f6d65726368616e646973657c733a31303a226b616f7320616a612031223b5f5f63695f766172737c613a313a7b733a31363a226e616d655f6d65726368616e64697365223b733a333a226f6c64223b7d),
('50c123dea5cd23f8c7bf8a8476136ddfce43257f', '::1', 1495943719, 0x5f5f63695f6c6173745f726567656e65726174657c693a313439353934333630313b6964656e746974797c733a31353a2261646d696e4061646d696e2e636f6d223b656d61696c7c733a31353a2261646d696e4061646d696e2e636f6d223b757365725f69647c733a313a2231223b67726f75705f69647c733a313a2230223b6f6c645f6c6173745f6c6f67696e7c733a31303a2231343935383835383630223b6c616e677c733a323a226964223b),
('54fb1f4136eab87a616333094a1545d2c015ffdc', '::1', 1495704121, 0x5f5f63695f6c6173745f726567656e65726174657c693a313439353730333936313b6964656e746974797c733a31353a2261646d696e4061646d696e2e636f6d223b656d61696c7c733a31353a2261646d696e4061646d696e2e636f6d223b757365725f69647c733a313a2231223b67726f75705f69647c733a313a2230223b6f6c645f6c6173745f6c6f67696e7c733a31303a2231343935363930333636223b6c616e677c733a323a226964223b7c4e3b),
('5611bcebe831d8ab83c9ff53108b203806b05c27', '::1', 1495885877, 0x5f5f63695f6c6173745f726567656e65726174657c693a313439353838353539343b6964656e746974797c733a31353a2261646d696e4061646d696e2e636f6d223b656d61696c7c733a31353a2261646d696e4061646d696e2e636f6d223b757365725f69647c733a313a2231223b67726f75705f69647c733a313a2230223b6f6c645f6c6173745f6c6f67696e7c733a31303a2231343935373634393833223b6c616e677c733a323a226964223b),
('5631dabfea47f14b4f329588c4b794215ae5788d', '::1', 1495454664, 0x5f5f63695f6c6173745f726567656e65726174657c693a313439353435343636343b6964656e746974797c733a31353a2261646d696e4061646d696e2e636f6d223b656d61696c7c733a31353a2261646d696e4061646d696e2e636f6d223b757365725f69647c733a313a2231223b67726f75705f69647c733a313a2230223b6f6c645f6c6173745f6c6f67696e7c733a31303a2231343935343236323234223b6c616e677c733a323a226964223b),
('5679b16217ecbe158515d07146dc4caf5d89078d', '::1', 1495706495, 0x5f5f63695f6c6173745f726567656e65726174657c693a313439353730363439333b6964656e746974797c733a31353a2261646d696e4061646d696e2e636f6d223b656d61696c7c733a31353a2261646d696e4061646d696e2e636f6d223b757365725f69647c733a313a2231223b67726f75705f69647c733a313a2230223b6f6c645f6c6173745f6c6f67696e7c733a31303a2231343935363930333636223b6c616e677c733a323a226964223b7c4e3b),
('5754b7f125f9c4ecf600280006b44aa4b7afea97', '::1', 1495283114, 0x5f5f63695f6c6173745f726567656e65726174657c693a313439353238333039393b6964656e746974797c733a31353a2261646d696e4061646d696e2e636f6d223b656d61696c7c733a31353a2261646d696e4061646d696e2e636f6d223b757365725f69647c733a313a2231223b67726f75705f69647c733a313a2230223b6f6c645f6c6173745f6c6f67696e7c733a31303a2231343935313634343233223b6c616e677c733a323a226964223b637372666b65797c733a383a223553466668564237223b5f5f63695f766172737c613a323a7b733a373a22637372666b6579223b733a333a226f6c64223b733a393a226373726676616c7565223b733a333a226f6c64223b7d6373726676616c75657c733a32303a2236626d734d6731466b6158596c47377a50485378223b),
('5786a068a321fa8135f82dbec5946ba43dc00c19', '::1', 1495426238, 0x5f5f63695f6c6173745f726567656e65726174657c693a313439353432363230343b6964656e746974797c733a31353a2261646d696e4061646d696e2e636f6d223b656d61696c7c733a31353a2261646d696e4061646d696e2e636f6d223b757365725f69647c733a313a2231223b67726f75705f69647c733a313a2230223b6f6c645f6c6173745f6c6f67696e7c733a31303a2231343935343036313139223b6c616e677c733a323a226964223b),
('59669ff38c3414c80d4f7cf1f2022fd7d43050b3', '::1', 1495281086, 0x5f5f63695f6c6173745f726567656e65726174657c693a313439353238313034303b6964656e746974797c733a31353a2261646d696e4061646d696e2e636f6d223b656d61696c7c733a31353a2261646d696e4061646d696e2e636f6d223b757365725f69647c733a313a2231223b67726f75705f69647c733a313a2230223b6f6c645f6c6173745f6c6f67696e7c733a31303a2231343935313634343233223b6c616e677c733a323a226964223b),
('5d32d3b20d3ac388983c68b257ed0f5c8a1b6016', '::1', 1495094102, 0x5f5f63695f6c6173745f726567656e65726174657c693a313439353039343036363b6964656e746974797c733a31353a2261646d696e4061646d696e2e636f6d223b656d61696c7c733a31353a2261646d696e4061646d696e2e636f6d223b757365725f69647c733a313a2231223b67726f75705f69647c733a313a2230223b6f6c645f6c6173745f6c6f67696e7c733a31303a2231343934383434343733223b6c616e677c733a323a226964223b),
('5edb4829059220331f33957fca6f82950143a278', '::1', 1495435602, 0x5f5f63695f6c6173745f726567656e65726174657c693a313439353433353630313b6964656e746974797c733a31353a2261646d696e4061646d696e2e636f6d223b656d61696c7c733a31353a2261646d696e4061646d696e2e636f6d223b757365725f69647c733a313a2231223b67726f75705f69647c733a313a2230223b6f6c645f6c6173745f6c6f67696e7c733a31303a2231343935343036313139223b6c616e677c733a323a226964223b),
('618310d3bd0d67d4329a4d74d85e0aff6ffbe9ec', '::1', 1495409507, 0x5f5f63695f6c6173745f726567656e65726174657c693a313439353430393238313b6964656e746974797c733a31353a2261646d696e4061646d696e2e636f6d223b656d61696c7c733a31353a2261646d696e4061646d696e2e636f6d223b757365725f69647c733a313a2231223b67726f75705f69647c733a313a2230223b6f6c645f6c6173745f6c6f67696e7c733a31303a2231343935323831303539223b6c616e677c733a323a226964223b),
('6415d601bb3fdcd4ede5798462e29176712cc0f2', '::1', 1495678371, 0x5f5f63695f6c6173745f726567656e65726174657c693a313439353637383335373b6964656e746974797c733a31353a2261646d696e4061646d696e2e636f6d223b656d61696c7c733a31353a2261646d696e4061646d696e2e636f6d223b757365725f69647c733a313a2231223b67726f75705f69647c733a313a2230223b6f6c645f6c6173745f6c6f67696e7c733a31303a2231343935363435383139223b6c616e677c733a323a226964223b),
('670896bfdfb5c7d054681563039ca96fe117536d', '::1', 1495406131, 0x5f5f63695f6c6173745f726567656e65726174657c693a313439353430363038323b6964656e746974797c733a31353a2261646d696e4061646d696e2e636f6d223b656d61696c7c733a31353a2261646d696e4061646d696e2e636f6d223b757365725f69647c733a313a2231223b67726f75705f69647c733a313a2230223b6f6c645f6c6173745f6c6f67696e7c733a31303a2231343935323831303539223b6c616e677c733a323a226964223b),
('6b37c369a504e4db88ae6214771c254e24c62377', '::1', 1495942383, 0x5f5f63695f6c6173745f726567656e65726174657c693a313439353934323137303b6964656e746974797c733a31353a2261646d696e4061646d696e2e636f6d223b656d61696c7c733a31353a2261646d696e4061646d696e2e636f6d223b757365725f69647c733a313a2231223b67726f75705f69647c733a313a2230223b6f6c645f6c6173745f6c6f67696e7c733a31303a2231343935383835383630223b6c616e677c733a323a226964223b),
('70ef9c582eb88d86da9d369e5521bafbbfc83af8', '::1', 1495410619, 0x5f5f63695f6c6173745f726567656e65726174657c693a313439353431303338373b6964656e746974797c733a31353a2261646d696e4061646d696e2e636f6d223b656d61696c7c733a31353a2261646d696e4061646d696e2e636f6d223b757365725f69647c733a313a2231223b67726f75705f69647c733a313a2230223b6f6c645f6c6173745f6c6f67696e7c733a31303a2231343935323831303539223b6c616e677c733a323a226964223b),
('738d264579f46915abed6245f35260cd749515e0', '::1', 1494825044, 0x5f5f63695f6c6173745f726567656e65726174657c693a313439343832353030343b6964656e746974797c733a31353a2261646d696e4061646d696e2e636f6d223b656d61696c7c733a31353a2261646d696e4061646d696e2e636f6d223b757365725f69647c733a313a2231223b67726f75705f69647c733a313a2230223b6f6c645f6c6173745f6c6f67696e7c733a31303a2231343934363432343237223b6c616e677c733a323a226964223b),
('753140941cc3cd25a9b4a815e1597138a52c4eba', '::1', 1495408864, 0x5f5f63695f6c6173745f726567656e65726174657c693a313439353430383634393b6964656e746974797c733a31353a2261646d696e4061646d696e2e636f6d223b656d61696c7c733a31353a2261646d696e4061646d696e2e636f6d223b757365725f69647c733a313a2231223b67726f75705f69647c733a313a2230223b6f6c645f6c6173745f6c6f67696e7c733a31303a2231343935323831303539223b6c616e677c733a323a226964223b),
('7676b4175163e8bbb77a0122c6ce1be39986adcd', '::1', 1495946253, 0x5f5f63695f6c6173745f726567656e65726174657c693a313439353934353939323b6964656e746974797c733a31353a2261646d696e4061646d696e2e636f6d223b656d61696c7c733a31353a2261646d696e4061646d696e2e636f6d223b757365725f69647c733a313a2231223b67726f75705f69647c733a313a2230223b6f6c645f6c6173745f6c6f67696e7c733a31303a2231343935383835383630223b6c616e677c733a323a226964223b),
('792e90aab6bdfb0dbf05bf08587c7d0d9113272b', '::1', 1495885949, 0x5f5f63695f6c6173745f726567656e65726174657c693a313439353838353932373b6964656e746974797c733a31353a2261646d696e4061646d696e2e636f6d223b656d61696c7c733a31353a2261646d696e4061646d696e2e636f6d223b757365725f69647c733a313a2231223b67726f75705f69647c733a313a2230223b6f6c645f6c6173745f6c6f67696e7c733a31303a2231343935373634393833223b6c616e677c733a323a226964223b6e616d655f6d65726368616e646973657c733a363a22737061747531223b5f5f63695f766172737c613a313a7b733a31363a226e616d655f6d65726368616e64697365223b733a333a226f6c64223b7d),
('7f3b4a1798093f8771ff97993fdb6f4daea6f4df', '::1', 1495892538, 0x5f5f63695f6c6173745f726567656e65726174657c693a313439353839323336313b6964656e746974797c733a31353a2261646d696e4061646d696e2e636f6d223b656d61696c7c733a31353a2261646d696e4061646d696e2e636f6d223b757365725f69647c733a313a2231223b67726f75705f69647c733a313a2230223b6f6c645f6c6173745f6c6f67696e7c733a31303a2231343935373634393833223b6c616e677c733a323a226964223b7374617475734d73677c733a32383a2246696c65732075706c6f61646564207375636365737366756c6c792e223b5f5f63695f766172737c613a313a7b733a393a227374617475734d7367223b733a333a226e6577223b7d),
('7f7cf3b178381d77888b94f675016e9a85e500b8', '::1', 1495691709, 0x5f5f63695f6c6173745f726567656e65726174657c693a313439353639313730313b6964656e746974797c733a31353a2261646d696e4061646d696e2e636f6d223b656d61696c7c733a31353a2261646d696e4061646d696e2e636f6d223b757365725f69647c733a313a2231223b67726f75705f69647c733a313a2230223b6f6c645f6c6173745f6c6f67696e7c733a31303a2231343935363638313731223b6c616e677c733a323a226964223b),
('80d5f7ddb378e31d8e6abf2e34eff411bea7ae33', '::1', 1495890257, 0x5f5f63695f6c6173745f726567656e65726174657c693a313439353839303036313b6964656e746974797c733a31353a2261646d696e4061646d696e2e636f6d223b656d61696c7c733a31353a2261646d696e4061646d696e2e636f6d223b757365725f69647c733a313a2231223b67726f75705f69647c733a313a2230223b6f6c645f6c6173745f6c6f67696e7c733a31303a2231343935373634393833223b6c616e677c733a323a226964223b),
('869c261cf170428abdb693a0ef4f2dc47851045f', '::1', 1495942069, 0x5f5f63695f6c6173745f726567656e65726174657c693a313439353934313738323b6964656e746974797c733a31353a2261646d696e4061646d696e2e636f6d223b656d61696c7c733a31353a2261646d696e4061646d696e2e636f6d223b757365725f69647c733a313a2231223b67726f75705f69647c733a313a2230223b6f6c645f6c6173745f6c6f67696e7c733a31303a2231343935383835383630223b6c616e677c733a323a226964223b),
('88356dad7548f08c824045a614040bdb33926550', '::1', 1495888165, 0x5f5f63695f6c6173745f726567656e65726174657c693a313439353838373838363b6964656e746974797c733a31353a2261646d696e4061646d696e2e636f6d223b656d61696c7c733a31353a2261646d696e4061646d696e2e636f6d223b757365725f69647c733a313a2231223b67726f75705f69647c733a313a2230223b6f6c645f6c6173745f6c6f67696e7c733a31303a2231343935373634393833223b6c616e677c733a323a226964223b),
('88dd45b3f2252ce3d55370a15d4990caf0fbae50', '::1', 1495940698, 0x5f5f63695f6c6173745f726567656e65726174657c693a313439353934303430323b6964656e746974797c733a31353a2261646d696e4061646d696e2e636f6d223b656d61696c7c733a31353a2261646d696e4061646d696e2e636f6d223b757365725f69647c733a313a2231223b67726f75705f69647c733a313a2230223b6f6c645f6c6173745f6c6f67696e7c733a31303a2231343935383835383630223b6c616e677c733a323a226964223b),
('8b520d41de0d81649516c657ae7a347941519da9', '::1', 1495705108, 0x5f5f63695f6c6173745f726567656e65726174657c693a313439353730353130383b6964656e746974797c733a31353a2261646d696e4061646d696e2e636f6d223b656d61696c7c733a31353a2261646d696e4061646d696e2e636f6d223b757365725f69647c733a313a2231223b67726f75705f69647c733a313a2230223b6f6c645f6c6173745f6c6f67696e7c733a31303a2231343935363930333636223b6c616e677c733a323a226964223b7c4e3b),
('8ddf0a4258c25c7e9e5406b0ac00e75212c72632', '::1', 1494844484, 0x5f5f63695f6c6173745f726567656e65726174657c693a313439343834343436343b6964656e746974797c733a31353a2261646d696e4061646d696e2e636f6d223b656d61696c7c733a31353a2261646d696e4061646d696e2e636f6d223b757365725f69647c733a313a2231223b67726f75705f69647c733a313a2230223b6f6c645f6c6173745f6c6f67696e7c733a31303a2231343934383235303238223b6c616e677c733a323a226964223b),
('8eaaad25226332834546383c4a5b8e74d068e0f7', '::1', 1495889610, 0x5f5f63695f6c6173745f726567656e65726174657c693a313439353838393432373b6964656e746974797c733a31353a2261646d696e4061646d696e2e636f6d223b656d61696c7c733a31353a2261646d696e4061646d696e2e636f6d223b757365725f69647c733a313a2231223b67726f75705f69647c733a313a2230223b6f6c645f6c6173745f6c6f67696e7c733a31303a2231343935373634393833223b6c616e677c733a323a226964223b),
('8f7d8bf684d8ccf92cc2cbc726bc495f3c866cb2', '::1', 1495886916, 0x5f5f63695f6c6173745f726567656e65726174657c693a313439353838363830363b6964656e746974797c733a31353a2261646d696e4061646d696e2e636f6d223b656d61696c7c733a31353a2261646d696e4061646d696e2e636f6d223b757365725f69647c733a313a2231223b67726f75705f69647c733a313a2230223b6f6c645f6c6173745f6c6f67696e7c733a31303a2231343935373634393833223b6c616e677c733a323a226964223b),
('8ff61c37a58834c145c0cdee9c983e1b0700df73', '::1', 1495942674, 0x5f5f63695f6c6173745f726567656e65726174657c693a313439353934323531303b6964656e746974797c733a31353a2261646d696e4061646d696e2e636f6d223b656d61696c7c733a31353a2261646d696e4061646d696e2e636f6d223b757365725f69647c733a313a2231223b67726f75705f69647c733a313a2230223b6f6c645f6c6173745f6c6f67696e7c733a31303a2231343935383835383630223b6c616e677c733a323a226964223b),
('92158026f7d2601dc8bece3cbc71d0901c9fdd7d', '::1', 1495670727, 0x5f5f63695f6c6173745f726567656e65726174657c693a313439353637303732373b6964656e746974797c733a31353a2261646d696e4061646d696e2e636f6d223b656d61696c7c733a31353a2261646d696e4061646d696e2e636f6d223b757365725f69647c733a313a2231223b67726f75705f69647c733a313a2230223b6f6c645f6c6173745f6c6f67696e7c733a31303a2231343935363435383139223b6c616e677c733a323a226964223b),
('94f4e24a6828b40f943c8fe9951b72adf93e527a', '::1', 1495417834, 0x5f5f63695f6c6173745f726567656e65726174657c693a313439353431373738343b6964656e746974797c733a31353a2261646d696e4061646d696e2e636f6d223b656d61696c7c733a31353a2261646d696e4061646d696e2e636f6d223b757365725f69647c733a313a2231223b67726f75705f69647c733a313a2230223b6f6c645f6c6173745f6c6f67696e7c733a31303a2231343935323831303539223b6c616e677c733a323a226964223b),
('9533f6a2831c94b59a8e93849b856b9d1f32b27c', '::1', 1495890048, 0x5f5f63695f6c6173745f726567656e65726174657c693a313439353838393735353b6964656e746974797c733a31353a2261646d696e4061646d696e2e636f6d223b656d61696c7c733a31353a2261646d696e4061646d696e2e636f6d223b757365725f69647c733a313a2231223b67726f75705f69647c733a313a2230223b6f6c645f6c6173745f6c6f67696e7c733a31303a2231343935373634393833223b6c616e677c733a323a226964223b),
('959c47f428c21aed09d707d82fe967206c44375a', '::1', 1495703946, 0x5f5f63695f6c6173745f726567656e65726174657c693a313439353730333636303b6964656e746974797c733a31353a2261646d696e4061646d696e2e636f6d223b656d61696c7c733a31353a2261646d696e4061646d696e2e636f6d223b757365725f69647c733a313a2231223b67726f75705f69647c733a313a2230223b6f6c645f6c6173745f6c6f67696e7c733a31303a2231343935363930333636223b6c616e677c733a323a226964223b7c4e3b6e616d655f6d65726368616e646973657c4e3b5f5f63695f766172737c613a313a7b733a31363a226e616d655f6d65726368616e64697365223b733a333a226f6c64223b7d),
('95a9e8374313c9cb3a99a7e002e8602c4fb233f8', '::1', 1495411141, 0x5f5f63695f6c6173745f726567656e65726174657c693a313439353431303838313b6964656e746974797c733a31353a2261646d696e4061646d696e2e636f6d223b656d61696c7c733a31353a2261646d696e4061646d696e2e636f6d223b757365725f69647c733a313a2231223b67726f75705f69647c733a313a2230223b6f6c645f6c6173745f6c6f67696e7c733a31303a2231343935323831303539223b6c616e677c733a323a226964223b),
('97d563703b23f440382f0ebcb194badd2cfb3a4b', '::1', 1495437693, 0x5f5f63695f6c6173745f726567656e65726174657c693a313439353433373537393b6964656e746974797c733a31353a2261646d696e4061646d696e2e636f6d223b656d61696c7c733a31353a2261646d696e4061646d696e2e636f6d223b757365725f69647c733a313a2231223b67726f75705f69647c733a313a2230223b6f6c645f6c6173745f6c6f67696e7c733a31303a2231343935343036313139223b6c616e677c733a323a226964223b),
('9834d05a412802f536aff61f95129eff2a150b2d', '::1', 1494825460, 0x5f5f63695f6c6173745f726567656e65726174657c693a313439343832353336303b6964656e746974797c733a31353a2261646d696e4061646d696e2e636f6d223b656d61696c7c733a31353a2261646d696e4061646d696e2e636f6d223b757365725f69647c733a313a2231223b67726f75705f69647c733a313a2230223b6f6c645f6c6173745f6c6f67696e7c733a31303a2231343934363432343237223b6c616e677c733a323a226964223b),
('992c86b99d1bb19133f79f07b34c0c2e81e71fcf', '::1', 1495437112, 0x5f5f63695f6c6173745f726567656e65726174657c693a313439353433363933373b6964656e746974797c733a31353a2261646d696e4061646d696e2e636f6d223b656d61696c7c733a31353a2261646d696e4061646d696e2e636f6d223b757365725f69647c733a313a2231223b67726f75705f69647c733a313a2230223b6f6c645f6c6173745f6c6f67696e7c733a31303a2231343935343036313139223b6c616e677c733a323a226964223b),
('9b466d6b66a1280f2b5d0d33487c2d67cb7dc8f0', '::1', 1495938472, 0x5f5f63695f6c6173745f726567656e65726174657c693a313439353933383139353b6964656e746974797c733a31353a2261646d696e4061646d696e2e636f6d223b656d61696c7c733a31353a2261646d696e4061646d696e2e636f6d223b757365725f69647c733a313a2231223b67726f75705f69647c733a313a2230223b6f6c645f6c6173745f6c6f67696e7c733a31303a2231343935383835383630223b6c616e677c733a323a226964223b),
('9d3881247c653f01104db192efa7087122cfda40', '::1', 1495888823, 0x5f5f63695f6c6173745f726567656e65726174657c693a313439353838383533343b6964656e746974797c733a31353a2261646d696e4061646d696e2e636f6d223b656d61696c7c733a31353a2261646d696e4061646d696e2e636f6d223b757365725f69647c733a313a2231223b67726f75705f69647c733a313a2230223b6f6c645f6c6173745f6c6f67696e7c733a31303a2231343935373634393833223b6c616e677c733a323a226964223b),
('9f87e1a5586957bb546b285568baa7a23136c084', '::1', 1495407666, 0x5f5f63695f6c6173745f726567656e65726174657c693a313439353430373538303b6964656e746974797c733a31353a2261646d696e4061646d696e2e636f6d223b656d61696c7c733a31353a2261646d696e4061646d696e2e636f6d223b757365725f69647c733a313a2231223b67726f75705f69647c733a313a2230223b6f6c645f6c6173745f6c6f67696e7c733a31303a2231343935323831303539223b6c616e677c733a323a226964223b),
('9fc2fd1a7db04de5062d4c9fc0f605456ae2f8cb', '::1', 1495938774, 0x5f5f63695f6c6173745f726567656e65726174657c693a313439353933383531343b6964656e746974797c733a31353a2261646d696e4061646d696e2e636f6d223b656d61696c7c733a31353a2261646d696e4061646d696e2e636f6d223b757365725f69647c733a313a2231223b67726f75705f69647c733a313a2230223b6f6c645f6c6173745f6c6f67696e7c733a31303a2231343935383835383630223b6c616e677c733a323a226964223b),
('9fcf342d184753e38912f67b68fe1159dc9e0518', '::1', 1495890671, 0x5f5f63695f6c6173745f726567656e65726174657c693a313439353839303339353b6964656e746974797c733a31353a2261646d696e4061646d696e2e636f6d223b656d61696c7c733a31353a2261646d696e4061646d696e2e636f6d223b757365725f69647c733a313a2231223b67726f75705f69647c733a313a2230223b6f6c645f6c6173745f6c6f67696e7c733a31303a2231343935373634393833223b6c616e677c733a323a226964223b),
('9ffd9ed36a221d8c6ea12cceb743384be840e83d', '::1', 1495941556, 0x5f5f63695f6c6173745f726567656e65726174657c693a313439353934313431363b6964656e746974797c733a31353a2261646d696e4061646d696e2e636f6d223b656d61696c7c733a31353a2261646d696e4061646d696e2e636f6d223b757365725f69647c733a313a2231223b67726f75705f69647c733a313a2230223b6f6c645f6c6173745f6c6f67696e7c733a31303a2231343935383835383630223b6c616e677c733a323a226964223b),
('a0c269223f54bce0e454b83d37735547138a7f36', '::1', 1495438149, 0x5f5f63695f6c6173745f726567656e65726174657c693a313439353433383134373b6964656e746974797c733a31353a2261646d696e4061646d696e2e636f6d223b656d61696c7c733a31353a2261646d696e4061646d696e2e636f6d223b757365725f69647c733a313a2231223b67726f75705f69647c733a313a2230223b6f6c645f6c6173745f6c6f67696e7c733a31303a2231343935343036313139223b6c616e677c733a323a226964223b),
('a13e4c8ac48f614f01b56ea97311a686e0d7a8ed', '::1', 1495677814, 0x5f5f63695f6c6173745f726567656e65726174657c693a313439353637373739313b6964656e746974797c733a31353a2261646d696e4061646d696e2e636f6d223b656d61696c7c733a31353a2261646d696e4061646d696e2e636f6d223b757365725f69647c733a313a2231223b67726f75705f69647c733a313a2230223b6f6c645f6c6173745f6c6f67696e7c733a31303a2231343935363435383139223b6c616e677c733a323a226964223b),
('a69d2333147295c92ac68867cad054e5d391ff8a', '::1', 1495940261, 0x5f5f63695f6c6173745f726567656e65726174657c693a313439353934303036373b6964656e746974797c733a31353a2261646d696e4061646d696e2e636f6d223b656d61696c7c733a31353a2261646d696e4061646d696e2e636f6d223b757365725f69647c733a313a2231223b67726f75705f69647c733a313a2230223b6f6c645f6c6173745f6c6f67696e7c733a31303a2231343935383835383630223b6c616e677c733a323a226964223b),
('a73b9c31e7b9d1d7c894184c125498ad86e123b1', '::1', 1494844802, 0x5f5f63695f6c6173745f726567656e65726174657c693a313439343834343736393b6964656e746974797c733a31353a2261646d696e4061646d696e2e636f6d223b656d61696c7c733a31353a2261646d696e4061646d696e2e636f6d223b757365725f69647c733a313a2231223b67726f75705f69647c733a313a2230223b6f6c645f6c6173745f6c6f67696e7c733a31303a2231343934383235303238223b6c616e677c733a323a226964223b),
('b0e30144a49928360aed6a71e8bbc63861921614', '::1', 1495646056, 0x5f5f63695f6c6173745f726567656e65726174657c693a313439353634353830393b6964656e746974797c733a31353a2261646d696e4061646d696e2e636f6d223b656d61696c7c733a31353a2261646d696e4061646d696e2e636f6d223b757365725f69647c733a313a2231223b67726f75705f69647c733a313a2230223b6f6c645f6c6173745f6c6f67696e7c733a31303a2231343935363130363037223b6c616e677c733a323a226964223b),
('b45474dc1392aa9bb173d38b5746c9bc877896d5', '::1', 1495430624, 0x5f5f63695f6c6173745f726567656e65726174657c693a313439353433303332343b6964656e746974797c733a31353a2261646d696e4061646d696e2e636f6d223b656d61696c7c733a31353a2261646d696e4061646d696e2e636f6d223b757365725f69647c733a313a2231223b67726f75705f69647c733a313a2230223b6f6c645f6c6173745f6c6f67696e7c733a31303a2231343935343036313139223b6c616e677c733a323a226964223b),
('b47fc063130465c9d3b23ef4604762edd413aab5', '::1', 1495152297, 0x5f5f63695f6c6173745f726567656e65726174657c693a313439353135323233303b6964656e746974797c733a31353a2261646d696e4061646d696e2e636f6d223b656d61696c7c733a31353a2261646d696e4061646d696e2e636f6d223b757365725f69647c733a313a2231223b67726f75705f69647c733a313a2230223b6f6c645f6c6173745f6c6f67696e7c733a31303a2231343935303934303837223b6c616e677c733a323a226964223b),
('b687872c6a71a01c6c46adaf76b9d3984a64e455', '::1', 1495164434, 0x5f5f63695f6c6173745f726567656e65726174657c693a313439353136343339363b6964656e746974797c733a31353a2261646d696e4061646d696e2e636f6d223b656d61696c7c733a31353a2261646d696e4061646d696e2e636f6d223b757365725f69647c733a313a2231223b67726f75705f69647c733a313a2230223b6f6c645f6c6173745f6c6f67696e7c733a31303a2231343935313531363630223b6c616e677c733a323a226964223b),
('b8a1b2ebb4a0eaf38ba38901fd5c63ea780dc994', '::1', 1495945109, 0x5f5f63695f6c6173745f726567656e65726174657c693a313439353934343933333b6964656e746974797c733a31353a2261646d696e4061646d696e2e636f6d223b656d61696c7c733a31353a2261646d696e4061646d696e2e636f6d223b757365725f69647c733a313a2231223b67726f75705f69647c733a313a2230223b6f6c645f6c6173745f6c6f67696e7c733a31303a2231343935383835383630223b6c616e677c733a323a226964223b),
('b8f9e4ae45bee5f3071ddc650ba096de1c1890ab', '::1', 1495893862, 0x5f5f63695f6c6173745f726567656e65726174657c693a313439353839333538343b6964656e746974797c733a31353a2261646d696e4061646d696e2e636f6d223b656d61696c7c733a31353a2261646d696e4061646d696e2e636f6d223b757365725f69647c733a313a2231223b67726f75705f69647c733a313a2230223b6f6c645f6c6173745f6c6f67696e7c733a31303a2231343935373634393833223b6c616e677c733a323a226964223b),
('b937970e11818d73dd914e7ef850935f5600b0b6', '::1', 1495408310, 0x5f5f63695f6c6173745f726567656e65726174657c693a313439353430383031393b6964656e746974797c733a31353a2261646d696e4061646d696e2e636f6d223b656d61696c7c733a31353a2261646d696e4061646d696e2e636f6d223b757365725f69647c733a313a2231223b67726f75705f69647c733a313a2230223b6f6c645f6c6173745f6c6f67696e7c733a31303a2231343935323831303539223b6c616e677c733a323a226964223b),
('ba04bdf7062b3dd095e353ae645bc4a031f598b2', '::1', 1495941020, 0x5f5f63695f6c6173745f726567656e65726174657c693a313439353934303732373b6964656e746974797c733a31353a2261646d696e4061646d696e2e636f6d223b656d61696c7c733a31353a2261646d696e4061646d696e2e636f6d223b757365725f69647c733a313a2231223b67726f75705f69647c733a313a2230223b6f6c645f6c6173745f6c6f67696e7c733a31303a2231343935383835383630223b6c616e677c733a323a226964223b6e616d655f6d65726368616e646973657c733a353a224b616f7331223b5f5f63695f766172737c613a313a7b733a31363a226e616d655f6d65726368616e64697365223b733a333a226f6c64223b7d),
('be4963366e7537746da593598ee63f9f411d59cf', '::1', 1495646135, 0x5f5f63695f6c6173745f726567656e65726174657c693a313439353634363133353b6964656e746974797c733a31353a2261646d696e4061646d696e2e636f6d223b656d61696c7c733a31353a2261646d696e4061646d696e2e636f6d223b757365725f69647c733a313a2231223b67726f75705f69647c733a313a2230223b6f6c645f6c6173745f6c6f67696e7c733a31303a2231343935363130363037223b6c616e677c733a323a226964223b),
('bed72f9b0004a3cd5f35899e6527ea2c178a379f', '::1', 1495437504, 0x5f5f63695f6c6173745f726567656e65726174657c693a313439353433373236323b6964656e746974797c733a31353a2261646d696e4061646d696e2e636f6d223b656d61696c7c733a31353a2261646d696e4061646d696e2e636f6d223b757365725f69647c733a313a2231223b67726f75705f69647c733a313a2230223b6f6c645f6c6173745f6c6f67696e7c733a31303a2231343935343036313139223b6c616e677c733a323a226964223b),
('c0e864ff2160aac0fc3ad7891418b93daa2abf3b', '::1', 1495945614, 0x5f5f63695f6c6173745f726567656e65726174657c693a313439353934353333343b6964656e746974797c733a31353a2261646d696e4061646d696e2e636f6d223b656d61696c7c733a31353a2261646d696e4061646d696e2e636f6d223b757365725f69647c733a313a2231223b67726f75705f69647c733a313a2230223b6f6c645f6c6173745f6c6f67696e7c733a31303a2231343935383835383630223b6c616e677c733a323a226964223b),
('c2b75222cb5650f160875fcb81b0cfeedfb61f05', '::1', 1495764999, 0x5f5f63695f6c6173745f726567656e65726174657c693a313439353736343937323b6964656e746974797c733a31353a2261646d696e4061646d696e2e636f6d223b656d61696c7c733a31353a2261646d696e4061646d696e2e636f6d223b757365725f69647c733a313a2231223b67726f75705f69647c733a313a2230223b6f6c645f6c6173745f6c6f67696e7c733a31303a2231343935373032303139223b6c616e677c733a323a226964223b),
('c59de18f32d51f50b19bd50590bbfdcc176ca297', '::1', 1495702301, 0x5f5f63695f6c6173745f726567656e65726174657c693a313439353730323030363b6964656e746974797c733a31353a2261646d696e4061646d696e2e636f6d223b656d61696c7c733a31353a2261646d696e4061646d696e2e636f6d223b757365725f69647c733a313a2231223b67726f75705f69647c733a313a2230223b6f6c645f6c6173745f6c6f67696e7c733a31303a2231343935363930333636223b6c616e677c733a323a226964223b),
('c6cc086a98dae7af27d7ec7157f0fea71762c98e', '::1', 1495709017, 0x5f5f63695f6c6173745f726567656e65726174657c693a313439353730383736383b6964656e746974797c733a31353a2261646d696e4061646d696e2e636f6d223b656d61696c7c733a31353a2261646d696e4061646d696e2e636f6d223b757365725f69647c733a313a2231223b67726f75705f69647c733a313a2230223b6f6c645f6c6173745f6c6f67696e7c733a31303a2231343935363930333636223b6c616e677c733a323a226964223b7c4e3b6e616d655f6d65726368616e646973657c733a31333a2262616a75206c696d6120616a61223b5f5f63695f766172737c613a313a7b733a31363a226e616d655f6d65726368616e64697365223b733a333a226f6c64223b7d),
('c9663cadfd4256bd01132ae837ca103b2c9d0b1c', '::1', 1495432270, 0x5f5f63695f6c6173745f726567656e65726174657c693a313439353433313938393b6964656e746974797c733a31353a2261646d696e4061646d696e2e636f6d223b656d61696c7c733a31353a2261646d696e4061646d696e2e636f6d223b757365725f69647c733a313a2231223b67726f75705f69647c733a313a2230223b6f6c645f6c6173745f6c6f67696e7c733a31303a2231343935343036313139223b6c616e677c733a323a226964223b),
('d0f253c05dd6577119291c8bba4f62bfde92876d', '::1', 1495939551, 0x5f5f63695f6c6173745f726567656e65726174657c693a313439353933393235343b6964656e746974797c733a31353a2261646d696e4061646d696e2e636f6d223b656d61696c7c733a31353a2261646d696e4061646d696e2e636f6d223b757365725f69647c733a313a2231223b67726f75705f69647c733a313a2230223b6f6c645f6c6173745f6c6f67696e7c733a31303a2231343935383835383630223b6c616e677c733a323a226964223b),
('d486ac021e1bd12f423225c8b47feefeaea0a935', '::1', 1495966845, 0x5f5f63695f6c6173745f726567656e65726174657c693a313439353936363832343b6964656e746974797c733a31353a2261646d696e4061646d696e2e636f6d223b656d61696c7c733a31353a2261646d696e4061646d696e2e636f6d223b757365725f69647c733a313a2231223b67726f75705f69647c733a313a2230223b6f6c645f6c6173745f6c6f67696e7c733a31303a2231343935393337373736223b6c616e677c733a323a226964223b),
('d7ab156075bfb741db85377c99f55dc83af77c37', '::1', 1495894171, 0x5f5f63695f6c6173745f726567656e65726174657c693a313439353839333931313b6964656e746974797c733a31353a2261646d696e4061646d696e2e636f6d223b656d61696c7c733a31353a2261646d696e4061646d696e2e636f6d223b757365725f69647c733a313a2231223b67726f75705f69647c733a313a2230223b6f6c645f6c6173745f6c6f67696e7c733a31303a2231343935373634393833223b6c616e677c733a323a226964223b),
('d8798ed2996246bfdf8c334e7e79edfbe905338b', '::1', 1495944851, 0x5f5f63695f6c6173745f726567656e65726174657c693a313439353934343633313b6964656e746974797c733a31353a2261646d696e4061646d696e2e636f6d223b656d61696c7c733a31353a2261646d696e4061646d696e2e636f6d223b757365725f69647c733a313a2231223b67726f75705f69647c733a313a2230223b6f6c645f6c6173745f6c6f67696e7c733a31303a2231343935383835383630223b6c616e677c733a323a226964223b),
('d8ae6bc3da8a4ba847d49bd1a35ad3b25f96be15', '::1', 1495894898, 0x5f5f63695f6c6173745f726567656e65726174657c693a313439353839343631393b6964656e746974797c733a31353a2261646d696e4061646d696e2e636f6d223b656d61696c7c733a31353a2261646d696e4061646d696e2e636f6d223b757365725f69647c733a313a2231223b67726f75705f69647c733a313a2230223b6f6c645f6c6173745f6c6f67696e7c733a31303a2231343935373634393833223b6c616e677c733a323a226964223b),
('d91a3f77541deb51ac162978dcb969cb282235f4', '::1', 1495944276, 0x5f5f63695f6c6173745f726567656e65726174657c693a313439353934343237363b6964656e746974797c733a31353a2261646d696e4061646d696e2e636f6d223b656d61696c7c733a31353a2261646d696e4061646d696e2e636f6d223b757365725f69647c733a313a2231223b67726f75705f69647c733a313a2230223b6f6c645f6c6173745f6c6f67696e7c733a31303a2231343935383835383630223b6c616e677c733a323a226964223b),
('da4d3db593e3393ee4bf5086c1ee000678d5c102', '::1', 1494642629, 0x5f5f63695f6c6173745f726567656e65726174657c693a313439343634323431343b6964656e746974797c733a31353a2261646d696e4061646d696e2e636f6d223b656d61696c7c733a31353a2261646d696e4061646d696e2e636f6d223b757365725f69647c733a313a2231223b67726f75705f69647c733a313a2230223b6f6c645f6c6173745f6c6f67696e7c733a31303a2231343933393931343536223b6c616e677c733a323a226964223b),
('dd2aa2b94da2b2a920b6bbedbe2686906437db6e', '::1', 1495944233, 0x5f5f63695f6c6173745f726567656e65726174657c693a313439353934333936373b6964656e746974797c733a31353a2261646d696e4061646d696e2e636f6d223b656d61696c7c733a31353a2261646d696e4061646d696e2e636f6d223b757365725f69647c733a313a2231223b67726f75705f69647c733a313a2230223b6f6c645f6c6173745f6c6f67696e7c733a31303a2231343935383835383630223b6c616e677c733a323a226964223b),
('dda075d662d84d2fde80034642c69124f730ed26', '::1', 1495887505, 0x5f5f63695f6c6173745f726567656e65726174657c693a313439353838373233333b6964656e746974797c733a31353a2261646d696e4061646d696e2e636f6d223b656d61696c7c733a31353a2261646d696e4061646d696e2e636f6d223b757365725f69647c733a313a2231223b67726f75705f69647c733a313a2230223b6f6c645f6c6173745f6c6f67696e7c733a31303a2231343935373634393833223b6c616e677c733a323a226964223b),
('ddb12a6fc4898fdb3f59f26c852efd1697bc1686', '::1', 1495888482, 0x5f5f63695f6c6173745f726567656e65726174657c693a313439353838383231373b6964656e746974797c733a31353a2261646d696e4061646d696e2e636f6d223b656d61696c7c733a31353a2261646d696e4061646d696e2e636f6d223b757365725f69647c733a313a2231223b67726f75705f69647c733a313a2230223b6f6c645f6c6173745f6c6f67696e7c733a31303a2231343935373634393833223b6c616e677c733a323a226964223b),
('def99c6af927616856a646cdf46588c3e25e4ecb', '::1', 1495409190, 0x5f5f63695f6c6173745f726567656e65726174657c693a313439353430383936373b6964656e746974797c733a31353a2261646d696e4061646d696e2e636f6d223b656d61696c7c733a31353a2261646d696e4061646d696e2e636f6d223b757365725f69647c733a313a2231223b67726f75705f69647c733a313a2230223b6f6c645f6c6173745f6c6f67696e7c733a31303a2231343935323831303539223b6c616e677c733a323a226964223b),
('e35cc51ab82f3c7b4aa3f6af5cbc9af6742a2840', '::1', 1495690915, 0x5f5f63695f6c6173745f726567656e65726174657c693a313439353639303635393b6964656e746974797c733a31353a2261646d696e4061646d696e2e636f6d223b656d61696c7c733a31353a2261646d696e4061646d696e2e636f6d223b757365725f69647c733a313a2231223b67726f75705f69647c733a313a2230223b6f6c645f6c6173745f6c6f67696e7c733a31303a2231343935363638313731223b6c616e677c733a323a226964223b),
('e3ca147ac1b8fc29143e562f0fef4c39b6445019', '::1', 1495946428, 0x5f5f63695f6c6173745f726567656e65726174657c693a313439353934363332313b6964656e746974797c733a31353a2261646d696e4061646d696e2e636f6d223b656d61696c7c733a31353a2261646d696e4061646d696e2e636f6d223b757365725f69647c733a313a2231223b67726f75705f69647c733a313a2230223b6f6c645f6c6173745f6c6f67696e7c733a31303a2231343935383835383630223b6c616e677c733a323a226964223b),
('e45aa5ac2250ea33117da5cc39f74640a6fe2278', '::1', 1495892278, 0x5f5f63695f6c6173745f726567656e65726174657c693a313439353839323033323b6964656e746974797c733a31353a2261646d696e4061646d696e2e636f6d223b656d61696c7c733a31353a2261646d696e4061646d696e2e636f6d223b757365725f69647c733a313a2231223b67726f75705f69647c733a313a2230223b6f6c645f6c6173745f6c6f67696e7c733a31303a2231343935373634393833223b6c616e677c733a323a226964223b),
('e5868a048852d91115c1343bd361721b3fc601f0', '::1', 1495707760, 0x5f5f63695f6c6173745f726567656e65726174657c693a313439353730373734343b6964656e746974797c733a31353a2261646d696e4061646d696e2e636f6d223b656d61696c7c733a31353a2261646d696e4061646d696e2e636f6d223b757365725f69647c733a313a2231223b67726f75705f69647c733a313a2230223b6f6c645f6c6173745f6c6f67696e7c733a31303a2231343935363930333636223b6c616e677c733a323a226964223b7c4e3b),
('e592f5df8bd3b37ab03a614f5756f570eea812fb', '::1', 1494825941, 0x5f5f63695f6c6173745f726567656e65726174657c693a313439343832353836343b6964656e746974797c733a31353a2261646d696e4061646d696e2e636f6d223b656d61696c7c733a31353a2261646d696e4061646d696e2e636f6d223b757365725f69647c733a313a2231223b67726f75705f69647c733a313a2230223b6f6c645f6c6173745f6c6f67696e7c733a31303a2231343934363432343237223b6c616e677c733a323a226964223b),
('e7f3b160d41f05b116cab6508983328fa9d5f385', '::1', 1495894576, 0x5f5f63695f6c6173745f726567656e65726174657c693a313439353839343330393b6964656e746974797c733a31353a2261646d696e4061646d696e2e636f6d223b656d61696c7c733a31353a2261646d696e4061646d696e2e636f6d223b757365725f69647c733a313a2231223b67726f75705f69647c733a313a2230223b6f6c645f6c6173745f6c6f67696e7c733a31303a2231343935373634393833223b6c616e677c733a323a226964223b),
('e978d9d2af1aada99056bb34c0a4865fda17469b', '::1', 1495285792, 0x5f5f63695f6c6173745f726567656e65726174657c693a313439353238353737383b6964656e746974797c733a31353a2261646d696e4061646d696e2e636f6d223b656d61696c7c733a31353a2261646d696e4061646d696e2e636f6d223b757365725f69647c733a313a2231223b67726f75705f69647c733a313a2230223b6f6c645f6c6173745f6c6f67696e7c733a31303a2231343935313634343233223b6c616e677c733a323a226964223b),
('eac8c3bd5bb084ed5063dce0cf4a80b5f37f14a2', '::1', 1495691662, 0x5f5f63695f6c6173745f726567656e65726174657c693a313439353639313337323b6964656e746974797c733a31353a2261646d696e4061646d696e2e636f6d223b656d61696c7c733a31353a2261646d696e4061646d696e2e636f6d223b757365725f69647c733a313a2231223b67726f75705f69647c733a313a2230223b6f6c645f6c6173745f6c6f67696e7c733a31303a2231343935363638313731223b6c616e677c733a323a226964223b),
('ecc88dac27320df3a15cbe2a1ccfbd66f47abd8f', '::1', 1495440073, 0x5f5f63695f6c6173745f726567656e65726174657c693a313439353434303036363b6964656e746974797c733a31353a2261646d696e4061646d696e2e636f6d223b656d61696c7c733a31353a2261646d696e4061646d696e2e636f6d223b757365725f69647c733a313a2231223b67726f75705f69647c733a313a2230223b6f6c645f6c6173745f6c6f67696e7c733a31303a2231343935343036313139223b6c616e677c733a323a226964223b),
('ed75c59c321d56882b6ae4b89dad8b757769463a', '::1', 1495094068, 0x5f5f63695f6c6173745f726567656e65726174657c693a313439353039343036363b),
('edeb64873d677aeb320dc35634841927adb347ea', '::1', 1495157175, 0x5f5f63695f6c6173745f726567656e65726174657c693a313439353135373038333b6964656e746974797c733a31353a2261646d696e4061646d696e2e636f6d223b656d61696c7c733a31353a2261646d696e4061646d696e2e636f6d223b757365725f69647c733a313a2231223b67726f75705f69647c733a313a2230223b6f6c645f6c6173745f6c6f67696e7c733a31303a2231343935303934303837223b6c616e677c733a323a226964223b),
('ee01cdd35daab7a7ac8a4bef53516dfa48f305db', '::1', 1495707554, 0x5f5f63695f6c6173745f726567656e65726174657c693a313439353730373339353b6964656e746974797c733a31353a2261646d696e4061646d696e2e636f6d223b656d61696c7c733a31353a2261646d696e4061646d696e2e636f6d223b757365725f69647c733a313a2231223b67726f75705f69647c733a313a2230223b6f6c645f6c6173745f6c6f67696e7c733a31303a2231343935363930333636223b6c616e677c733a323a226964223b7c4e3b);
INSERT INTO `cms_sessions` (`id`, `ip_address`, `timestamp`, `data`) VALUES
('f1692a7c04ab259f1c84a18117287e2f01a4f271', '::1', 1495610611, 0x5f5f63695f6c6173745f726567656e65726174657c693a313439353631303538303b6964656e746974797c733a31353a2261646d696e4061646d696e2e636f6d223b656d61696c7c733a31353a2261646d696e4061646d696e2e636f6d223b757365725f69647c733a313a2231223b67726f75705f69647c733a313a2230223b6f6c645f6c6173745f6c6f67696e7c733a31303a2231343935343534323132223b6c616e677c733a323a226964223b6d6573736167657c733a32393a223c703e4c6f6767656420496e205375636365737366756c6c793c2f703e223b5f5f63695f766172737c613a313a7b733a373a226d657373616765223b733a333a226f6c64223b7d),
('fdc65f6211ec3cd91e9738c37b659b7ef8799c26', '::1', 1495968974, 0x5f5f63695f6c6173745f726567656e65726174657c693a313439353936383836363b6964656e746974797c733a31353a2261646d696e4061646d696e2e636f6d223b656d61696c7c733a31353a2261646d696e4061646d696e2e636f6d223b757365725f69647c733a313a2231223b67726f75705f69647c733a313a2230223b6f6c645f6c6173745f6c6f67696e7c733a31303a2231343935393337373736223b6c616e677c733a323a226964223b);

-- --------------------------------------------------------

--
-- Table structure for table `cms_stats`
--

CREATE TABLE `cms_stats` (
  `sessid` varchar(35) NOT NULL,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `cms_translate`
--

CREATE TABLE `cms_translate` (
  `id` int(11) NOT NULL,
  `slug` varchar(50) DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `cms_translate_lang`
--

CREATE TABLE `cms_translate_lang` (
  `id` int(11) NOT NULL,
  `lang` varchar(50) NOT NULL,
  `translate` text,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `cms_users`
--

CREATE TABLE `cms_users` (
  `id` int(11) NOT NULL,
  `ip_address` varchar(15) CHARACTER SET utf8 NOT NULL,
  `username` varchar(100) CHARACTER SET utf8 DEFAULT NULL,
  `password` varchar(255) CHARACTER SET utf8 NOT NULL,
  `salt` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `email` varchar(100) CHARACTER SET utf8 NOT NULL,
  `activation_code` varchar(40) CHARACTER SET utf8 DEFAULT NULL,
  `forgotten_password_code` varchar(40) CHARACTER SET utf8 DEFAULT NULL,
  `forgotten_password_time` int(11) UNSIGNED DEFAULT NULL,
  `remember_code` varchar(40) CHARACTER SET utf8 DEFAULT NULL,
  `created_on` int(11) UNSIGNED NOT NULL,
  `last_login` int(11) UNSIGNED DEFAULT NULL,
  `active` tinyint(1) UNSIGNED DEFAULT NULL,
  `first_name` varchar(50) CHARACTER SET utf8 DEFAULT NULL,
  `last_name` varchar(50) CHARACTER SET utf8 DEFAULT NULL,
  `company` varchar(100) CHARACTER SET utf8 DEFAULT NULL,
  `phone` varchar(20) CHARACTER SET utf8 DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `cms_users`
--

INSERT INTO `cms_users` (`id`, `ip_address`, `username`, `password`, `salt`, `email`, `activation_code`, `forgotten_password_code`, `forgotten_password_time`, `remember_code`, `created_on`, `last_login`, `active`, `first_name`, `last_name`, `company`, `phone`) VALUES
(1, '127.0.0.1', 'administrator', '$2a$07$SeBknntpZror9uyftVopmu61qg0ms8Qv1yV6FG.kQOSM.9QhmTo36', '', 'admin@admin.com', 'ad9953b0f8bfe7fef21a24c223932a1f2ade9879', NULL, NULL, NULL, 1268889823, 1495966838, 1, 'Eddy', 'Subratha', 'Admin', '321'),
(2, '::1', NULL, '$2y$08$hN2OOeNjhQoLanv0QM9OUuxOAxTrIDzj3YEkLm5BiMJpKAyy2mumK', NULL, 'eddy.subratha@gmail.com', NULL, NULL, NULL, NULL, 1456843873, 1463144183, 1, 'Eddy', 'Nugoroh', 'Accounting Dept.', '085228720780');

-- --------------------------------------------------------

--
-- Table structure for table `cms_users_groups`
--

CREATE TABLE `cms_users_groups` (
  `id` int(11) UNSIGNED NOT NULL,
  `user_id` int(11) UNSIGNED NOT NULL,
  `group_id` mediumint(8) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `cms_users_groups`
--

INSERT INTO `cms_users_groups` (`id`, `user_id`, `group_id`) VALUES
(31, 1, 0),
(26, 2, 1);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `cms_app_config`
--
ALTER TABLE `cms_app_config`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cms_app_event`
--
ALTER TABLE `cms_app_event`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cms_app_exercise`
--
ALTER TABLE `cms_app_exercise`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cms_app_fans`
--
ALTER TABLE `cms_app_fans`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cms_app_history`
--
ALTER TABLE `cms_app_history`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cms_app_merchandise`
--
ALTER TABLE `cms_app_merchandise`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_merchandisa_category` (`id_merchandise_category`);

--
-- Indexes for table `cms_app_merchandise_category`
--
ALTER TABLE `cms_app_merchandise_category`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cms_app_merchandise_colors`
--
ALTER TABLE `cms_app_merchandise_colors`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_merchandisa` (`id_merchandise`);

--
-- Indexes for table `cms_app_merchandise_comment`
--
ALTER TABLE `cms_app_merchandise_comment`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cms_app_merchandise_images`
--
ALTER TABLE `cms_app_merchandise_images`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cms_app_merchandise_size`
--
ALTER TABLE `cms_app_merchandise_size`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cms_app_paket_topup`
--
ALTER TABLE `cms_app_paket_topup`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cms_app_payment`
--
ALTER TABLE `cms_app_payment`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cms_app_rekening`
--
ALTER TABLE `cms_app_rekening`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cms_app_session`
--
ALTER TABLE `cms_app_session`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cms_app_trainer`
--
ALTER TABLE `cms_app_trainer`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cms_app_users`
--
ALTER TABLE `cms_app_users`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cms_app_wallet`
--
ALTER TABLE `cms_app_wallet`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cms_category`
--
ALTER TABLE `cms_category`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cms_category_lang`
--
ALTER TABLE `cms_category_lang`
  ADD KEY `slug` (`slug`);

--
-- Indexes for table `cms_config`
--
ALTER TABLE `cms_config`
  ADD PRIMARY KEY (`id`),
  ADD KEY `name` (`name`(191));

--
-- Indexes for table `cms_event`
--
ALTER TABLE `cms_event`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_event_users1_idx` (`users_id`);

--
-- Indexes for table `cms_event_lang`
--
ALTER TABLE `cms_event_lang`
  ADD PRIMARY KEY (`id`,`lang`),
  ADD KEY `id` (`id`);

--
-- Indexes for table `cms_groups`
--
ALTER TABLE `cms_groups`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `name` (`name`);

--
-- Indexes for table `cms_lang`
--
ALTER TABLE `cms_lang`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cms_logs`
--
ALTER TABLE `cms_logs`
  ADD PRIMARY KEY (`id`),
  ADD KEY `log_flag` (`flag`(250)),
  ADD KEY `log_person_id` (`users_id`);
ALTER TABLE `cms_logs` ADD FULLTEXT KEY `log_message` (`message`);

--
-- Indexes for table `cms_media`
--
ALTER TABLE `cms_media`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cms_media_lang`
--
ALTER TABLE `cms_media_lang`
  ADD PRIMARY KEY (`lang`,`id`),
  ADD KEY `fk_media_lang_media1_idx` (`id`);

--
-- Indexes for table `cms_member`
--
ALTER TABLE `cms_member`
  ADD PRIMARY KEY (`member_id`);

--
-- Indexes for table `cms_member_tokens`
--
ALTER TABLE `cms_member_tokens`
  ADD PRIMARY KEY (`token_id`);

--
-- Indexes for table `cms_message`
--
ALTER TABLE `cms_message`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cms_modules`
--
ALTER TABLE `cms_modules`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cms_nav`
--
ALTER TABLE `cms_nav`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cms_nav_groups`
--
ALTER TABLE `cms_nav_groups`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cms_nav_lang`
--
ALTER TABLE `cms_nav_lang`
  ADD PRIMARY KEY (`lang`,`id`);

--
-- Indexes for table `cms_page`
--
ALTER TABLE `cms_page`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_page_category_idx` (`category_id`),
  ADD KEY `fk_page_users1_idx` (`users_id`),
  ADD KEY `fk_page_media1_idx` (`media_id`);

--
-- Indexes for table `cms_page_lang`
--
ALTER TABLE `cms_page_lang`
  ADD PRIMARY KEY (`lang`,`id`),
  ADD KEY `fk_page_lang_page1_idx` (`id`);

--
-- Indexes for table `cms_sessions`
--
ALTER TABLE `cms_sessions`
  ADD PRIMARY KEY (`id`),
  ADD KEY `cms_sessions_timestamp` (`timestamp`);

--
-- Indexes for table `cms_stats`
--
ALTER TABLE `cms_stats`
  ADD PRIMARY KEY (`sessid`),
  ADD UNIQUE KEY `sessid` (`sessid`);

--
-- Indexes for table `cms_translate`
--
ALTER TABLE `cms_translate`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cms_translate_lang`
--
ALTER TABLE `cms_translate_lang`
  ADD PRIMARY KEY (`id`,`lang`);

--
-- Indexes for table `cms_users`
--
ALTER TABLE `cms_users`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cms_users_groups`
--
ALTER TABLE `cms_users_groups`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `uc_users_groups` (`user_id`,`group_id`),
  ADD KEY `fk_users_groups_users1_idx` (`user_id`),
  ADD KEY `fk_users_groups_groups1_idx` (`group_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `cms_app_config`
--
ALTER TABLE `cms_app_config`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `cms_app_event`
--
ALTER TABLE `cms_app_event`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `cms_app_exercise`
--
ALTER TABLE `cms_app_exercise`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `cms_app_fans`
--
ALTER TABLE `cms_app_fans`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;
--
-- AUTO_INCREMENT for table `cms_app_history`
--
ALTER TABLE `cms_app_history`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `cms_app_merchandise`
--
ALTER TABLE `cms_app_merchandise`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=41;
--
-- AUTO_INCREMENT for table `cms_app_merchandise_category`
--
ALTER TABLE `cms_app_merchandise_category`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `cms_app_merchandise_colors`
--
ALTER TABLE `cms_app_merchandise_colors`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=51;
--
-- AUTO_INCREMENT for table `cms_app_merchandise_comment`
--
ALTER TABLE `cms_app_merchandise_comment`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `cms_app_merchandise_images`
--
ALTER TABLE `cms_app_merchandise_images`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=35;
--
-- AUTO_INCREMENT for table `cms_app_merchandise_size`
--
ALTER TABLE `cms_app_merchandise_size`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=47;
--
-- AUTO_INCREMENT for table `cms_app_paket_topup`
--
ALTER TABLE `cms_app_paket_topup`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `cms_app_payment`
--
ALTER TABLE `cms_app_payment`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `cms_app_rekening`
--
ALTER TABLE `cms_app_rekening`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `cms_app_session`
--
ALTER TABLE `cms_app_session`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT for table `cms_app_trainer`
--
ALTER TABLE `cms_app_trainer`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `cms_app_users`
--
ALTER TABLE `cms_app_users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;
--
-- AUTO_INCREMENT for table `cms_app_wallet`
--
ALTER TABLE `cms_app_wallet`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `cms_category`
--
ALTER TABLE `cms_category`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `cms_config`
--
ALTER TABLE `cms_config`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;
--
-- AUTO_INCREMENT for table `cms_event`
--
ALTER TABLE `cms_event`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `cms_groups`
--
ALTER TABLE `cms_groups`
  MODIFY `id` mediumint(8) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `cms_lang`
--
ALTER TABLE `cms_lang`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `cms_logs`
--
ALTER TABLE `cms_logs`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `cms_media`
--
ALTER TABLE `cms_media`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `cms_member`
--
ALTER TABLE `cms_member`
  MODIFY `member_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `cms_member_tokens`
--
ALTER TABLE `cms_member_tokens`
  MODIFY `token_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `cms_message`
--
ALTER TABLE `cms_message`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `cms_modules`
--
ALTER TABLE `cms_modules`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=37;
--
-- AUTO_INCREMENT for table `cms_nav`
--
ALTER TABLE `cms_nav`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `cms_nav_groups`
--
ALTER TABLE `cms_nav_groups`
  MODIFY `id` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `cms_page`
--
ALTER TABLE `cms_page`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `cms_translate`
--
ALTER TABLE `cms_translate`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `cms_translate_lang`
--
ALTER TABLE `cms_translate_lang`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `cms_users`
--
ALTER TABLE `cms_users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `cms_users_groups`
--
ALTER TABLE `cms_users_groups`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=32;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
