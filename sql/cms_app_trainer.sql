-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Jun 01, 2017 at 03:53 PM
-- Server version: 10.1.19-MariaDB
-- PHP Version: 5.6.28

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `aderai`
--

-- --------------------------------------------------------

--
-- Table structure for table `cms_app_trainer`
--

CREATE TABLE `cms_app_trainer` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `status` int(11) NOT NULL COMMENT '0=offiline, 1=online, 2=bussy',
  `vidcallprice` int(11) NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `cms_app_trainer`
--

INSERT INTO `cms_app_trainer` (`id`, `user_id`, `status`, `vidcallprice`, `created`, `modified`) VALUES
(1, 2, 1, 0, '2017-04-07 00:00:00', '2017-04-07 00:00:00'),
(3, 4, 2, 1000, '2017-06-01 08:13:18', '2017-06-01 09:48:07'),
(8, 23, 0, 500, '2017-06-01 08:46:37', '2017-06-01 09:48:20');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `cms_app_trainer`
--
ALTER TABLE `cms_app_trainer`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `cms_app_trainer`
--
ALTER TABLE `cms_app_trainer`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
