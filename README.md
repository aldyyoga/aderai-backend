Ade Rai Apps
============

Backend for Ade Rai Apps

For demo purpose only. **Do not use in production environment**.
Please create a new database using SQL file inside `/sql`
And change database configuration in `/app/config/database.php`
And also change the base_url in `/app/config/config.php`
If you've got a **404/505 error**, please check the `.htaccess` file and change as necessary.

For administrator login, use the user and password below.
```
User : admin@admin.com
Password : password
```

Enjoy.# My project's README
