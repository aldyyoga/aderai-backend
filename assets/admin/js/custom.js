$(document).ready(function() {
	$('.delete').click(function(){
		if(confirm('Are you sure delete this record ? ')) {
			return true;
		} else {
			return false;
		}
	});

	$('.search').click(function(){
		$(this).toggleClass('active');
		$('.search + form').toggleClass('hidden');
		$('.search + form > input').focus().val('');
	});

	$('.printarea').click(function(){
		$('.box-body').printArea();    
	});

	$('.logout').click(function(){
		if(confirm('Do you really want to log out ')) {
			return true;
		} else {
			return false;
		}
	});

    $('.datepicker').datepicker();
    
});

var slug = function(str) {
    var $slug = '';
    var trimmed = $.trim(str);
    $slug = trimmed.replace(/[^a-z0-9-]/gi, '-').
    replace(/-+/g, '-').
    replace(/^-|-$/g, '');
    return $slug.toLowerCase();
}