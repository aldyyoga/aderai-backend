<?php
error_reporting(-1);
ini_set('display_errors', 1);
require_once 'android_login_connect.php';

$db = new android_login_connect();
$conn = $db->connect();

// json response array
$response = array("error" => FALSE, "data" => array());

if (isset($_GET['category'])) {

    // receiving the post params
    $kategori = $_GET['category'];
    if($kategori==0){
      $stmt = $conn->prepare("SELECT id, name, price FROM `cms_app_merchandise` WHERE 1 LIMIT 10");
    }else{
      $stmt = $conn->prepare("SELECT id, name, price FROM `cms_app_merchandise` WHERE id_merchandise_category = ? LIMIT 10");
      $stmt->bind_param("s", $kategori);
    }

    if ($stmt->execute()) {
        $stmt-> bind_result($id,$name,$price);

        $response["error"] = FALSE;

        while ( $stmt-> fetch() ) {
           $data["id"] = $id;
           $data["name"] = $name;
           $data["price"] = $price;
           $data["image"] = '';
           array_push($response['data'], $data);
        }

        $stmt->close();

        echo json_encode($response);
    } else {
        $response["error"] = TRUE;
        echo json_encode($response);
    }

} else {
    // required post params is missing
    $response["error"] = TRUE;
    $response["error_msg"] = "Required parameters is missing!";
    echo json_encode($response);
}
?>
