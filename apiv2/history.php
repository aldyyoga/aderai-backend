<?php
error_reporting(-1);
ini_set('display_errors', 0);
require_once 'android_login_connect.php';

$db = new android_login_connect();
$conn = $db->connect();
$response = array("error" => FALSE, "data" => array());

if (isset($_GET['user_id'])){

  $stmt = $conn->prepare("
    SELECT id,title,type,`datetime`,calltime,price 
    FROM `cms_app_history` WHERE user_id=? ORDER BY id DESC LIMIT 10
  ");
  $stmt->bind_param("s", $_GET['user_id']);

  if ($stmt->execute()) {
      $stmt->bind_result($id, $title, $type, $datetime, $calltime, $price);

      $response["error"] = FALSE;

      while ( $stmt-> fetch() ) {
         $data["id"] = $id;
         $data["title"] = $title;
         $data["type"] = $type;
         $data["datetime"] = $datetime;
         $data["calltime"] = date('H:i:s',$calltime);
         $data["price"] = $price;
         array_push($response['data'], $data);
      }

      $stmt->close();

      echo json_encode($response);
  } else {
      $response["error"] = TRUE;
      echo json_encode($response);
  }

} else {
    $response["error"] = TRUE;
    $response["error_msg"] = "Required parameters is missing!";
    echo json_encode($response);
}

?>
