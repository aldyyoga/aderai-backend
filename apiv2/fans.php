<?php
error_reporting(-1);
ini_set('display_errors', 1);
require_once 'android_login_connect.php';

$db = new android_login_connect();
$conn = $db->connect();
$response = array("error" => FALSE, "data" => array());

if (isset($_GET['user_id'])){
  $stmt = $conn->prepare("
    SELECT u.id, ranks.rank, ranks.point, u.name, u.image
    FROM (    
       SELECT @rownum := @rownum +1 rank, p.id, p.point, p.user_id
       FROM cms_app_fans p, (SELECT @rownum :=0)r
       ORDER BY point DESC
    ) ranks INNER JOIN cms_app_users u ON ranks.user_id=u.id
    WHERE ranks.user_id=?");
  $stmt->bind_param("s", $_GET['user_id']);

  if ($stmt->execute()) {
    $stmt-> bind_result($id, $rank, $point, $name, $image);
    while ( $stmt-> fetch() ) {
       $data["id"] = $id;
       $data["rank"] = $rank;
       $data["point"] = $point;
       $data["name"] = $name;
       $data["image"] = $image;
       $response['myrank'] = $data;
    }
  }

}

$stmt = $conn->prepare("
  SELECT u.id, ranks.rank, ranks.point, u.name, u.image
  FROM (    
     SELECT @rownum := @rownum +1 rank, p.id, p.point, p.user_id
     FROM cms_app_fans p, (SELECT @rownum :=0)r
     ORDER BY point DESC
  ) ranks INNER JOIN cms_app_users u ON ranks.user_id=u.id
  WHERE 1 LIMIT 10");

if ($stmt->execute()) {
    $stmt-> bind_result($id, $rank, $point, $name, $image);

    $response["error"] = FALSE;

    while ( $stmt-> fetch() ) {
       $data["id"] = $id;
       $data["rank"] = $rank;
       $data["point"] = $point;
       $data["name"] = $name;
       $data["image"] = $image;
       array_push($response['data'], $data);
    }

    $stmt->close();

    echo json_encode($response);
} else {
    $response["error"] = TRUE;
    echo json_encode($response);
}

?>
