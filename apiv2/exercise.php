<?php
error_reporting(-1);
ini_set('display_errors', 1);
require_once 'android_login_connect.php';

$db = new android_login_connect();
$conn = $db->connect();
$response = array("error" => FALSE, "data" => array());

$stmt = $conn->prepare("SELECT id,title,description,total_like,total_share,total_views,video FROM `cms_app_exercise` WHERE 1 LIMIT 10");

if ($stmt->execute()) {
    $stmt-> bind_result($id, $judul, $deskripsi, $total_like, $total_share, $total_views, $video);

    $response["error"] = FALSE;

    while ( $stmt-> fetch() ) {
       $data["id"] = $id;
       $data["title"] = $judul;
       $data["description"] = $deskripsi;
       $data["total_like"] = $total_like;
       $data["total_share"] = $total_share;
       $data["total_views"] = $total_views;
       $data["video"] = $video;
       array_push($response['data'], $data);
    }

    $stmt->close();

    echo json_encode($response);
} else {
    $response["error"] = TRUE;
    echo json_encode($response);
}

?>
