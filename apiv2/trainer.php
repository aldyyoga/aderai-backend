<?php
error_reporting(-1);
ini_set('display_errors', 1);
require_once 'android_login_connect.php';

$db = new android_login_connect();
$conn = $db->connect();
$response = array("error" => FALSE, "data" => array());

$stmt = $conn->prepare("
  SELECT u.id, u.name, u.image, t.status 
  FROM `cms_app_users` AS `u`
  INNER JOIN `cms_app_trainer` AS `t`
  ON u.id=t.user_id WHERE 1 ORDER BY u.name LIMIT 10
");

if ($stmt->execute()) {
    $stmt-> bind_result($id, $name, $image, $status);
    $response["error"] = FALSE;

    while ( $stmt-> fetch() ) {
       $data["id"] = $id;
       $data["name"] = $name;
       $data["image"] = $image;

       if($status==0){
        $str_status = 'offline';
       }elseif ($status==1) {
        $str_status = 'online';
       }elseif ($status==2) {
        $str_status = 'bussy';
       }else{
        $str_status = 'None';
       }
       $data["status"] = $str_status;
       array_push($response['data'], $data);
    }
    $stmt->close();

    echo json_encode($response);
} else {
    $response["error"] = TRUE;
    echo json_encode($response);
}

?>
