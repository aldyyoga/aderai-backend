<?php
error_reporting(-1);
ini_set('display_errors', 0);
require_once 'android_login_connect.php';

$db = new android_login_connect();
$conn = $db->connect();
$response = array("error" => TRUE, "data" => array());

if (isset($_POST['user_id']) && isset($_POST['title']) && isset($_POST['type'])) {

  if($_POST['type']=='VIDCALL'){
    if(!isset($_POST['calltime'])){
      $response["error_msg"] = "Required parameters calltime!";
      echo json_encode($response);
      exit();
    }else{

      $str_time = $_POST['calltime'];
      sscanf($str_time, "%d:%d:%d", $hours, $minutes, $seconds);
      $time_seconds = isset($seconds) ? $hours * 3600 + $minutes * 60 + $seconds : $hours * 60 + $minutes;

      $price = 0;
      $calltime = $time_seconds;
      $stmt = $conn->prepare("SELECT id, value FROM cms_app_config WHERE `key`='VIDCALL.PRICE' LIMIT 1");
      if ($stmt->execute()) {
        $stmt->bind_result($id, $value);
        while ( $stmt-> fetch() ) {
          $price = (ceil($calltime/60) * $value);
        }
      }

    }
  }elseif ($_POST['type']=='MERCHAN') {
    if(!isset($_POST['price'])){
      $response["error_msg"] = "Required parameters price!";
      echo json_encode($response);
      exit();
    }else{
      $calltime = 0;
      $price = $_POST['price'];
    }
  }

  $datetime = date("Y-m-d H:i:s");

  $stmt = $conn->prepare("
    INSERT INTO `cms_app_history` (`id`, `user_id`, `title`, `type`, `datetime`, `calltime`, `price`, `created`, `modified`) 
    VALUES (NULL, ?, ?, ?, ?, ?, ?, ?, ?);
  ");
  $stmt->bind_param("ssssssss", $_POST['user_id'], $_POST['title'], $_POST['type'], $datetime, $calltime, $price, $datetime, $datetime);
  $result = $stmt->execute();
  $stmt->close();

  if ($result) {
    $data["id"] = 0;
    $data["title"] = $_POST['title'];
    $data["type"] = $_POST['type'];
    $data["datetime"] = $datetime;
    $data["calltime"] = date('H:i:s',$calltime);
    $data["price"] = $price;

    $response["error"] = FALSE;
    $response["data"] = $data;
    echo json_encode($response);
  }else{
    $response["error"] = TRUE;
    echo json_encode($response);
  }

} else {
    $response["error"] = TRUE;
    $response["error_msg"] = "Required parameters is missing!";
    echo json_encode($response);
}

?>
