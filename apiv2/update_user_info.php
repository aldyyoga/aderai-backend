<?php

class update_user_info {

    private $conn;

    // constructor
    function __construct() {
        require_once 'android_login_connect.php';
        // connecting to database
        $db = new android_login_connect();
        $this->conn = $db->connect();
    }

    // destructor
    function __destruct() {

    }

    /**
     * Storing new user
     * returns user details
     */
    public function StoreUserInfo($name, $email, $password, $gender, $age) {
        $encrypted_password = sha1($password);

        $stmt = $this->conn->prepare("INSERT INTO `cms_app_users` (`id`, `name`, `email`, `password`, `remember_token`, `image`, `created`, `modified`) VALUES (NULL, ?, ?, ?,NULL, '', '2017-03-21 00:00:00', '2017-03-21 00:00:00');");
        $stmt->bind_param("sss", $name, $email, $encrypted_password);
        $result = $stmt->execute();
        $stmt->close();

        // check for successful store
        if ($result) {
            $stmt = $this->conn->prepare("SELECT id, name, email, password FROM `cms_app_users` WHERE email = ?");
            $stmt->bind_param("s", $email);
            $stmt->execute();
            $stmt->bind_result($token1, $token2,$token3,$token4);
            while ( $stmt-> fetch() ) {
               $user["id"] = $token1;
               $user["name"] = $token2;
               $user["email"] = $token3;
               $user["gender"] = "Male";
               $user["age"] = 23;
            }
            $stmt->close();

            $stmt = $this->conn->prepare("INSERT INTO `cms_app_fans` (`id`, `user_id`, `point`, `created`, `modified`) VALUES (NULL, ?, 5, '2017-03-21 00:00:00', '2017-03-21 00:00:00');");
               $stmt->bind_param("s", $token1);
               $result = $stmt->execute();
            $stmt->close();
            
            return $user;
        } else {
          return false;
        }
    }

    /**
     * Get user by email and password
     */
    public function VerifyUserAuthentication($email, $password) {

        $stmt = $this->conn->prepare("SELECT u.id, u.name, u.email, u.password, IF(t.id, 'true', 'false') AS trainer FROM `cms_app_users` AS u LEFT JOIN `cms_app_trainer` AS t ON u.id=t.user_id WHERE email = ?");
        $stmt->bind_param("s", $email);

        if ($stmt->execute()) {
            $stmt-> bind_result($token1, $token2,$token3,$token4,$token5);

            while ( $stmt-> fetch() ) {
               $user["id"] = $token1;
               $user["name"] = $token2;
               $user["email"] = $token3;
               $user["encrypted_password"] = $token4;
               $user["trainer"] = $token5;
               $user["gender"] = "Male";
               $user["age"] = 23;
            }

            $stmt->close();

            $encrypted_password = $token4;
            if ($encrypted_password == sha1($password)) {
                return $user;
            }
        } else {
            return NULL;
        }
    }

    /**
     * Check user is existed or not
     */
    public function CheckExistingUser($email) {
        $stmt = $this->conn->prepare("SELECT email from `cms_app_users` WHERE email = ?");

        $stmt->bind_param("s", $email);

        $stmt->execute();

        $stmt->store_result();

        if ($stmt->num_rows > 0) {
            // user existed 
            $stmt->close();
            return true;
        } else {
            // user not existed
            $stmt->close();
            return false;
        }
    }

}

?>
