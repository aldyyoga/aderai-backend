<?php
error_reporting(-1);
ini_set('display_errors', 1);
require_once 'android_login_connect.php';

$db = new android_login_connect();
$conn = $db->connect();
$response = array("error" => FALSE);

if (isset($_GET['id']) && isset($_GET['type']) && isset($_GET['user_id'])) {

  $stmt = $conn->prepare("SELECT id,title,description,total_like,total_share,total_views,video FROM `cms_app_exercise` WHERE id=? LIMIT 10");
  $stmt->bind_param("s", $_GET['id']);
  if ($stmt->execute()) {
      $stmt->bind_result($id, $judul, $deskripsi, $total_like, $total_share, $total_views, $video);

      while ( $stmt-> fetch() ) {
         $data["id"] = $id;
         $data["title"] = $judul;
         $data["description"] = $deskripsi;
         $data["total_like"] = $total_like;
         $data["total_share"] = $total_share;
         $data["total_views"] = $total_views;
         $data["video"] = $video;
      }
      $stmt->close();

      if($_GET['type']=='like'){
        $data['total_like']++;
        $update_data = $data['total_like'];
        $stmt = $conn->prepare("UPDATE `cms_app_exercise` SET `total_like` = ? WHERE `id` = ?;");
      }elseif ($_GET['type']=='share') {
        $data['total_share']++;
        $update_data = $data['total_share'];
        $stmt = $conn->prepare("UPDATE `cms_app_exercise` SET `total_share` = ? WHERE `id` = ?;");
      }elseif ($_GET['type']=='views') {
        $data['total_views']++;
        $update_data = $data['total_views'];
        $stmt = $conn->prepare("UPDATE `cms_app_exercise` SET `total_views` = ? WHERE `id` = ?;");
      }else{
        echo json_encode($response);
        exit();
      }

      $stmt->bind_param("ss", $update_data, $_GET['id']);
      $result = $stmt->execute();
      $stmt->close();

      $response['data'] = $data;
      $response["error"] = FALSE;
      echo json_encode($response);
  } else {
      $response["error_msg"] = "Parameters type error!";
      $response["error"] = TRUE;
      echo json_encode($response);
  }

} else {
    $response["error"] = TRUE;
    $response["error_msg"] = "Required parameters is missing!";
    echo json_encode($response);
}
?>