<?php
error_reporting(-1);
ini_set('display_errors', 1);
require_once 'android_login_connect.php';

$db = new android_login_connect();
$conn = $db->connect();
$response = array("error" => FALSE, "data" => array());

$stmt = $conn->prepare("SELECT id,name FROM `cms_app_merchandise_category` WHERE 1");

if ($stmt->execute()) {
    $stmt-> bind_result($id, $name);

    $response["error"] = FALSE;

    while ( $stmt-> fetch() ) {
       $data["id"] = $id;
       $data["name"] = $name;
       array_push($response['data'], $data);
    }

    $stmt->close();

    echo json_encode($response);
} else {
    $response["error"] = TRUE;
    echo json_encode($response);
}

?>
