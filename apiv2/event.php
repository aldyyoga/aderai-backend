<?php
error_reporting(-1);
ini_set('display_errors', 1);
require_once 'android_login_connect.php';

$db = new android_login_connect();
$conn = $db->connect();
$response = array("error" => FALSE, "data" => array());

$stmt = $conn->prepare("SELECT id,`date`,title,description FROM `cms_app_event` WHERE 1 LIMIT 10");

if ($stmt->execute()) {
    $stmt-> bind_result($id, $tanggal, $judul, $deskripsi);

    $response["error"] = FALSE;

    while ( $stmt-> fetch() ) {
       $data["id"] = $id;
       $data["date"] = $tanggal;
       $data["title"] = $judul;
       $data["description"] = $deskripsi;
       array_push($response['data'], $data);
    }

    $stmt->close();

    echo json_encode($response);
} else {
    $response["error"] = TRUE;
    echo json_encode($response);
}

?>
